# Assumptions

* Network
    * Server/client split
    * Content is sent on start up, and cannot be resent.
* World and map
    * Composition
        * 3D tile map rendered in 2D. Player can move up and down via stairs
        * Map split into chunks.
        * Chunks are grouped into vertical columns called sectors.
    * Map size
        * X/Y should support at least 30km maps, ideally more.
        * Z only needs 255 levels, maybe 134 down and 120 up.
    * MapGen
        * City with blocks: Straight lines, 90 deg intercepts.
        * Most buildings should be able to be entered.
    * Entities
        * Only 20 NPCs and Players per chunk, usually.
        * May be up to 200 of dropped items and electronics.
    * Players will never be able to walk into or be in an unloaded area, except for:
        * Joining a game
        * Going up/down stairs.
          (This should should be hidden by automatically sending render_chunks if there are stairs.)

# Limitations

* Entities will crash after 27 years of load time due to packet serialisation: (2^32) * 0.2/60/60/24/365

# Issues needing solving

Tile selection.
Placing nodes does not follow MVC.

# Subsystems

* Types   - defines portable types (u8, u16, etc), V2s, V3s, V3f
* World   - the ingame world, used both client and serer side. Also see client/localworld
* Content - type definitions for tiles, floors, terrain, items, etc.
* Network - has the socket wrapper, helper functions, and protocol enum
* Server
* Tests   - tests for all above sections.

## Client

* client
    * sends and receives Packets
* game
    * controls the render window
* localworld
    * draws the World
* playercontroller
    * controller for the PlayerEntity.
* ResourceManager
    * Holds the loaded textures, manages texture atlases

# Entity system

* Both server and client know the entities type.

# Flow

Game creates client and all components
Game represents a single online session
Main runs the main menu which then calls the Game
