# Types of Content

## 1. Entities

Entities have a float position on XY plane, and tend/can to move regularly.

* Dropped items - Can be interacted with E or something.
* NPCs - AI controlled
    * Inhabitants
    * Rivals
    * Civilians
    * Police
    * Animals
    * Turrets/robots?
* Vehicles - can be mounted/attached to
    * Cars
    * Bikes
    * Trains

## 2. Tiles/Nodes

Nodes have an integer position and don't tend to move.

* Building
    * Walls/Floor/Terrain - no interaction, other than placing. Can use 9-splice
    * Doors - slide
* Crafting
    * Work tables
    * Storage
    * Training/research
* Welfare
    * Beds
    * Chairs
    * Benches
* Military
    * Sandbags
    * Wall mounted turrets

## 3. Particles

Particles are client visuals with no side effects. IE: nothing can collide with
particles, particles don't set things on fire. Particles may collide with things.

When a gun shoots, two things happen:

1. A raytracer looks at the route.
2. A particle is displayed to show shot, but isn't actually a bullet.

## 4. Plot

An organisational structure, used for finding jobs and resources.


# Materials

Materials need to be specified by the server, but whether the client uses
an atlas is up to them (except maybe for the tileset)
Each texture has a name, say the original filename
Manager class to get some sort of ClientMaterial to be drawn, which takes into
account stylesheet.

How to pass to where it is needed?

What does the server need?

* Source file
* Rendering detail

Client:

* Actual Texture
* Start position in texture

# Content

## Outside // Gardens and Nature

* Grass
* Dirt
* Tree
* Pond

## Outside // City

* Road pieces
* Crossings
* Signs
* Pavements
* Phone boxes
    * Can be used to call people (?)
* Shopfronts
* Stalls
* Statues / squares

## Indoors // Typical

* Walls
    * Plastered brick
* Stairs
    * Up and down
* Doors
    * Wood
    * Steel
    * Bomb seal (?)
* Chairs / sofas / benches
* Tables / islands / bedside Tables
* Lights
    * Ceiling
    * bedside
* Tech
    * TVs
    * Desktops
    * Laptops
* Work Tables
* Storage
* Research

## Indoors // Training and Military

* Fitness machines
* Target ranges
* Sandbags
* Turrets

## Indoors // Other

* Servers
* Cells
