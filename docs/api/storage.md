---
title: Storage
layout: default
nav_order: 10
---

# Storage

## KeyValueStore

A key-value store, to handle meta data or settings. Curre

All functions return either the stated function or nil.

* `get_string(key)`
* `set_string(key, value)`
* `get_int(key)`
* `set_int(key, value)`
* `get_bool(key)`
* `set_bool(key, value)`
* `get_float(key)`
* `set_float(key, value)`
* `to_table()` - table with string keys and string values.
