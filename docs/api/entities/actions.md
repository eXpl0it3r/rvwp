---
title: Entity Actions
layout: default
nav_order: 10
---

# Entity Actions

An action is a special type of [Entity Controller](EntityController.html) which exists only until a particular goal is
met.

## rvwp.entity.actions

A list of available actions

* follow(entity) - Follows an entity forever
* go_to(pos) - Go to a position


## Example

```lua
local Thing = EntityController:new()

function Thing:on_create()
    self.entity:set_properties({ material = "thing.png" })
    self:set_action(rvwp.entity.actions.go_to, Vector:new(1, 1, 0))
end

rvwp.entity.register("dog", Dog)
```
