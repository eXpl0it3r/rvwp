--
-- Test NPC
--

local Dog = class(EntityController)
function Dog:on_create()
	self.entity:set_properties({ material = "dog.png" })
	self.bt = behaviour.load_tree(self, "mods/core/behaviour/trees/dog.json")
end
function Dog:step(dtime)
	self.bt:run()
end

rvwp.entity.register("dog", Dog)



rvwp.register_chat_command("dog", commands.ChatCommand:new({
	func = function(self, name)
		local pos = rvwp.get_player_entity(name):get_pos()
		if rvwp.spawn_entity(pos, "dog") then
			return true, "Done"
		else
			return false, "Failed"
		end
	end
}))
