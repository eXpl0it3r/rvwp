_G.table.copy = function(tab)
	local ret = {}
	for key, value in pairs(tab) do
		ret[key] = value
	end
	return ret
end

local MockBehaviour = class(behaviour.Node)

function MockBehaviour:constructor()
	self.result = behaviour.states.RUNNING
	self.ran = false
end

function MockBehaviour:on_step()
	self.ran = true
	return self.result
end

return {
	MockBehaviour = MockBehaviour
}
