local function hash(value)
	return tostring(value:floor())
end

local reserved = {}

function jobs.can_access(pos, entity)
	assert(entity)

	local id = reserved[hash(pos)]
	return not id or id == entity:get_id()
end

function jobs.reserve(pos, entity)
	assert(entity)

	print("Reserve " .. pos)
	reserved[hash(pos)] = entity:get_id()
end

function jobs.unreserve(pos, entity)
	assert(entity)

	print("Unreserved " .. pos)

	local id = reserved[hash(pos)]
	if id then
		assert(id == entity:get_id())
		reserved[hash(pos)] = nil
	end
end


rvwp.register_chat_command("reserved", commands.ChatCommand:new({
	func = function(self, name, param)
		return true, "Reserved positions: " .. table.concat(table.keys(reserved), " | ")
	end
}))
