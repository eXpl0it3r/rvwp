commands = {}

dofile("mods/core/commands/api.lua")

local ChatCommand = commands.ChatCommand

rvwp.register_on_chat_message(function(name, message)
	print("Lua: " .. name .. " said " .. message)
end)

rvwp.register_chat_command("lua", ChatCommand:new({
	func = function(self, name, param)
		-- First check to see if it can be evaluated as an expression,
		-- then fallback to executing it raw
		local func, error
		func = loadstring("return (" .. param .. ")")
		if not func then
			func, error = loadstring(param)
			if not func then
				return false, "E " .. error
			end
		end

		local good, errOrResult = pcall(func)
		if not good then
			return false, "E " .. errOrResult
		end

		return true, "= " .. dump(errOrResult)
	end
}))

rvwp.register_chat_command("mods", ChatCommand:new({
	func = function(self, name, param)
		local mods = rvwp.get_mods()
		table.sort(mods)
		return true, "Mods: " .. table.concat(mods, ", ")
	end
}))

rvwp.register_chat_command("help", ChatCommand:new({
	func = function(self, name, param)
		local names = {}
		for cmdname, _ in pairs(rvwp.registered_chat_commands) do
			names[#names + 1] = cmdname
		end
		table.sort(names)
		return true, "Commands: " .. table.concat(names, ", ")
	end
}))

rvwp.register_chat_command("time", ChatCommand:new({
	func = function(self, name, param)
		if param:trim() == "" then
			local time = rvwp.get_time()
			return true, ("%s - speed %.0f - time of day %.2f"):format(time.clock, time.speed, time.time_of_day)
		else
			local time_of_day = tonumber(param)
			if not time_of_day then
				return false, "Expected number to /time"
			end

			rvwp.set_time_of_day(time_of_day)
			return true, "Set time"
		end
	end
}))

rvwp.register_chat_command("timespeed", ChatCommand:new({
	func = function(self, name, param)
		local speed = tonumber(param)
		if not speed then
			return false, "Expected number to /timespeed"
		end

		local time = rvwp.get_time()
		rvwp.set_time_of_day(time.time_of_day, speed)
		return true, "Set time speed"
	end
}))


if rvwp.debug_pause then
	rvwp.register_chat_command("debug", ChatCommand:new({
		func = function(self, name, param)
			rvwp.debug_pause()
			return true, "Finished debug pause"
		end
	}))

	rvwp.register_chat_command("bp", ChatCommand:new({
		func = function(self, name, param)
			rvwp.debug_breakpoint(param)
			return true, "Added breakpoint"
		end
	}))
end
