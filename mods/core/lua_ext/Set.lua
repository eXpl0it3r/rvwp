Set = class()

function Set:constructor(items, hasher)
	self.data = {}
	self.hasher = hasher

	if items then
		if not self.hasher and getmetatable(items) == Set then
			self.hasher = items.hasher
		end
		self:add_all(items)
	end
end

local function hash(hasher, value)
	return hasher and hasher(value) or value
end

function Set:contains(value)
	return self.data[hash(self.hasher, value)] ~= nil
end

function Set:add(value)
	self.data[hash(self.hasher, value)] = value
end

function Set:add_all(iterable)
	for _, value in pairs(iterable) do
		self.data[hash(self.hasher, value)] = value
	end
end

function Set:remove(value)
	self.data[hash(self.hasher, value)] = nil
end

function Set:remove_all(iterable)
	for _, value in pairs(iterable) do
		self.data[hash(self.hasher, value)] = nil
	end
end

function Set:to_table()
	local ret = {}
	for _, value in pairs(self.data) do
		ret[#ret + 1] = value
	end
	return ret
end

function Set:pairs()
	return _G.pairs(self.data)
end
Set.__pairs = Set.pairs

function Set:union(other)
	local set = Set:new(self)
	set:add_all(other)
	return set
end
Set.__add = Set.union

function Set:difference(other)
	local set = Set:new(self)
	set:remove_all(other)
	return set
end
Set.__sub = Set.difference

function Set:intersection(other)
	local set = Set:new({}, self.hasher)
	for _, value in pairs(self.data) do
		if other:contains(value) then
			set:add(value)
		end
	end
	return set
end

function Set:is_subset(other)
	for _, value in pairs(other) do
		if not self:contains(value) then
			return false
		end
	end
	return true
end

function Set:is_disjoint(other)
	for _, value in pairs(other) do
		if self:contains(value) then
			return false
		end
	end
	return true
end

function Set:__eq(other)
	local data = table.copy(self.data)
	for key in pairs(other) do
		data[key] = nil
	end

	return not next(data)
end
