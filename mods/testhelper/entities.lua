local textures = {
	"dog.png",
	"sword.png"
}

local Flip = class(EntityController)
function Flip:on_create()
	self.counter = 0
	self.i = 0
	self:next()

	self.entity:set_properties({ footsteps = false })
end
function Flip:next()
	self.i = self.i + 1
	if self.i > #textures then
		self.i = 1
	end

	self.entity:set_properties({ material = textures[self.i] })
end
function Flip:step(dtime)
	self.counter = self.counter + dtime
	if self.counter > 2 then
		self.counter = 0
		self:next()
	end
end

rvwp.entity.register("flip", Flip)
