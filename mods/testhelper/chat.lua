local ChatCommand = commands.ChatCommand

rvwp.register_chat_command("a", ChatCommand:new({
	func = function(self, name, param)
		return true, "You said: " .. param:color("#ff0")
	end
}))

rvwp.register_chat_command("err", ChatCommand:new({
	func = function(self, name, param)
		error(param or "crash!")
		return true, "You said: " .. param
	end
}))

rvwp.register_chat_command("echo", ChatCommand:new({
	func = function(self, name, param)
		rvwp.chat_send_all(param)
		return true, "Echo'd"
	end
}))


rvwp.register_chat_command("player", ChatCommand:new({
	func = function(self, name, param)
		local player = rvwp.get_player(name)

		local info = {
			"Name: ", player:get_username(), "\n",
			"Online?: ", player:is_online() and "true" or "false", "\n",
			"Pos: ", tostring(player:get_entity():get_pos()), "\n",
			"Address: ", dump(player:get_address()),
		}

		return true, table.concat(info, "")
	end
}))


-- source: https://www.ltg.ed.ac.uk/~richard/unicode-sample.html
local unicode = {
	"\" # $ % & ' ( ) * + ,",
	"¡ ¢ £ ¤ ¥ ¦ § ¨ © ª « ¬ ­ ® ¯ ° ± ² ³ ´",
	"Ā ā Ă ă Ą ą Ć ć Ĉ ĉ Ċ",
	"ƀ Ɓ Ƃ ƃ Ƅ ƅ",
	"ɐ ɑ ɒ ɓ ɔ ɕ ɖ ɗ ɘ ə ",
	"Θ Ι Κ Λ Μ Ν Ξ Ο",
	"Ё Ђ Ѓ Є Ѕ І",
	"Ը Թ Ժ",
	"ڕ ږ ڗ ژ ڙ ښ ڛ ڜ",
	"ฒ ณ ด ต ถ",
	"℅ ℆ ℇ ℈ ℉ ℊ",
	"↕ ↖ ↗ ↘ ↙ ↚ ↛ ↜",
	"∇ ∈ ∉ ∊ ∋ ∌ ∍",
	"⑦ ⑧ ⑨ ⑩ ⑪",
	" ┈ ┉ ┊ ┋ ┌ ┍ ",
	"ぜ そ ぞ た だ ち ぢ っ つ",
	"上 下 丌 不 与 丏 丐 丑 丒 专 且 丕 世 丗 丘",
}

local function concat_wrap(tab)
	local ret = {}
	local segment = ""
	for i=1, #tab do
		if #segment + #tab[i] > 60 then
			ret[#ret + 1] = segment
			segment = ""
		end
		segment = segment .. " | " .. tab[i]
	end
	return table.concat(ret, "\n")
end

rvwp.register_chat_command("unicode", ChatCommand:new({
	func = function(self, name, param)
		return true, concat_wrap(unicode)
	end
}))

rvwp.register_chat_command("load_chunks", ChatCommand:new({
	func = function(self, name, param)
		local ret = rvwp.send_chunks_to_player(name, tonumber(param) or 5, 10000)
		return ret, (ret and "Success" or "Failed")
	end
}))

rvwp.register_chat_command("hp", ChatCommand:new({
	func = function(self, name, param)
		local entity = rvwp.get_player_entity(name)
		if param:trim() == "" then
			return true, "HP = " .. entity:get_hp()
		else
			param = tonumber(param)
			if not param then
				return false, "Expected number"
			end

			local old_hp = entity:get_hp()
			entity:set_hp(param)
			return true, "new HP = " .. entity:get_hp() .. ", old_hp=" .. old_hp
		end
	end
}))
