#!/bin/bash

CLANG_FORMAT=clang-format
files_to_lint="$(find src/  -path src/libs -prune -o \( -name '*.cpp' -or -name '*.hpp' \) )"

errorcount=0
fail=0
for f in ${files_to_lint}; do
	d=$(diff -u "$f" <(${CLANG_FORMAT} "$f") || true)

	if [[ -n "$d" ]]; then
		errorcount=$((errorcount+1))

		printf "The file %s is not compliant with the coding style" "$f"
		if [ ${errorcount} -gt 50 ]; then
			printf "\nToo many errors encountered previously, this diff is hidden.\n"
		else
			printf ":\n%s\n" "$d"
		fi

		fail=1
	else
		echo "Passed: $f"
	fi
done

if [ "$fail" = 1 ]; then
	echo "LINT reports failure."
	exit 1
fi

echo "LINT OK"
