local data = { [2] =  1, [8] =  2, [10] =  3, [11] =  4, [16] =  5, [18] =  6, [22] =  7, [24] =  8,
[26] =  9, [27] =  10, [30] =  11, [31] =  12, [64] =  13, [66] =  14, [72] =  15, [74] =  16, [75] =  17,
[80] =  18, [82] =  19, [86] =  20, [88] =  21, [90] =  22, [91] =  23, [94] =  24, [95] =  25, [104] =  26, [106] =  27,
[107] =  28, [120] =  29, [122] =  30, [123] =  31, [126] =  32, [127] =  33, [208] =  34, [210] =  35, [214] =  36,
[216] =  37, [218] =  38, [219] =  39, [222] =  40, [223] =  41, [248] =  42, [250] =  43, [251] =  44, [254] =  45,
[255] =  46, [0] =  47 }

local map = {
	TL = 1,
	TOP = 2,
	TR = 4,
	LEFT = 8,
	RIGHT = 16,
	BL = 32,
	BOTTOM = 64,
	BR = 128,
}

local cb = {
	TL = map.TOP | map.LEFT,
	TR = map.TOP | map.RIGHT,
	BL = map.BOTTOM | map.LEFT,
	BR = map.BOTTOM | map.RIGHT,
}

function to_bits(num,bits)
	-- returns a table of bits, most significant first.
	bits = bits or math.max(1, select(2, math.frexp(num)))
	local t = {} -- will contain the bits
	for b = bits, 1, -1 do
		t[b] = math.fmod(num, 2)
		num = math.floor((num - t[b]) / 2)
	end
	return table.concat(t, "")
end


local out = ""
for i=0, 255 do
	if not data[i] then
		print("Original: " .. i .. " | " .. to_bits(i, 8))
		local adjusted = i
		if (i & map.TL) > 0 and (i & cb.TL) ~= cb.TL then
			print(" - TL but no top or left, adjusting")
			adjusted = adjusted ~ map.TL
		end
		if (i & map.TR) > 0 and (i & cb.TR) ~= cb.TR then
			print(" - TR but no top or right, adjusting")
			adjusted = adjusted ~ map.TR
		end
		if (i & map.BL) > 0 and (i & cb.BL) ~= cb.BL then
			print(" - BL but no bottom or left, adjusting")
			adjusted = adjusted ~ map.BL
		end
		if (i & map.BR) > 0 and (i & cb.BR) ~= cb.BR then
			print(" - BR but no bottom or right, adjusting")
			adjusted = adjusted ~ map.BR
		end

		print(" - Adjusted: " .. to_bits(adjusted, 8))
		assert(adjusted ~= i, "Adjusted index is the same as the initial index")
		assert(data[adjusted], "Adjusted index doesn't exist")
		data[i] = data[adjusted]
		print(" - Set to " .. data[i])
	end


	if i > 0 and i % 16 == 0 then
		out = out .. ",\n"
	elseif i > 0 then
		out = out .. ", "
	end
	out = out .. data[i]
	if data[i] < 10 then
		out = out .. " "
	end
end

print(out)
