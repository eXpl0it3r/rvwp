#!/bin/bash

set -e

./bin/rvwp tests

find . -name "*.gcno"

mkdir coverage
lcov --directory CMakeFiles/rvwp.dir/src/ --base-directory src/ --no-external --capture --output-file coverage/lcov.info
lcov --remove coverage/lcov.info "*src/main*" -o coverage/lcov.info
lcov --remove coverage/lcov.info "*src/libs*" -o coverage/lcov.info
lcov --remove coverage/lcov.info "*src/test*" -o coverage/lcov.info
genhtml --output-directory coverage coverage/lcov.info

zip -r coverage.zip coverage
