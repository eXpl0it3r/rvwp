#!/bin/bash

set -e

if [ ! -d "vcpkg" ]; then
	git clone https://github.com/rubenwardy/vcpkg.git
	pushd vcpkg
	./bootstrap-vcpkg.sh
else
	pushd vcpkg
	git pull
fi

VCPKG_DEFAULT_TRIPLET=x64-linux ./vcpkg install sfml enet tgui jsoncpp
VCPKG_DEFAULT_TRIPLET=x64-linux ./vcpkg install thor --head

popd
