@echo off

git clone https://github.com/rubenwardy/vcpkg.git
cd vcpkg

call ./bootstrap-vcpkg.bat
VCPKG_DEFAULT_TRIPLET=x64-windows ./vcpkg install sfml enet luajit tgui jsoncpp
VCPKG_DEFAULT_TRIPLET=x64-windows ./vcpkg install thor --head
VCPKG_DEFAULT_TRIPLET=x64-windows ./vcpkg integrate install
