#!/bin/bash

set -e

valgrind --leak-check=full --log-file=valgrind.txt --gen-suppressions=yes ./bin/rvwp tests || true

cat valgrind.txt

errors=$(grep -oE "ERROR SUMMARY: [0-9]+" valgrind.txt | cut -c 16-)
if (( errors > 1 )); then
	exit 1
fi

exit 0
