#pragma once

#include <TGUI/TGUI.hpp>
#include <vector>
#include <optional>

namespace client {

class BottomBar : public tgui::Panel {
	struct MenuItem {
		tgui::String text;
		std::optional<tgui::Texture> texture;
		tgui::Widget::Ptr view;
		tgui::Widget::Ptr button = {};

		MenuItem(const tgui::String &text, std::optional<tgui::Texture> texture,
				tgui::Widget::Ptr view)
				: text(text), texture(texture), view(view) {}
	};

	tgui::HorizontalLayout::Ptr layout;
	std::vector<MenuItem> items;
	int selectedIdx = -1;

public:
	typedef std::shared_ptr<BottomBar> Ptr;

	BottomBar();

	static Ptr create(const tgui::Layout2d &size = {"100%", "100%"});

	void addButton(const std::string &text, sf::Texture *texture,
			tgui::Widget::Ptr view);
	void openTab(int idx);
	void toggleTab(int idx);
	bool closeTab();

private:
	void recreate();
};

} // namespace client
