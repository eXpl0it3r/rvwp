#pragma once

#include <SFML/Graphics/Sprite.hpp>
#include <string>
#include "content/InventorySelection.hpp"
#include "../ResourceManager.hpp"
#include <functional>

namespace client {
class InvSelection : public sf::Sprite, public content::InventorySelection {
public:
	explicit InvSelection(content::InventoryProvider provider,
			EventBus<content::InventoryCommand> *inventoryBus)
			: sf::Sprite(), content::InventorySelection(
									std::move(provider), inventoryBus) {
		setOrigin(32, 32);
	}

	InvSelection(const InvSelection &other) = delete;

	void clickWithMaterial(const content::StackLocation &location, int count,
			const ClientMaterial &material);
};
} // namespace client
