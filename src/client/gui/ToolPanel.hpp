#pragma once

#include <TGUI/TGUI.hpp>

#include "client/ResourceManager.hpp"
#include "content/ToolSpec.hpp"

namespace client {

class ToolPanel : public tgui::ScrollablePanel {
	ResourceManager *resourceManager;
	std::function<void(const content::ToolSpec &spec)> onToolSelected;
	std::vector<content::ToolSpec> specs;
	tgui::Widget::Ptr firstButton;
	std::unordered_map<u32, std::vector<tgui::Button::Ptr>>
			shortcutKeyToButtons;

public:
	typedef std::shared_ptr<ToolPanel> Ptr;

	explicit ToolPanel(ResourceManager *resourceManager);

	static Ptr create(
			ResourceManager *resourceManager, const tgui::Layout2d &size);

	void setSpecs(const std::vector<content::ToolSpec> &specs);

	void setOnToolSelected(
			std::function<void(const content::ToolSpec &spec)> v) {
		onToolSelected = v;
	}

	void textEntered(char32_t key) override;

private:
	void recreate();
};

} // namespace client
