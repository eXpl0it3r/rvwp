#include <cmath>

#include "HudView.hpp"
#include "client/Game.hpp"
#include "client/sfext/HexToColor.hpp"

#include <sstream>
#include <SFML/System.hpp>

using namespace client;
using namespace world;

HudView::HudView(Game *game, const sf::Font &font, WorldView *worldView)
		: game(game), font(font), worldView(worldView), chat(font, 17),
		  fps_graph("FPS", font, INT_MAX, 200, 100) {
	graphs.emplace_back("dtime/us", font, INT_MAX);
	graphs.emplace_back("Packets Sent", font);
	graphs.emplace_back("Packets Recvd", font);
	graphs.emplace_back("Packets Lost", font);
	graphs.emplace_back("Packets in transit", font);
	graphs.emplace_back("Chunk Meshgen", font);

	fps_graph.enable_colors = true;
}

void HudView::pushStat(HudGraphType type, int value) {
	switch (type) {
	case HudGraphType::GRAPH_DTIME:
		return graphs[0].push(value);
	case HudGraphType::GRAPH_PACKETS_SEND:
		return graphs[1].push(value);
	case HudGraphType::GRAPH_PACKETS_RECV:
		return graphs[2].push(value);
	case HudGraphType::GRAPH_PACKETS_LOST:
		return graphs[3].push(value);
	case HudGraphType::GRAPH_PACKETS_RELIABLE:
		return graphs[4].push(value);
	case HudGraphType::GRAPH_CHUNK_MESHGEN:
		return graphs[5].push(value);
	case HudGraphType::GRAPH_FPS:
		return fps_graph.push(value);
	}

	FatalError("Unsupported graph type.");
}

bool HudView::load() {
	hud_debug_info = sf::Text("RVWP", font, 16);
	hud_debug_info.setPosition(sf::Vector2f(10, 10));
	hud_debug_info.setFillColor(sf::Color::White);
	return true;
}

void HudView::drawHUD(sf::RenderTarget &target, sf::RenderStates states) {
	const bool draw_debug_info = debug_mode >= 1;
	const bool draw_fps_graph = draw_debug_info;
	const bool draw_graphs = draw_debug_info;

	if (draw_debug_info) {
		sf::RectangleShape hud_bg;
		hud_bg.setPosition(sf::Vector2f(5, 5));
		hud_bg.setFillColor(sf::Color(0, 0, 0, 128));

		// Update HUD
		if (display_refresh_counter > 0.2) {
			display_refresh_counter = 0;

			World *world = game->getWorld();
			int chunkCount = world->getChunkCount();
			int renderChunkCount = worldView->getRenderChunkCount();
			int vertexCount = worldView->getVertexCount();
			float last_rtime = game->last_rtime;
			const auto &camera = game->getCamera();
			const Plot *plot = world->getPlot(camera.pos.floor());
			const GameTime &time = game->getTime();

			std::ostringstream os;
			os << "RVWP"
			   << "\nFPS: " << std::floor(10.f / last_dtime) / 10.f
			   << "\ndtime: " << std::floor(100000.f * last_dtime) / 100.f
			   << "\nrtime: " << std::floor(100000.f * last_rtime) / 100.f
			   << "\nutime: "
			   << std::floor(100000.f * (last_dtime - last_rtime)) / 100.f
			   << "\n----------------------------"
			   << "\nCamera: " << std::floor(camera.pos.x * 10.f) / 10.f << ", "
			   << std::floor(camera.pos.y * 10.f) / 10.f << ", " << camera.pos.z
			   << " f " << camera.focal_z
			   << "\nZoom: " << std::floor(camera.zoom * 10.f) / 10.f
			   << "\nTime: " << time.getString()
			   << "\nPlot: " << (plot ? plot->name : "-")
			   << "\n----------------------------"
			   << "\nChunks: " << renderChunkCount << " / " << chunkCount
			   << "\nWorldVertices: " << vertexCount << "\n";

			hud_debug_info.setString(os.str().c_str());
		}

		hud_bg.setSize(
				sf::Vector2f(
						std::max(hud_debug_info.getLocalBounds().width, 200.f),
						hud_debug_info.getLocalBounds().height) +
				sf::Vector2f(10, 0));

		target.draw(hud_bg);
		target.draw(hud_debug_info);
	}

	auto windowSize = target.getSize();

	if (draw_graphs) {
		int y = 0;
		for (auto &graph : graphs) {
			auto graphSize = graph.getSize();
			graph.setPosition(windowSize.x - graphSize.x - 10, y + 10);
			y += graphSize.y;
			graph.regenVertexArray();
			target.draw(graph);
		}
	}

	if (draw_fps_graph) {
		auto graphSize = fps_graph.getSize();
		fps_graph.regenVertexArray();
		fps_graph.setPosition(10, windowSize.y - graphSize.y - bottomRight.y);
		target.draw(fps_graph);
	}

	int y = (int)target.getSize().y - 10 - bottomRight.y;
	if (debug_mode) {
		y -= fps_graph.getSize().y + 10;
	}

	chat.setPosition({10, y - chat.getSize().y});
	if (!chatShadow) {
		chatShadow = std::make_unique<sfext::DropShadow>(chat, chat.getSize(),
				sf::Vector2f(2.f, 2.f), sf::Color(0x0000007F));
	}
	target.draw(*chatShadow);
}

void HudView::update(float delta) {
	last_dtime = delta;
	display_refresh_counter += delta;
}

void HudView::resize(unsigned int width, unsigned int height) {
	chatShadow = nullptr;

	auto g_height = (unsigned int)((height - 80) / graphs.size());
	if (g_height > 100) {
		g_height = 100;
	}
	for (auto &graph : graphs) {
		graph.setHeight(g_height);
	}
}

void HudView::switchDebugMode() {
	debug_mode = (debug_mode + 1) % 2;
}
