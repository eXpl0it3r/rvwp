#pragma once
#include <SFML/Graphics.hpp>
#include <client/models/HeroEvent.hpp>
#include "client/views/View.hpp"
#include "content/Inventory.hpp"
#include "client/views/world/ParticleLayer.hpp"
#include <events/EventListener.hpp>

namespace client {

class WorldView;
class Game;
class HeroView : public View {
	Game *game;
	const sf::Font &font;
	WorldView *worldView;
	const EventListener<HeroEvent> eventListener;
	ParticleLayer *layer = nullptr;

public:
	HeroView(Game *game, const sf::Font &font, WorldView *worldView,
			EventBus<HeroEvent> *heroEvent)
			: game(game), font(font), worldView(worldView),
			  eventListener(heroEvent, [this](auto e) { handleEvent(e); }) {}

	bool load() override;
	void activate() override;
	void deactivate() override;

	void handleEvent(const HeroEvent &event);

	void draw(sf::RenderTarget &target, sf::RenderStates states) override {}
	void drawHUD(sf::RenderTarget &target, sf::RenderStates states) override;

private:
	void drawItemStack(sf::RenderTarget &target,
			const content::ItemStack &stack, int x, int y, bool count = true);
	void drawStatBar(sf::RenderTarget &target, float perc, sf::Color color,
			float width = 200);
};

} // namespace client
