#include "HeroView.hpp"
#include "client/ResourceManager.hpp"
#include "client/Game.hpp"

using namespace client;

bool HeroView::load() {
	layer = dynamic_cast<ParticleLayer *>(worldView->getLayer("Particle"));
	SanityCheck(layer);

	return View::load();
}

void HeroView::activate() {
	worldView->setCursor(game->getModeController()->getHero());
}

void HeroView::deactivate() {
	worldView->resetCursor();
}

void HeroView::handleEvent(const HeroEvent &event) {
	if (event.type == HeroEvent::RANGED) {
		float yaw = event.yaw * PI / 180.f - PI / 2.f;
		V3f direction(std::cos(yaw), std::sin(yaw), 0);
		layer->addSingleParticle(
				event.pos, direction * 50.f, content::Material("fire.png"));
	}
}

void HeroView::drawHUD(sf::RenderTarget &target, sf::RenderStates states) {
	auto size = sf::Vector2f(target.getSize());
	sf::View hud_view({0.f, 0.f, size.x, size.y});

	content::InventoryList *list =
			game->getInventory()->get("hotbar").value_or(nullptr);
	if (!list) {
		Log("HudView", ERROR) << "Invlist 'hotbar' does not exist!";
		return;
	}

	auto hudViewSize = hud_view.getSize();
	auto end = hudViewSize.x - 74;

	Entity *heroEntity = game->getModeController()->getHero()->getHeroEntity();
	if (heroEntity && heroEntity->hp < 100.f) {
		float width = std::max(size.x / 4.f, 100.f);
		drawStatBar(
				target, (float)heroEntity->hp / 100.f, sf::Color::Red, width);
	}

	drawItemStack(
			target, list->get(0), (int)end - 64 - 10, (int)hudViewSize.y - 74);
	drawItemStack(target, list->get(1), (int)end, (int)hudViewSize.y - 74);
}

void HeroView::drawItemStack(sf::RenderTarget &target,
		const content::ItemStack &stack, int x, int y, bool count) {
	if (stack.empty()) {
		sf::RectangleShape rect(sf::Vector2f(64, 64));
		rect.setFillColor(sf::Color::Transparent);
		rect.setOutlineThickness(1);
		rect.setOutlineColor(sf::Color::White);
		rect.move(sf::Vector2f(x, y));
		target.draw(rect);
		return;
	}

	content::ItemDef *idef = game->getDefMan()->getItemDef(stack.name);
	if (idef) {
		ClientMaterial cmat =
				game->getResourceManager()->getMaterialOrLoad(idef->material);
		cmat.draw(target, x, y);

		if (count) {
			std::ostringstream os;
			os << stack.count;
			sf::Text label = sf::Text(os.str().c_str(), font, 20);
			label.setPosition(
					sf::Vector2f(x + TILE_SIZE / 2.f, y + TILE_SIZE / 2.f));
			label.setFillColor(sf::Color::White);
			target.draw(label);
		}
	} else {
		Log("HudView", ERROR) << "No ItemDef found for content::ItemStack, cid="
							  << stack.name;
	}
}

void HeroView::drawStatBar(
		sf::RenderTarget &target, float perc, sf::Color color, float width) {
	auto size = sf::Vector2f(target.getSize());

	sf::RectangleShape bk{sf::Vector2f(width + 2, 12)};
	bk.setPosition(size.x / 2 - width / 2 - 1, size.y - 21);
	color.a = 0x33;
	bk.setFillColor(color);
	target.draw(bk);

	sf::RectangleShape bar{sf::Vector2f(width * perc, 10)};
	bar.setPosition(size.x / 2 - width / 2, size.y - 20);
	color.a = 0xCC;
	bar.setFillColor(color);
	target.draw(bar);
}
