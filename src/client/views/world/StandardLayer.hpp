#pragma once

#include "WorldView.hpp"

namespace client {

class StandardLayer : public IWorldRenderLayer {
	sf::Shader &worldShader;

public:
	StandardLayer(sf::Shader &worldShader)
			: IWorldRenderLayer("Standard"), worldShader(worldShader) {}

protected:
	void draw_impl(sf::RenderTarget &target, sf::RenderStates states,
			const client::RenderZLevel &level,
			const sf::FloatRect &cameraRect) override;
};

} // namespace client
