#pragma once
#include <SFML/Graphics.hpp>
#include "MeshGen.hpp"
#include "world/Raycast.hpp"
#include "world/Entity.hpp"
#include "world/World.hpp"
#include "content/content.hpp"
#include "client/sfext/SingleParticleEmitter.hpp"
#include "client/ResourceManager.hpp"
#include "client/views/View.hpp"
#include "client/input/ICursor.hpp"
#include "events/EventListener.hpp"
#include "world/WorldEvent.hpp"
#include "RenderLayer.hpp"
#include <string>

using namespace world;

namespace client {

class Game;
class ParticleLayer;
class WorldView : public View {
	Game *game;

	sf::Sprite s_crosshair;
	sf::Shader worldShader;
	sf::View camera_view{};

	std::map<int, RenderZLevel> renderZLevels;
	std::vector<std::unique_ptr<IWorldRenderLayer>> renderLayers;
	IWorldRenderLayer *lightDebugLayer;
	IWorldRenderLayer *debugLayer;
	ParticleLayer *particleLayer;

	ICursor *cursor = nullptr;

	const EventListener<world::WorldEvent> listener;

	int numChunksMeshGenned = 0;
#ifdef NDEBUG
	int renderMode = 0;
#else
	int renderMode = 1;
#endif

public:
	using Ptr = std::shared_ptr<WorldView>;

	WorldView(Game *game, const sf::Font &font,
			EventBus<world::WorldEvent> *worldBus);

	bool load() override;

	/// Render the world
	void draw(sf::RenderTarget &target, sf::RenderStates states) override;

	void update(float dtime) override;

	void setCursor(ICursor *v) { cursor = v; }

	void resetCursor() { cursor = nullptr; }

	/// Build meshes to represent the world.
	///
	/// Appends data to render_data.
	void buildMesh(WorldChunk *chunk);

	void registerLayer(std::unique_ptr<IWorldRenderLayer> &&layer) {
		renderLayers.emplace_back(std::move(layer));
	}

	IWorldRenderLayer *getLayer(const std::string &name) {
		for (auto &layer : renderLayers) {
			if (layer->name == name) {
				return layer.get();
			}
		}

		return nullptr;
	}

	void rebuildLayer(IWorldRenderLayer *layer);

	sf::Shader &getWorldShader() { return worldShader; }

	V3f getWorldPositionFromScreen(sf::RenderWindow *window, V2s pos);

	void switchRenderMode() {
		renderMode = (renderMode + 1) % 5;
		if (renderMode == 1) {
			debugLayer->enable();
		} else {
			debugLayer->disable();
		}
		if (renderMode == 2) {
			lightDebugLayer->enable();
		} else {
			lightDebugLayer->disable();
		}
	}

	int getRenderChunkCount() const {
		int counter = 0;
		for (const auto &col : renderZLevels) {
			counter += col.second.size();
		}
		return counter;
	}

	int getVertexCount() {
		int counter = 0;
		for (const auto &col : renderZLevels) {
			for (const auto &layer : col.second.peek()) {
				counter += layer.tilesMesh.getVertexCount();
				counter += layer.floorMesh.getVertexCount();
			}
		}
		return counter;
	}

	int getChunkMeshGenAndReset() {
		int ret = numChunksMeshGenned;
		numChunksMeshGenned = 0;
		return ret;
	}

	void onWorldEvent(const WorldEvent &event);
	void spawnSingleParticle(const V3f &pos, const V3f &velocity,
			const content::Material &material);
};

} // namespace client
