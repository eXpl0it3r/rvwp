#include "EntityLayer.hpp"
#include "client/ResourceManager.hpp"

using namespace client;

void EntityLayer::draw_impl(sf::RenderTarget &target, sf::RenderStates states,
		const RenderZLevel &level, const sf::FloatRect &cameraRect) {

	for (const auto &r : level.peek()) {
		for (auto e : r.chunk->entities) {
			assert(level.z == e->getPosition().z);

			auto material =
					resources->getMaterialOrLoad(e->properties.material);
			sf::Sprite sprite = material.toSprite();
			if (!material.texture) {
				static sf::Texture t_err;
				static bool done = false;
				if (!done && !t_err.loadFromFile("assets/textures/error.png")) {
					Log("EntityLayer", ERROR)
							<< "Error loading error texture :/";
				}
				done = true;
				sprite.setTexture(t_err);
				sprite.setTextureRect(sf::IntRect(0, 0, TILE_SIZE, TILE_SIZE));
			}

			const sf::Texture *text = sprite.getTexture();
			sprite.setOrigin(
					sf::Vector2f(text->getSize().x / 2, text->getSize().y / 2));
			sprite.setPosition(e->getPosition() * TILE_SIZE);
			sprite.setRotation(e->getYaw());

			target.draw(sprite);
		}
	}
}
