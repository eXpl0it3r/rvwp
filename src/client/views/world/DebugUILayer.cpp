#include "client/sfext/HexToColor.hpp"
#include "DebugUILayer.hpp"
#include "world/DebugUI.hpp"

using namespace client;
using namespace debug;

void DebugUILayer::draw_impl(sf::RenderTarget &target, sf::RenderStates states,
		const RenderZLevel &level, const sf::FloatRect &cameraRect) {
#ifdef DEBUG_UI_ENABLED
	sf::Text text("", font, 16);
	text.setFillColor(sf::Color::White);

	sf::RectangleShape text_bg;
	text_bg.setFillColor(sf::Color(0, 0, 0, 128));

	sf::VertexArray lines(sf::PrimitiveType::Lines);

	auto reader = DebugUI().getReader();
	for (const auto &element : reader.get()) {
		switch (element.type) {
		case DebugElementType::LABEL_3D: {
			sf::Vector2f pos = element.pos * TILE_SIZE;

			auto sfString = sf::String::fromUtf8(
					element.arg1.begin(), element.arg1.end());
			text.setString(sfString);
			text.setPosition(pos);
			auto bounds = text.getLocalBounds();
			sf::Vector2f textSize = {
					text.getLocalBounds().width, text.getLocalBounds().height};
			text.setOrigin(
					sf::Vector2f(bounds.left, bounds.top) + textSize * 0.5f);

			text_bg.setSize(textSize + sf::Vector2f(10.f, 10.f));
			text_bg.setPosition(pos);
			text_bg.setOrigin(text_bg.getSize() * 0.5f);
			target.draw(text_bg);

			target.draw(text);

			break;
		}
		case DebugElementType::LINE: {
			sf::Vector2f from = element.pos * TILE_SIZE;
			sf::Vector2f to = element.pos2 * TILE_SIZE;

			sf::Color color = sf::Color::White;
			sfe::HexToColor(element.arg1, color);

			lines.append(sf::Vertex(from, color));
			lines.append(sf::Vertex(to, color));
			break;
		}
		}
	}

	target.draw(lines);
#endif
}

void DebugUILayer::enable() {
	IWorldRenderLayer::enable();

	DebugUI().setEnabled(true);
}

void DebugUILayer::disable() {
	IWorldRenderLayer::disable();

	DebugUI().setEnabled(false);
}
