#pragma once

#include "../world/WorldView.hpp"
#include "tools/ITool.hpp"

namespace client {

class ArchitectController;
class ArchitectLayer : public IWorldRenderLayer {
	sf::Shader &worldShader;
	const sf::Texture *tileset;
	world::World *world;
	ResourceManager *resources;
	content::DefinitionManager *def;
	std::unique_ptr<ITool> tool;
	ArchitectController *controller;

public:
	ArchitectLayer(sf::Shader &worldShader, const sf::Texture *tileset,
			World *world, ResourceManager *resources,
			content::DefinitionManager *def, ArchitectController *controller)
			: IWorldRenderLayer("Architect"), worldShader(worldShader),
			  tileset(tileset), world(world), resources(resources), def(def),
			  controller(controller) {}

	void build(RenderChunk &chunk) override;

	void setTool(std::unique_ptr<ITool> &&v) { tool = std::move(v); }

	void update(float dtime);

	bool hasTool() const { return (bool)tool; }

protected:
	void draw_impl(sf::RenderTarget &target, sf::RenderStates states,
			const RenderZLevel &level,
			const sf::FloatRect &cameraRect) override;
};

} // namespace client
