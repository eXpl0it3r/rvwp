#include "ArchitectLayer.hpp"

#include "client/controllers/architect/ArchitectController.hpp"

using namespace client;

void ArchitectLayer::draw_impl(sf::RenderTarget &target,
		sf::RenderStates states, const client::RenderZLevel &level,
		const sf::FloatRect &cameraRect) {
	states.texture = tileset;

	auto plot = controller->getPlot();

	for (const auto &r : level.peek()) {
		sf::FloatRect chunk_rect(r.getPos().x * CHUNK_SIZE * TILE_SIZE,
				r.getPos().y * CHUNK_SIZE * TILE_SIZE, CHUNK_SIZE * TILE_SIZE,
				CHUNK_SIZE * TILE_SIZE);
		if (!cameraRect.intersects(chunk_rect)) {
			continue;
		}

		sf::Transform tf;
		tf.translate(r.getPos().x * CHUNK_SIZE * TILE_SIZE,
				r.getPos().y * CHUNK_SIZE * TILE_SIZE);
		states.transform = tf;
		worldShader.setUniform("lightmap", r.lightMapTexture);
		target.draw(r.architectMesh, states);
	}

	if (plot) {
		std::vector<Work *> workorders;
		plot->getAllWork(workorders);
		for (const auto &work : workorders) {
			if (work->bounds.minPos.z <= level.z &&
					work->bounds.getMaxPos().y >= level.z &&
					work->itemName.empty()) {
				auto cmat = resources->getMaterialOrLoad(work->material);
				auto sprite = cmat.toSprite();
				auto bounds = cmat.texture
						? sf::Vector2f(cmat.texture->getSize())
						: sf::Vector2f();
				sprite.setPosition(work->bounds.minPos * TILE_SIZE);
				sprite.setScale({(float)TILE_SIZE / bounds.x,
						(float)TILE_SIZE / bounds.y});
				target.draw(sprite);
			}
		}

		sf::RectangleShape border{plot->bounds.size * TILE_SIZE};
		border.setPosition(plot->bounds.minPos * TILE_SIZE);
		border.setOutlineColor(sf::Color(0xFFFFFF99));
		border.setOutlineThickness(2);
		border.setFillColor(sf::Color::Transparent);
		target.draw(border);
	}

	if (tool) {
		tool->draw(target, states);
	}
}

void ArchitectLayer::build(RenderChunk &rchunk) {
	rchunk.architectMesh.clear();

	auto plot = controller->getPlot();
	if (!plot) {
		return;
	}

	Rect3s chunk_rect(CPosToPos(rchunk.getPos()), {CHUNK_SIZE, CHUNK_SIZE, 1});
	if (!plot->intersects(chunk_rect)) {
		return;
	}

	WorldChunk *chunk = rchunk.chunk;
	std::vector<Work *> workorders;
	plot->getWorkInBounds(workorders, chunk_rect);
	if (workorders.empty()) {
		return;
	}

	// Initialise variables
	MeshGen meshGen(def, resources, world);

	std::array<WorldTile, CHUNK_SIZE * CHUNK_SIZE> tiles;
	tiles.fill({});

	for (const auto &work : workorders) {
		if (work->bounds.intersects(chunk_rect) && !work->itemName.empty()) {
			assert(work->bounds.minPos.z == chunk->pos.z);
			const auto idx = chunk->getIndex(work->getPosition());
			tiles[idx].cid = def->getCidFromName(work->itemName);
		}
	}

	ChunkConnectionMap bits;
	std::fill(bits.begin(), bits.end(), 0);

	// Generate meshes
	meshGen.spreadConnections(bits, &chunk->tiles[0]);
	meshGen.spreadConnections(bits, &tiles[0]);
	meshGen.buildTileMesh(
			rchunk.architectMesh, &tiles[0], CPosToPos(chunk->pos), bits);
}

void ArchitectLayer::update(float dtime) {
	if (tool) {
		tool->update_if_enabled(dtime);
	}
}
