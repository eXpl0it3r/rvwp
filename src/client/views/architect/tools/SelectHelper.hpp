#pragma once

#include <optional>
#include <SFML/Graphics.hpp>

#include "types/Vector.hpp"
#include "content/ToolSpec.hpp"
#include "client/input/ICursor.hpp"

namespace client {

class SelectHelper {
	content::ToolSelectType selectType;
	V3s fixedSize;
	ICursor *cursor;
	sf::Color tint;
	std::optional<V3s> selectionStart{};

public:
	SelectHelper(content::ToolSelectType selectType, V3s fixedSize,
			ICursor *cursor, sf::Color tint)
			: selectType(selectType), fixedSize(fixedSize), cursor(cursor),
			  tint(tint) {}

	void draw(sf::RenderTarget &target, sf::RenderStates states);
	std::vector<Rect3s> getSelectedRects();

	void start() {
		auto optCursor = cursor->getCursorPosition();
		if (optCursor.has_value()) {
			selectionStart = optCursor.value().floor();
		}
	}

	void stop() { selectionStart = {}; }
};

} // namespace client
