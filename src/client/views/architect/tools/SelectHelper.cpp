#include "SelectHelper.hpp"

using namespace client;
using namespace content;

void SelectHelper::draw(sf::RenderTarget &target, sf::RenderStates states) {
	auto optCursor = cursor->getCursorPosition();
	if (!selectionStart.has_value() || !optCursor.has_value()) {
		return;
	}

	for (const auto &rect : getSelectedRects()) {
		sf::RectangleShape shape({static_cast<float>(rect.size.x * TILE_SIZE),
				static_cast<float>(rect.size.y * TILE_SIZE)});
		sf::Color color = tint;
		color.a = 0x33;
		shape.setFillColor(sf::Color(color));
		shape.setPosition(rect.minPos * (float)TILE_SIZE);
		target.draw(shape);
	}

	{
		V3s cursorPos = optCursor.value().floor();
		V3s start = selectionStart.value();
		auto min = cursorPos.min(start);
		auto max = cursorPos.max(start) + V3s(1, 1, 0);
		auto size = max - min;

		sf::RectangleShape shape({static_cast<float>(size.x * TILE_SIZE),
				static_cast<float>(size.y * TILE_SIZE)});
		sf::Color color = tint;
		color.a = 0x99;
		shape.setFillColor(sf::Color::Transparent);
		shape.setOutlineColor(sf::Color(color));
		shape.setOutlineThickness(4);
		shape.setPosition(min * (float)TILE_SIZE);
		target.draw(shape);
	}
}

std::vector<Rect3s> SelectHelper::getSelectedRects() {
	std::vector<Rect3s> retval;

	auto optCursor = cursor->getCursorPosition();
	if (!selectionStart.has_value() || !optCursor.has_value()) {
		return retval;
	}

	V3s cursorPos = optCursor.value().floor();
	V3s start = selectionStart.value();

	auto add = [&](const V3s &a, const V3s &b) {
		retval.emplace_back(Rect3s::fromInclusive(a, b));
	};

	auto from = cursorPos.min(start);
	auto to = cursorPos.max(start);

	switch (selectType) {
	case ToolSelectType::FILLED:
		add(from, to);
		break;
	case ToolSelectType::BORDER:
		if (to.y > from.y + 1 && to.x > from.x + 1) {
			// Top
			add(from, V3s(to.x, from.y, to.z));
			// Bottom
			add(V3s(from.x, to.y, from.z), to);
			// Left
			add(from + V3s(0, 1, 0), V3s(from.x, to.y - 1, to.z));
			// Right
			add(V3s(to.x, from.y + 1, from.z), to - V3s(0, 1, 0));
		} else {
			add(from, to);
		}
		break;
	case ToolSelectType::FIXED:
		add(from, from + fixedSize - V3s(1, 1, 1));
		break;
	}

	return retval;
}
