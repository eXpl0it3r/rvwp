#pragma once

#include "client/input/InputConnection.hpp"
#include "ITool.hpp"
#include "content/ToolSpec.hpp"
#include "content/content.hpp"
#include "SelectHelper.hpp"

namespace client {

class ArchitectController;
class BuildTool : public ITool {
	ArchitectController *controller;
	const content::TileDef *def;
	content::ToolSpec spec;
	std::vector<InputConnection> inputConnections;
	SelectHelper selectHelper;

public:
	BuildTool(ArchitectController *controller, const content::TileDef *def,
			const content::ToolSpec &spec);

	const content::ToolSpec &getSpec() override { return spec; }

	void draw(sf::RenderTarget &target, sf::RenderStates states) override {
		selectHelper.draw(target, states);
	}

	void update(float dtime) override {}

private:
	void onSelect();
	void onDeselect();
};

} // namespace client
