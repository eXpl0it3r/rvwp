#pragma once
#include <SFML/Graphics.hpp>
#include <TGUI/TGUI.hpp>

#include "types/Controller.hpp"
#include "input/IInput.hpp"
#include "server/ServerSpec.hpp"

namespace client {

class MainMenu {
	sf::RenderWindow *rwindow;
	tgui::Gui &gui;
	sf::Font &font;
	IInputController *input;

	tgui::ListBox::Ptr servers;
	tgui::Button::Ptr btnStart;
	tgui::EditBox::Ptr txtUser, txtPass, txtAddress, txtPort;
	tgui::CheckBox::Ptr chkHost;

	std::vector<InputConnection> inputConnections;

public:
	MainMenu(sf::RenderWindow *rwindow, tgui::Gui &gui, sf::Font &font,
			IInputController *input);

	MainMenu(const MainMenu &other) = delete;

	bool load();
	bool run();
	bool onEvent(const sf::Event &event);

	void onConnectClick();
	void onFieldChange();
	void onServerListChange(const tgui::String &id);

	struct ClientSpec {
		std::string username;
		std::string password;
		std::string address;
		unsigned int port;
		bool startServer = false;

		server::ServerSpec server;

		inline bool empty() const { return username.empty(); }
		void describe() const;
	};

	ClientSpec spec;
};

} // namespace client
