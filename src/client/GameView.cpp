#include "GameView.hpp"

#include "views/InvView.hpp"

#include <functional>

using namespace client;

GameView::GameView(Game *game, EventBus<world::WorldEvent> *worldBus,
		EventBus<HeroEvent> *heroEvent,
		EventBus<content::InventoryCommand> *inventoryBus,
		content::DefinitionManager *def, ResourceManager *resources,
		tgui::Gui &gui)
		: heroListener(heroEvent, [this](auto e) { handleHeroEvent(e); }) {
	worldView = std::make_shared<WorldView>(
			game, resources->getDefaultFont(), worldBus);

	hudView = std::make_shared<HudView>(
			game, resources->getDefaultFont(), worldView.get());

	auto makeTwo = [&](View::Ptr front) {
		return std::make_unique<StackView>(worldView, front);
	};

	auto makeWithHUD = [&](View::Ptr front) {
		return std::make_unique<StackView>(worldView, hudView, front);
	};

	invView = makeTwo(std::make_shared<InvView>(game, inventoryBus, gui));

	chatSendView = makeTwo(std::make_shared<ChatSendView>(
			game, gui, resources->getDefaultFont()));

	archView = makeWithHUD(std::make_shared<ArchitectView>(game,
			worldView.get(), hudView.get(), gui, worldView->getWorldShader()));

	heroView = makeWithHUD(std::make_shared<HeroView>(
			game, resources->getDefaultFont(), worldView.get(), heroEvent));
}

bool GameView::setRootView(View::Ptr newctr) {
	if (rootView) {
		rootView->deactivate();
	}

	hudView->setViewBoxing(0);

	rootView = std::move(newctr);
	rootView->activate();

	return true;
}

void GameView::draw(sf::RenderTarget &target, sf::RenderStates states) {
	rootView->draw(target, states);
}

void GameView::drawHUD(sf::RenderTarget &target, sf::RenderStates states) {
	rootView->drawHUD(target, states);
}

void GameView::drawPostGUI(sf::RenderTarget &target, sf::RenderStates states) {
	rootView->drawPostGUI(target, states);
}

void GameView::update(float dtime) {
	rootView->update(dtime);
}

bool GameView::load() {
	if (!(hudView->load() && invView->load() && chatSendView->load() &&
				worldView->load() && heroView->load() && archView->load())) {
		return false;
	}

	switchToHeroMode();
	return true;
}

void GameView::resize(unsigned int width, unsigned int height) {
	heroView->resize(width, height);
	archView->resize(width, height);
	hudView->resize(width, height);
	invView->resize(width, height);
	chatSendView->resize(width, height);
	worldView->resize(width, height);
}

bool GameView::isMainViewActive() const {
	return rootView == mainView;
}

void GameView::switchToChatSendView(const std::string &msg) {
	chatSendView->getFront<ChatSendView>()->setText(msg);
	setRootView(chatSendView);
}

std::function<V3f(V2s)> GameView::getScreenToWorldMapper(
		sf::RenderWindow *window) const {
	return std::bind(&WorldView::getWorldPositionFromScreen, worldView.get(),
			window, std::placeholders::_1);
}

void GameView::handleHeroEvent(const HeroEvent &event) {
	if (event.type == HeroEvent::SHOW_INVENTORY) {
		switchToInvView();
	}
}

bool GameView::exit() {
	return rootView->exit();
}
