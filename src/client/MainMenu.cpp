#include "MainMenu.hpp"
#include <string>
#include <log.hpp>
#include "build_info.hpp"
#include "sfext/FancyText.hpp"

using namespace client;

static const std::string help_text = std::string("RVWP ") +
		BuildInfo::VERSION_GIT_SHA1 +
		"\n\n"
		"Switch mode: F7\n"
		"Move: WASD\n"
		"Camera slice: PgUp / PgDn\n"
		"Hero mode:\n"
		"    Use item: Left/right-click\n"
		"    Jump: Space\n"
		"    Sneak: Shift\n"
		"    Inventory: Tab\n"
		"Architect mode:\n"
		"    Use tool: Left-click\n"
		"    Release tool: Right-click";

MainMenu::MainMenu(sf::RenderWindow *rwindow, tgui::Gui &gui, sf::Font &font,
		IInputController *input)
		: rwindow(rwindow), gui(gui), font(font), input(input) {
	input->setActionStateProvider([&]() { return GUI; });

	auto bind = [&](Action action, std::function<void()> func) {
		inputConnections.emplace_back(input->connect(action, func));
	};

	bind(Action::GUI_SELECT, [&]() { onConnectClick(); });
}

bool MainMenu::load() {
	auto window = tgui::ChildWindow::create("RVWP");
	window->setPosition("&.width / 2 - width / 2", "&.height / 2 - height / 2");
	window->setSize("max(600, min(&.width, &.height) * 5 / 6)",
			"max(300, min(&.width, &.height) * 0.5) + 32");
	window->setPositionLocked(true);
	window->setTitleTextSize(17);
	gui.add(window);

	auto close = [&]() {
		rwindow->close();
		return true;
	};
	window->onClose(close);
	window->onEscapeKeyPress(close);

	auto help = tgui::Label::create(help_text);
	window->add(help);
	help->setPosition("10", "100% - height - 10");
	help->setSize("50% - 20", "50%");
	help->setVerticalAlignment(tgui::Label::VerticalAlignment::Bottom);

	servers = tgui::ListBox::create();
	window->add(servers, "listServers");
	servers->setPosition(0, 0);
	servers->setSize("50%", "50%");
	servers->addItem("Start game");
	servers->addItem("Join localhost");
	servers->addItem("Join rubenwardy.com");
	servers->setSelectedItemByIndex(0);
	servers->onItemSelect(&MainMenu::onServerListChange, this);

	txtAddress = tgui::EditBox::create();
	window->add(txtAddress, "txtAddress");
	txtAddress->onTextChange(&MainMenu::onFieldChange, this);

	txtPort = tgui::EditBox::create();
	window->add(txtPort, "txtPort");
	txtPort->onTextChange(&MainMenu::onFieldChange, this);

	chkHost = tgui::CheckBox::create("Start game");
	window->add(chkHost);

	txtUser = tgui::EditBox::create();
	window->add(txtUser, "txtUser");
#ifndef NDEBUG
	txtUser->setText("rubenwardy");
#endif
	txtUser->onTextChange(&MainMenu::onFieldChange, this);

	txtPass = tgui::EditBox::create();
	window->add(txtPass, "txtPass");
	txtPass->setPasswordCharacter('*');
#ifndef NDEBUG
	txtPass->setText("p");
#endif
	txtPass->onTextChange(&MainMenu::onFieldChange, this);

	btnStart = tgui::Button::create("Start Game");
	window->add(btnStart, "btnStart");
	btnStart->onPress(&MainMenu::onConnectClick, this);

	btnStart->setPosition("50% + 10", "100% - height - 10");
	btnStart->setSize("50% - 20", "44");
	txtAddress->setPosition("btnStart.left", "10");
	txtAddress->setSize("btnStart.width * 2 / 3 - 4", "30");
	txtAddress->setTextSize(13);
	chkHost->setPosition("btnStart.left", "txtAddress.bottom + 10");
	chkHost->setSize(24, 24);
	txtPass->setPosition("btnStart.left", "btnStart.top - height - 10");
	txtPass->setSize("btnStart.width", "txtAddress.height");
	txtPass->setTextSize(13);
	txtUser->setPosition("btnStart.left", "txtPass.top - height - 10");
	txtUser->setSize("btnStart.width", "txtAddress.height");
	txtUser->setTextSize(13);
	txtPort->setPosition("txtAddress.right + 10", "10");
	txtPort->setSize("btnStart.width * 1 / 3 - 4", "txtAddress.height");
	txtPort->setTextSize(13);

	if (txtUser->getText().empty()) {
		gui.unfocusAllWidgets();
		txtUser->setFocused(true);
	} else if (txtPass->getText().empty()) {
		gui.unfocusAllWidgets();
		txtPass->setFocused(true);
	}

	onFieldChange();
	onServerListChange(servers->getSelectedItem());

	return true;
}

bool MainMenu::run() {
	sf::Clock dtimeClock;
	while (rwindow->isOpen() && spec.empty()) {
		sf::Event event;
		while (rwindow->pollEvent(event)) {
			onEvent(event);
		}

		rwindow->clear(sf::Color(sf::Uint32(0x336699FF)));

		sfext::FancyText label("RVWP", font, 24);
		label.setFillColor(sf::Color::White);
		const auto size = rwindow->getSize();
		label.setPosition(sf::Vector2f(size.x / 2, size.y * 0.1f));
		sf::Vector2f textRect = label.getSize();
		label.setOrigin(textRect.x / 2.0f, textRect.y / 2.0f);
		rwindow->draw(label);
		gui.draw();

		rwindow->display();

		float dtime = dtimeClock.restart().asSeconds();
		input->update(dtime);
	}

	return !spec.empty();
}

bool MainMenu::onEvent(const sf::Event &event) {
	if (event.type == sf::Event::Closed ||
			(event.type == sf::Event::KeyReleased &&
					event.key.code == sf::Keyboard::Escape)) {
		rwindow->close();
	} else if (event.type == sf::Event::Resized) {
		const auto size = rwindow->getSize();
		const sf::FloatRect rect = sf::FloatRect(0.f, 0.f, size.x, size.y);
		rwindow->setView(sf::View(rect));
		gui.setAbsoluteView(tgui::FloatRect(rect));
	}

	auto handleReturn = [&]() {
		if (event.type == sf::Event::KeyReleased &&
				event.key.code == sf::Keyboard::Return) {
			onConnectClick();
			return true;
		}
		return false;
	};

	if (event.type == sf::Event::KeyPressed ||
			event.type == sf::Event::KeyReleased) {
		return input->onEvent(event) || gui.handleEvent(event) ||
				handleReturn();
	} else {
		return gui.handleEvent(event) || input->onEvent(event) ||
				handleReturn();
	}
}

void MainMenu::onConnectClick() {
	bool hostServer = chkHost->isChecked() && chkHost->isVisible();

	spec.username = txtUser->getText().toStdString();
	spec.password = txtPass->getText().toStdString();

	spec.address = txtAddress->getText().toStdString();
	spec.port = (unsigned int)std::stoul(txtPort->getText().toStdString());
	spec.startServer = hostServer;

	spec.server.shutdown_on_last_leave = hostServer;
}

void MainMenu::onFieldChange() {
	bool valid = !txtPass->getText().empty() && !txtUser->getText().empty() &&
			!txtAddress->getText().empty();
	btnStart->setEnabled(valid);

	bool isLocalhost = txtAddress->getText() == "127.0.0.1" ||
			txtAddress->getText() == "0.0.0.0" ||
			txtAddress->getText() == "localhost";

	chkHost->setVisible(isLocalhost);
}

void MainMenu::onServerListChange(const tgui::String &id) {
	if (id == "Start game") {
		txtAddress->setText("127.0.0.1");
		txtPort->setText("30100");
		chkHost->setChecked(true);
#ifndef NDEBUG
		txtUser->setText("rubenwardy");
#endif
	} else if (id == "Join localhost") {
		txtAddress->setText("127.0.0.1");
		txtPort->setText("30100");
		chkHost->setChecked(false);
#ifndef NDEBUG
		txtUser->setText("player2");
#endif
	} else if (id == "Join rubenwardy.com") {
		txtAddress->setText("194.36.147.174");
		txtPort->setText("30100");
		chkHost->setChecked(false);
	} else {
		if (!id.empty()) {
			LogF(ERROR, "[Mainmenu] Unknown serverlist entry %s",
					id.toStdString().c_str());
		}
		servers->setSelectedItemByIndex(0);
	}

	onFieldChange();
}

void MainMenu::ClientSpec::describe() const {
	Log("Mainmenu", INFO) << "Connecting to " << address << ":" << (int)port
						  << " as " << username << " (password=" << password
						  << ")";
}
