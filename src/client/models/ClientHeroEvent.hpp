#pragma once

#include "../../types/types.hpp"
#include "content/Inventory.hpp"

namespace world {
class Entity;
}

namespace client {

struct ClientHeroEvent {
	enum Type {
		/// Replace inventory
		SET_INVENTORY,

		/// Set the entity that is being controlled
		SET_ENTITY,

		/// Teleport to a position
		TELEPORT
	};

	Type type;
	V3f pos;

	/// WARNING: this transfers ownership
	/// TODO: don't risk leaking memory
	content::Inventory *inv;

	world::Entity *entity;

	static ClientHeroEvent setInventory(content::Inventory *inv) {
		return {SET_INVENTORY, {}, inv, nullptr};
	}

	static ClientHeroEvent setEntity(world::Entity *entity) {
		return {SET_ENTITY, {}, nullptr, entity};
	}

	static ClientHeroEvent teleport(V3f pos) {
		return {TELEPORT, pos, nullptr, nullptr};
	}
};

} // namespace client
