#include "Client.hpp"
#include "Game.hpp"
#include "client/controllers/hero/HeroEntityController.hpp"
#include "world/World.hpp"
#include "../content/content.hpp"
#include <log.hpp>
#include <assert.h>

using namespace network;
using namespace client;
using namespace world;
using namespace content;

#define B(func) std::bind(&Client::func, this, _1)

void Client::initPacketHandlers() {
	using client::Client;
	using namespace std::placeholders;

	// clang-format off

	PacketRoutingTable table = {
			nullptr_PKT,                    // TONONE_nullptr
			nullptr_PKT,                    // TOSERVER_HELLO
			B(handleHello),                 // TOCLIENT_HELLO
			B(handleReady),                 // TOCLIENT_READY
			B(handleDisconnect),            // TOANY_DISC
			B(handleItemDefinition),        // TOCLIENT_ITEM_DEFINITION
			B(handleToolDefinition),    // TOCLIENT_TOOL_DEFINITION
			B(handleProfile),               // TOCLIENT_PROFILE
			B(handleInventory),             // TOCLIENT_PLAYER_INVENTORY
			nullptr_PKT,
			B(handleChatMessage),
			B(handleSetTime),               // TOCLIENT_SET_TIME
			B(handleWorldBlock),            // TOCLIENT_WORLD_BLOCK
			B(handleWorldEntityNew),        // TOCLIENT_WORLD_NEW_ENTITY
			B(handleWorldEntityUpdate),     // TOCLIENT_WORLD_UPDATE_ENTITY
			B(handleWorldEntityDelete),     // TOCLIENT_WORLD_DELETE_ENTITY
			B(handleWorldGoto),             // TOCLIENT_WORLD_GOTO
			nullptr_PKT,
			nullptr_PKT,
			nullptr_PKT,
			B(handleWorldSetTile),
			B(handleWorldPlot),
			B(handleWorldNewWork),          // TOANY_WORLD_NEW_WORK
			B(handleWorldRemoveWork),       // TOANY_WORLD_REMOVE_WORK
			nullptr_PKT,                    // TOSERVER_FIRE_WEAPON
			B(handleSpawnParticle)          // TOCLIENT_SPAWN_PARTICLE
	};

	// clang-format on

	socket.setHandlers(table);
}

#define REQ_STATE(st)                                                          \
	if (state != (st)) {                                                       \
		Log("Client", ERROR) << "Unexpected packet received in wrong state, "  \
							 << "requires State " << (st);                     \
		return;                                                                \
	}

#define REQ_LOADED() REQ_STATE(LOADED)

void Client::handleHello(Packet &pkt) {
	REQ_STATE(CONNECTING)

	// Validate packet.
	if (pkt.size() == 1) {
		Log("Client", ERROR)
				<< "Incorrect size for TOCLIENT_HELLO packet, must not be "
				<< pkt.size() << " bytes long. Packet ignored.";
		return;
	}

	// Get protocol version.
	u16 protocol_v = 0;
	pkt >> protocol_v;
	Log("Client", INFO) << "Connected! (server protocol_v=" << protocol_v
						<< ")";

	// Get MOTD.
	std::string motd;
	pkt >> motd;
	Log("Client", INFO) << "MOTD: '" << motd << "'";
	assert(pkt.end());

	game->displayMessageToChat(motd);

	state = LOADING;
}

void Client::handleReady(Packet &pkt) {
	REQ_STATE(LOADING)

	Log("Client", INFO) << "Done loading.";
	state = LOADED;
	assert(pkt.end());
}

void Client::handleDisconnect(Packet &pkt) {
	if (pkt.size() > 0) {
		u8 error_type;
		std::string err_msg;
		u8 reconnect;
		pkt >> error_type;
		pkt >> err_msg;
		pkt >> reconnect;
		Log("Client", INFO) << "Server -!- " << err_msg;
	}

	assert(pkt.end());
	state = DISCONNECTED;
}

void Client::handleItemDefinition(Packet &pkt) {
	auto defman = game->getDefMan();
	if (defman->isLocked()) {
		LogF(ERROR,
				"[Client] Unable to receive item definition, the defman is "
				"locked");
		assert(false);
		return;
	}

	u8 utype;
	std::string name;
	std::string title;
	Material material;
	pkt >> utype >> name >> title;
	material.deserialise(pkt);

	u8 uweaponType;
	pkt >> uweaponType;

	WeaponSpec weapon_spec{};
	weapon_spec.type = static_cast<WeaponSpec::Type>(uweaponType);
	if (weapon_spec.type != WeaponSpec::Type::None) {
		pkt >> weapon_spec.range >> weapon_spec.damage >>
				weapon_spec.useInterval;
	}

	ItemType type = static_cast<ItemType>(utype);
	switch (type) {
	case ItemType::Item: {
		auto def = std::make_unique<ItemDef>(type);
		def->name = name;
		def->title = title;
		def->material = material;
		def->setWeaponSpec(weapon_spec);
		assert(pkt.end());

		defman->registerItem(def.release());
		break;
	}
	case ItemType::Terrain:
		// fallthrough
	case ItemType::Floor:
		// fallthrough
	case ItemType::Tile: {
		auto def = std::make_unique<TileDef>(type);
		def->name = name;
		def->title = title;
		def->material = material;
		def->setWeaponSpec(weapon_spec);

		u8 numCollisionBoxes;
		pkt >> def->id >> def->collides >> def->lightSource >>
				def->lightPropagates >> def->footstepSound >> def->friction >>
				numCollisionBoxes;

		def->collisionBoxes.clear();
		def->collisionBoxes.reserve(numCollisionBoxes);
		for (u8 i = 0; i < numCollisionBoxes; i++) {
			Rect3f box;
			pkt >> box;
			def->collisionBoxes.push_back(box);
		}

		assert(pkt.end());

		defman->registerTile(def.release());
		break;
	}
	default:
		LogF(ERROR, "[Client] Unknown item type %d", utype);
		break;
	}
}

void Client::handleToolDefinition(Packet &pkt) {
	auto defman = game->getDefMan();
	if (defman->isLocked()) {
		LogF(ERROR,
				"[Client] Unable to receive item definition, the defman is "
				"locked");
		assert(false);
		return;
	}

	u8 utype;
	std::string description;
	Material material;
	u8 uselectType;
	V3s fixedSelectionSize;
	std::string itemName;
	u8 ulayer;

	pkt >> utype >> description >> uselectType >> fixedSelectionSize >>
			itemName >> ulayer;
	material.deserialise(pkt);

	ToolType type = static_cast<ToolType>(utype);
	ToolSelectType selectType = static_cast<ToolSelectType>(uselectType);
	ChunkLayer layer = static_cast<ChunkLayer>(ulayer);

	defman->registerToolSpec({type, description, material, selectType,
			fixedSelectionSize, itemName, layer});
}

void Client::handleProfile(Packet &pkt) {
	REQ_STATE(LOADING)

	// Validate packet.
	Log("Client", INFO) << "Received profile:";
	if (pkt.size() < 2) {
		Log("Client", ERROR)
				<< "Incorrect size for TOCLIENT_PROFILE packet, was "
				<< pkt.size() << " should be 2 bytes or more. Packet ignored.";
		return;
	}

	// Read Username
	std::string t_username;
	pkt >> t_username;
	if (pkt.error() || t_username != username) {
		Log("Client", ERROR) << "TOCLIENT_PROFILE for wrong username ("
							 << t_username << "). Packet ignored.";
		return;
	}

	// Read position
	V3f pos;
	pkt >> pos;
	if (pkt.error()) {
		Log("Client", ERROR) << "Error reading position from TOCLIENT_PROFILE "
								"packet. Packet ignored.";
		return;
	}

	Log("Client", INFO) << "Pos: " << pos.x << ", " << pos.y << ", " << pos.z;

	assert(pkt.end());

	Material material("player.png");
	localPlayer = game->getWorld()->createEntity(
			pos, Entity::PLAYER_TYPE_NAME, material, -1);
	heroBus->push(ClientHeroEvent::setEntity(localPlayer));
}

void Client::handleInventory(Packet &pkt) {
	const int min_size = Inventory(InventoryLocation::FromPlayer(username))
								 .serialised_size();
	const int size = pkt.size();
	if (size < min_size) {
		Log("Client", ERROR)
				<< "Incorrect size for TOCLIENT_PLAYER_INVENTORY packet, was "
				<< size << " should be " << min_size
				<< " or bigger. Packet ignored.";
		return;
	}

	Inventory *inv = new Inventory(InventoryLocation::FromPlayer(username));
	inv->deserialise(pkt);
	assert(pkt.end() && !pkt.error());

	heroBus->push(ClientHeroEvent::setInventory(inv));
}

void Client::handleChatMessage(Packet &pkt) {
	std::string message;

	pkt >> message;
	assert(pkt.end());

	game->displayMessageToChat(message);
}

void Client::handleSetTime(Packet &pkt) {
	f32 timeSinceBeginning, timeOfDay, speed;
	pkt >> timeSinceBeginning;
	pkt >> timeOfDay;
	pkt >> speed;

	game->updateTime({timeSinceBeginning, timeOfDay, speed});
}

void Client::handleWorldBlock(Packet &pkt) {
	const int exp_size = 3 * 4 + 2 * CHUNK_SIZE * CHUNK_SIZE * (2 + 1) +
			2 * CHUNK_SIZE * CHUNK_SIZE + 2 * CHUNK_SIZE * CHUNK_SIZE;
	const int size = pkt.size();
	if (size != exp_size) {
		Log("Client", ERROR)
				<< "Incorrect size for TOCLIENT_WORLD_BLOCK packet, was "
				<< size << " should be " << exp_size << ". Packet ignored.";
		return;
	}

	V3s pos;
	pkt >> pos;

	world::World *world = game->getWorld();

	auto chunk = world->getOrCreateChunk(pos);
	chunk->clear();

	for (auto &tile : chunk->tiles) {
		u16 cid;
		u8 damage;
		pkt >> cid;
		pkt >> damage;

		tile.cid = cid;
	}

	for (auto &floor_tile : chunk->floort) {
		u16 cid;
		u8 damage;
		pkt >> cid;
		pkt >> damage;

		floor_tile.cid = cid;
	}

	for (content_id &cid : chunk->terrain) {
		pkt >> cid;
	}

	for (u16 &lightLevel : chunk->lightLevels) {
		pkt >> lightLevel;
	}

	assert(pkt.end());

	world->activateChunk(chunk);
}

void Client::handleWorldEntityNew(Packet &pkt) {
	int count = 0;
	while (!pkt.end()) {
		u32 id;
		V3f pos;
		f32 yaw;
		u16 hp;
		f32 radius;
		bool footsteps;
		Material material;

		pkt >> id;
		pkt >> pos;
		pkt >> yaw;
		pkt >> hp;
		material.deserialise(pkt);
		pkt >> radius;
		pkt >> footsteps;

		count++;

		auto e = game->getWorld()->createEntity(pos, "", material, id);
		e->hasAuthority = false;
		e->setYaw(yaw);
		e->hp = hp;
		e->properties.radius = radius;
		e->properties.footsteps = footsteps;
	}

	LogF(INFO, "[Client] %d new entities received", count);
}

void Client::handleWorldEntityUpdate(Packet &pkt) {
	int countE = 0;
	int countP = 0;
	while (!pkt.end()) {
		u32 id;
		pkt >> id;
		countE++;

		auto entity = game->getWorld()->getEntityById(id);
		if (!entity) {
			LogF(WARNING,
					"[Client] Received UPDATE_ENTITY with entity id=%d, which "
					"doesn't exist",
					id);
			continue;
		}

		while (true) {
			u16 propertyId;
			pkt >> propertyId;
			if (propertyId == 0) {
				break;
			}

			countP++;

			auto property = static_cast<Entity::Property>(propertyId - 1);
			switch (property) {
			case Entity::Property::Position: {
				V3f position;
				pkt >> position;

				entity->last_sent_position = position;

				// TODO: fix hack with z-level jumps
				if (entity->hasAuthority ||
						entity->getPosition().z !=
								entity->last_sent_position.z) {
					entity->setPosition(entity->last_sent_position);
				}

				break;
			}
			case Entity::Property::Yaw:
				pkt >> entity->last_sent_yaw;
				break;
			case Entity::Property::HP:
				pkt >> entity->hp;
				break;
			case Entity::Property::Material:
				entity->properties.material.deserialise(pkt);
				break;
			case Entity::Property::Radius:
				pkt >> entity->properties.radius;
				break;
			case Entity::Property::Footsteps:
				pkt >> entity->properties.footsteps;
				break;
			}
		}
	}

	/*LogF(WARNING,
			"Received UPDATE_ENTITY packet with %d entities and %d properties",
			countE, countP);*/
}

void Client::handleWorldEntityDelete(Packet &pkt) {
	const int exp_size = 4;
	const int size = pkt.size();
	if (size != exp_size) {
		Log("Client", ERROR)
				<< "Incorrect size for TOCLIENT_WORLD_DELETE_ENTITY packet, "
				   "was "
				<< size << " should be " << exp_size << ". Packet ignored.";
		return;
	}

	// TOCLIENT_WORLD_REMOVE_ENTITY,
	//    u32     id
	u32 id;
	pkt >> id;
	assert(pkt.end());

	Entity *e = game->getWorld()->getEntityById(id);
	if (e) {
		game->getWorld()->removeEntity(e);
		Log("Client", INFO) << "Removed entity " << id;
	} else {
		Log("Client", ERROR) << "No such entity " << id << " to remove!";
	}
}

void Client::handleWorldGoto(Packet &pkt) {
	const int exp_size = 3 * 4;
	const int size = pkt.size();
	if (size != exp_size) {
		Log("Client", ERROR)
				<< "Incorrect size for TOCLIENT_WORLD_GOTO packet, was " << size
				<< " should be " << exp_size << ". Packet ignored.";
		return;
	}

	V3f pos;
	pkt >> pos;
	assert(pkt.end());

	heroBus->push(ClientHeroEvent::teleport(pos));
}

void Client::handleWorldSetTile(Packet &pkt) {
	const int exp_size = 3 * 4 + 4;
	const int size = pkt.size();
	if (size != exp_size) {
		Log("Client", ERROR)
				<< "Incorrect size for TOANY_WORLD_SET_TILE packet, was "
				<< size << " should be " << exp_size << ". Packet ignored.";
		return;
	}

	V3s pos;
	u32 cid;
	pkt >> pos >> cid;
	assert(pkt.end());

	ChunkLayer layer = ChunkLayer::Tile;
	if (cid > 0) {
		const TileDef *def = game->getDefMan()->getTileDef((content_id)cid);
		layer = typeToChunkLayer(def->getType());
	}

	if (game->getWorld()->set(pos, layer, cid)) {
		Log("Client", INFO) << "Set tile(" << cid << ") at " << pos.x << ", "
							<< pos.y << ", " << pos.z;
	} else {
		Log("Client", ERROR) << "Failed to set tile at " << pos.x << ", "
							 << pos.y << ", " << pos.z;
	}
}

void Client::handleWorldPlot(Packet &pkt) {
	u32 id;
	std::string name;
	u16 count;

	pkt >> id >> name >> count;

	Plot *plot = game->getWorld()->createPlot(name, {}, id);

	for (int i = 0; i < count; i++) {
		Rect3s rect;
		pkt >> rect;

		plot->addBox(rect);
	}
}

void Client::handleWorldNewWork(Packet &pkt) {
	u32 serverId;
	u32 plotId;
	std::string type;
	Rect3s bounds;
	Material material;
	std::string itemName;
	u8 ulayer;

	pkt >> serverId >> plotId >> type >> bounds;
	material.deserialise(pkt);
	pkt >> itemName >> ulayer;

	world::ChunkLayer layer = static_cast<world::ChunkLayer>(ulayer);

	auto work = game->getWorld()->addWork(plotId,
			std::make_unique<Work>(type, bounds, material, itemName, layer));
	if (!work) {
		LogF(ERROR, "Error placing work from network");
		return;
	}

	workClientToServer[work->id] = serverId;
	workServerToClient[serverId] = work->id;
}

void Client::handleWorldRemoveWork(Packet &pkt) {
	u32 serverId;
	u32 clientId;

	pkt >> serverId >> clientId;

	if (clientId == 0 && serverId != 0) {
		clientId = workServerToClient[serverId];
	}

	if (clientId == 0) {
		LogF(ERROR,
				"[Client] Failed to remove work: Unable to find clientId from "
				"serverId=%d",
				serverId);
		return;
	}

	if (!game->getWorld()->removeWork(clientId)) {
		LogF(ERROR,
				"[Client] Failed to remove work: Unable to find work from "
				"clientId=%d",
				clientId);
		return;
	}

	workClientToServer.erase(clientId);
	workServerToClient.erase(serverId);
}

void Client::handleSpawnParticle(Packet &pkt) {
	V3f position;
	V3f velocity;
	content::Material material;
	pkt >> position >> velocity;
	material.deserialise(pkt);
	assert(pkt.end());

	LogF(WARNING, "Received spawn particle");

	game->spawnSingleParticle(position, velocity, material);
}
