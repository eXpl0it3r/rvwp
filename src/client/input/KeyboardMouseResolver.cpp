#include "KeyboardMouseResolver.hpp"

#include <Thor/Vectors.hpp>
#include <utility>

using namespace client;

KeyboardMouseResolver::KeyboardMouseResolver(
		const sf::Window &window, std::function<V3f(V2s)> mapper)
		: window(window), mapper(std::move(mapper)) {
	auto ctrl = thor::Action(sf::Keyboard::LControl, thor::Action::Hold) ||
			thor::Action(sf::Keyboard::RControl, thor::Action::Hold);

	auto shift = thor::Action(sf::Keyboard::LShift, thor::Action::Hold) ||
			thor::Action(sf::Keyboard::RShift, thor::Action::Hold);

	map[Action::MOVE_LEFT] =
			thor::Action(sf::Keyboard::A, thor::Action::Hold) ||
			thor::Action(sf::Keyboard::Left, thor::Action::Hold);

	map[Action::MOVE_RIGHT] =
			thor::Action(sf::Keyboard::D, thor::Action::Hold) ||
			thor::Action(sf::Keyboard::Right, thor::Action::Hold);

	map[Action::MOVE_UP] = thor::Action(sf::Keyboard::W, thor::Action::Hold) ||
			thor::Action(sf::Keyboard::Up, thor::Action::Hold);

	map[Action::MOVE_DOWN] =
			thor::Action(sf::Keyboard::S, thor::Action::Hold) ||
			thor::Action(sf::Keyboard::Down, thor::Action::Hold);

	map[Action::USE_L] = thor::Action(sf::Mouse::Left, thor::Action::PressOnce);
	map[Action::USE_R] =
			thor::Action(sf::Mouse::Right, thor::Action::PressOnce);
	map[Action::INTERACT] =
			thor::Action(sf::Keyboard::E, thor::Action::PressOnce);
	map[Action::SNEAK] = thor::Action(sf::Keyboard::LShift, thor::Action::Hold);
	map[Action::JUMP] =
			thor::Action(sf::Keyboard::Space, thor::Action::PressOnce);
	map[Action::CAMERA_SLICE_DOWN] =
			thor::Action(sf::Keyboard::LBracket, thor::Action::PressOnce);
	map[Action::CAMERA_SLICE_UP] =
			thor::Action(sf::Keyboard::RBracket, thor::Action::PressOnce);
	map[Action::SWITCH_DEBUG_MODE] =
			thor::Action(sf::Keyboard::F5, thor::Action::PressOnce);
	map[Action::SWITCH_RENDER_MODE] =
			thor::Action(sf::Keyboard::F6, thor::Action::PressOnce);
	map[Action::ZOOM_SCROLL] = thor::Action(sf::Event::MouseWheelScrolled);
	map[Action::ZOOM_IN] =
			ctrl && thor::Action(sf::Keyboard::Equal, thor::Action::PressOnce);
	map[Action::ZOOM_OUT] =
			ctrl && thor::Action(sf::Keyboard::Hyphen, thor::Action::PressOnce);
	map[Action::ZOOM_RESET] =
			ctrl && thor::Action(sf::Keyboard::Num0, thor::Action::PressOnce);
	map[Action::SWITCH_MODE] =
			thor::Action(sf::Keyboard::F7, thor::Action::PressOnce);
	map[Action::EXIT] =
			thor::Action(sf::Keyboard::Escape, thor::Action::PressOnce);
	map[Action::INVENTORY] =
			thor::Action(sf::Keyboard::Tab, thor::Action::PressOnce);
	map[Action::CHAT] =
			thor::Action(sf::Keyboard::T, thor::Action::ReleaseOnce);
	map[Action::CHAT_COMMAND] =
			thor::Action(sf::Keyboard::Slash, thor::Action::ReleaseOnce);
	map[Action::TOOL_SELECT] =
			thor::Action(sf::Mouse::Left, thor::Action::PressOnce);
	map[Action::TOOL_RELEASE] =
			thor::Action(sf::Mouse::Left, thor::Action::ReleaseOnce);
	map[Action::GUI_SELECT] =
			thor::Action(sf::Keyboard::Enter, thor::Action::ReleaseOnce);

	map[Action::TAB_1] =
			thor::Action(sf::Keyboard::Num1, thor::Action::ReleaseOnce);
	map[Action::TAB_2] =
			thor::Action(sf::Keyboard::Num2, thor::Action::ReleaseOnce);
	map[Action::TAB_3] =
			thor::Action(sf::Keyboard::Num3, thor::Action::ReleaseOnce);
	map[Action::TAB_4] =
			thor::Action(sf::Keyboard::Num4, thor::Action::ReleaseOnce);
	map[Action::TAB_5] =
			thor::Action(sf::Keyboard::Num5, thor::Action::ReleaseOnce);
	map[Action::TAB_6] =
			thor::Action(sf::Keyboard::Num6, thor::Action::ReleaseOnce);
	map[Action::TAB_7] =
			thor::Action(sf::Keyboard::Num7, thor::Action::ReleaseOnce);
	map[Action::TAB_8] =
			thor::Action(sf::Keyboard::Num8, thor::Action::ReleaseOnce);

	lastMousePos = sf::Mouse::getPosition(window);
}

void KeyboardMouseResolver::update(float dtime) {
	timeSinceLastActive = std::min(100000, timeSinceLastActive + 1);

	auto mp = sf::Mouse::getPosition(window);
	if (thor::squaredLength(lastMousePos - mp) > 2) {
		lastMousePos = mp;
		timeSinceLastActive = 0;
	}
}

V3f KeyboardMouseResolver::getMoveVector() {
	V3f retval;
	if (map.isActive(Action::MOVE_LEFT)) {
		retval.x = -1;
	} else if (map.isActive(Action::MOVE_RIGHT)) {
		retval.x = 1;
	}

	if (map.isActive(Action::MOVE_UP)) {
		retval.y = -1;
	} else if (map.isActive(Action::MOVE_DOWN)) {
		retval.y = 1;
	}

	return retval;
}

std::optional<V3f> KeyboardMouseResolver::getCursorPosition(const V3f &origin) {
	sf::Vector2i mp = sf::Mouse::getPosition(window);
	return mapper({mp.x, mp.y});
}
