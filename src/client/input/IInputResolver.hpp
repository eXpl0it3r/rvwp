#pragma once
#include "types/types.hpp"
#include "types/Controller.hpp"
#include "Action.hpp"

#include <SFML/Graphics.hpp>
#include <Thor/Input.hpp>
#include <optional>

namespace client {

class IInputResolver : public types::Controller {
protected:
	thor::ActionMap<Action> map;

public:
	/// Returns time in ticks since this resolver last detected activity.
	/// Used when deciding which resolver should take priority.
	///
	/// @return time in ticks
	virtual int getTimeSinceLastActive() const = 0;

	/// Movement direction vector
	///
	/// @return vector
	virtual V3f getMoveVector() = 0;

	/// Return the cursor's current position. For keyboard+mouse,
	/// this is just the mouse position. For gamepad, this is relative
	/// to the player position - called "origin" for abstraction here.
	///
	/// @param origin Usually the player position
	/// @return Optional cursor position, in world co-ordinates
	virtual std::optional<V3f> getCursorPosition(const V3f &origin) = 0;

	/// @return Whether the cursor is virtual, ie: a gamepad joystick
	virtual bool isVirtualCursor() const = 0;

	thor::ActionMap<Action> &getActionMap() { return map; }
};

} // namespace client
