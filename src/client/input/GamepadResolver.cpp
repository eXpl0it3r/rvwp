#include "GamepadResolver.hpp"
#include <log.hpp>

#define MAX_HARDWARE_BUTTONS 20

using namespace client;

GamepadResolver::GamepadResolver(unsigned int joystick_id)
		: joystick_id(joystick_id) {
	for (int &i : buttons_map) {
		i = -1;
	}

	reconfigure();
}

using uint = unsigned int;

void GamepadResolver::reconfigure() {
	if (!sf::Joystick::isConnected(joystick_id)) {
		type = NONE;
		return;
	}

	sf::Joystick::Identification info =
			sf::Joystick::getIdentification(joystick_id);

	if (info.name.find("xbox")) {
		type = XBOX360;
	} else {
		type = GENERIC;
	}

	// Just assume that all controllers are like XBOX for now
	buttons_map[BTN_A] = 0;
	buttons_map[BTN_B] = 1;
	buttons_map[BTN_X] = 2;
	buttons_map[BTN_Y] = 3;
	buttons_map[BTN_LB] = 4;
	buttons_map[BTN_RB] = 5;
	buttons_map[BTN_BACK] = 6;
	buttons_map[BTN_START] = 7;
	buttons_map[BTN_LJS_CLICK] = 9;
	buttons_map[BTN_RJS_CLICK] = 10;
	buttons_map[BTN_DPAD_UP] = 13;
	buttons_map[BTN_DPAD_LEFT] = 11;
	buttons_map[BTN_DPAD_RIGHT] = 12;
	buttons_map[BTN_DPAD_DOWN] = 14;

	// Bind up and down on left JS to buttons
	buttons_map[BTN_LJS_UP] = MAX_HARDWARE_BUTTONS + 1;
	buttons_map[BTN_LJS_DOWN] = MAX_HARDWARE_BUTTONS + 2;
	buttons_map[BTN_LT] = MAX_HARDWARE_BUTTONS + 3;
	buttons_map[BTN_RT] = MAX_HARDWARE_BUTTONS + 4;

	// Whether LT & RT are axes or buttons varies - detect this here
	if (sf::Joystick::hasAxis(joystick_id, sf::Joystick::Axis::Z)) {
		trigger_lt = sf::Joystick::Axis::Z;
		trigger_rt = sf::Joystick::Axis::R;
	} else {
		buttons_map[BTN_LT] = 6;
		buttons_map[BTN_RT] = 7;
	}

	map.clearActions();

	map[Action::USE_L] = thor::Action(
			thor::JoystickButton(joystick_id, (uint)buttons_map[BTN_LT]),
			thor::Action::PressOnce);
	map[Action::USE_R] = thor::Action(
			thor::JoystickButton(joystick_id, (uint)buttons_map[BTN_RT]),
			thor::Action::PressOnce);

	if (buttons_map[BTN_RB] >= 0) {
		map[Action::ZOOM_IN] = thor::Action(
				thor::JoystickButton(joystick_id, (uint)buttons_map[BTN_RB]),
				thor::Action::PressOnce);
	}
	if (buttons_map[BTN_LB] >= 0) {
		map[Action::ZOOM_OUT] = thor::Action(
				thor::JoystickButton(joystick_id, (uint)buttons_map[BTN_LB]),
				thor::Action::PressOnce);
	}

	map[Action::INTERACT] = thor::Action(
			thor::JoystickButton(joystick_id, (uint)buttons_map[BTN_X]),
			thor::Action::PressOnce);
	map[Action::JUMP] = thor::Action(
			thor::JoystickButton(joystick_id, (uint)buttons_map[BTN_A]),
			thor::Action::PressOnce);
	map[Action::SWITCH_MODE] = thor::Action(
			thor::JoystickButton(joystick_id, (uint)buttons_map[BTN_DPAD_UP]),
			thor::Action::PressOnce);
	map[Action::GUI_FOCUS_UP] = thor::Action(
			thor::JoystickButton(joystick_id, (uint)buttons_map[BTN_LJS_UP]),
			thor::Action::PressOnce);
	map[Action::GUI_FOCUS_DOWN] = thor::Action(
			thor::JoystickButton(joystick_id, (uint)buttons_map[BTN_LJS_DOWN]),
			thor::Action::PressOnce);
	map[Action::GUI_SELECT] = thor::Action(
			thor::JoystickButton(joystick_id, (uint)buttons_map[BTN_A]),
			thor::Action::PressOnce);
	map[Action::EXIT] = thor::Action(
			thor::JoystickButton(joystick_id, (uint)buttons_map[BTN_B]),
			thor::Action::PressOnce);
	map[Action::INVENTORY] = thor::Action(
			thor::JoystickButton(joystick_id, (uint)buttons_map[BTN_DPAD_DOWN]),
			thor::Action::PressOnce);
	map[Action::TOOL_SELECT] = thor::Action(
			thor::JoystickButton(joystick_id, (uint)buttons_map[BTN_A]),
			thor::Action::PressOnce);
	map[Action::TOOL_RELEASE] = thor::Action(
			thor::JoystickButton(joystick_id, (uint)buttons_map[BTN_A]),
			thor::Action::ReleaseOnce);
}

inline float rawJoystickToOutputValue(float v) {
	if (std::fabs(v) < 0.25f) {
		return 0;
	} else {
		return ((v < 0) ? -1.f : 1.f) * v * v;
	}
}

inline float GetAxis(unsigned int joystick_id, sf::Joystick::Axis axis) {
	float val = sf::Joystick::getAxisPosition(joystick_id, axis) / 100.f;
	return rawJoystickToOutputValue(val);
}

inline bool IsAxisActive(
		unsigned int joystick_id, int axis, bool absoluteValue = false) {
	if (axis < 0) {
		return false;
	}

	float value = GetAxis(joystick_id, (sf::Joystick::Axis)axis);
	if (absoluteValue) {
		return std::fabs(value) > 0.25f;
	} else {
		return value > 0.25f;
	}
}

sf::Event CreateJoystickButtonEvent(sf::Event::EventType type,
		unsigned int joystick_id, unsigned int button) {
	sf::Event event{};
	event.type = type;
	event.joystickButton.joystickId = joystick_id;
	event.joystickButton.button = button;
	return event;
}

void GamepadResolver::update(float dtime) {
	timeSinceLastActive = std::min(100000, timeSinceLastActive + 1);

	if (!sf::Joystick::isConnected(joystick_id)) {
		return;
	}
	if (checkForActivity()) {
		timeSinceLastActive = 0;
	}

	if (trigger_lt) {
		bool pressed = IsAxisActive(joystick_id, trigger_lt);
		if (pressed != button_was_pressed[BTN_LT]) {
			auto type = pressed ? sf::Event::JoystickButtonPressed
								: sf::Event::JoystickButtonReleased;
			map.pushEvent(CreateJoystickButtonEvent(
					type, joystick_id, (uint)buttons_map[BTN_LT]));
		}

		button_was_pressed[BTN_LT] = pressed;
	}

	if (trigger_rt) {
		bool pressed = IsAxisActive(joystick_id, trigger_rt);

		if (pressed != button_was_pressed[BTN_RT]) {
			auto type = pressed ? sf::Event::JoystickButtonPressed
								: sf::Event::JoystickButtonReleased;
			map.pushEvent(CreateJoystickButtonEvent(
					type, joystick_id, (uint)buttons_map[BTN_RT]));
		}

		button_was_pressed[BTN_RT] = pressed;
	}

	float axisY = GetAxis(joystick_id, sf::Joystick::Axis::Y);

	{
		bool pressed = axisY < -0.9f;
		if (pressed != button_was_pressed[BTN_LJS_UP]) {
			button_was_pressed[BTN_LJS_UP] = pressed;
			auto type = pressed ? sf::Event::JoystickButtonPressed
								: sf::Event::JoystickButtonReleased;
			map.pushEvent(CreateJoystickButtonEvent(
					type, joystick_id, (uint)buttons_map[BTN_LJS_UP]));
		}
	}

	{
		bool pressed = axisY > 0.9f;
		if (pressed != button_was_pressed[BTN_LJS_DOWN]) {
			button_was_pressed[BTN_LJS_DOWN] = pressed;
			auto type = pressed ? sf::Event::JoystickButtonPressed
								: sf::Event::JoystickButtonReleased;
			map.pushEvent(CreateJoystickButtonEvent(
					type, joystick_id, (uint)buttons_map[BTN_LJS_DOWN]));
		}
	}
}

V3f GamepadResolver::getMoveVector() {
	return {GetAxis(joystick_id, sf::Joystick::Axis::X),
			GetAxis(joystick_id, sf::Joystick::Axis::Y), 0};
}

std::optional<V3f> GamepadResolver::getCursorPosition(const V3f &origin) {
	sf::Vector2f delta = {GetAxis(joystick_id, sf::Joystick::Axis::U),
			GetAxis(joystick_id, sf::Joystick::Axis::V)};
	sf::Vector2f sign = {delta.x >= 0 ? 1.f : -1.f, delta.y >= 0 ? 1.f : -1.f};
	sf::Vector2f adjustedDelta = {
			sign.x * delta.x * delta.x, sign.y * delta.y * delta.y};

	float radius = 5.f;
	return origin + V3f(adjustedDelta.x, adjustedDelta.y, 0) * radius;
}

bool GamepadResolver::checkForActivity() const {
	if (!sf::Joystick::isConnected(joystick_id)) {
		return false;
	}

	for (const auto &btn : this->buttons_map) {
		if (btn >= 0 && sf::Joystick::isButtonPressed(joystick_id, btn)) {
			return true;
		}
	}

	return IsAxisActive(joystick_id, trigger_lt) ||
			IsAxisActive(joystick_id, trigger_rt) ||
			IsAxisActive(joystick_id, sf::Joystick::Axis::X, true) ||
			IsAxisActive(joystick_id, sf::Joystick::Axis::Y, true) ||
			IsAxisActive(joystick_id, sf::Joystick::Axis::U, true) ||
			IsAxisActive(joystick_id, sf::Joystick::Axis::V, true);
}
