#pragma once

enum class Action {
	// Shared-all

	EXIT,
	CHAT,
	CHAT_COMMAND,

	// Shared-ingame

	MOVE_LEFT,
	MOVE_RIGHT,
	MOVE_UP,
	MOVE_DOWN,
	CAMERA_SLICE_DOWN,
	CAMERA_SLICE_UP,
	SWITCH_DEBUG_MODE,
	SWITCH_RENDER_MODE,
	ZOOM_SCROLL,
	ZOOM_IN,
	ZOOM_OUT,
	ZOOM_RESET,
	SWITCH_MODE,

	// Hero

	USE_L,
	USE_R,
	INTERACT,
	JUMP,
	SNEAK,

	// Architect

	TOOL_SELECT,
	TOOL_RELEASE,
	TAB_1,
	TAB_2,
	TAB_3,
	TAB_4,
	TAB_5,
	TAB_6,
	TAB_7,
	TAB_8,

	// GUI

	GUI_FOCUS_UP,
	GUI_FOCUS_DOWN,
	GUI_SELECT,
	INVENTORY,
};

enum ActionState { INGAME, GUI };

/// Whether this action can be performed
/// @param action
/// @return
inline bool IsActionAllowed(Action action, ActionState state) {
	if (action == Action::EXIT) {
		return true;
	}

	bool isGUIAction = action == Action::GUI_FOCUS_DOWN ||
			action == Action::GUI_FOCUS_UP || action == Action::GUI_SELECT;
	return state == (isGUIAction ? GUI : INGAME);
}
