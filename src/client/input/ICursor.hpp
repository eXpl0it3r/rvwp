#pragma once

#include <optional>
#include "types/types.hpp"
#include "world/SelectionSpec.hpp"

namespace client {
class ICursor {
public:
	virtual ~ICursor() = default;

	/// Get the current selected position
	///
	/// @return world position
	virtual std::optional<V3f> getCursorPosition() const = 0;

	/// A raycast is fired from the player to the cursor
	/// position to determine the interacted object.
	///
	/// @return Selection
	virtual world::SelectionSpec getInteractionSelection() const = 0;

	/// Whether the cursor should be shown in the world.
	/// This is used when the source cursor isn't an actual cursor
	///
	/// @return show crosshair
	virtual bool shouldShowCrosshair() const = 0;
};
} // namespace client
