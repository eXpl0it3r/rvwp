#include <cassert>
#include <sstream>
#include "BarLineGraph.hpp"

std::pair<int, int> getMinMax(const std::deque<int> &data, int maxMax) {
	if (data.empty()) {
		return {0, 0};
	}

	int max = 0;
	int min = INT_MAX;
	for (const auto &v : data) {
		if (v > max) {
			max = v;
		}

		if (v < min) {
			min = v;
		}
	}

	if (max > maxMax) {
		max = maxMax;
	}

	return {min, max};
}

BarLineGraph::BarLineGraph(const std::string &title, const sf::Font &font,
		int falseBottomMin, int maxEntries, int maxValue)
		: data(), maxEntries(maxEntries), title(title), font(font),
		  falseBottomMin(falseBottomMin), maxValue(maxValue) {
	regenVertexArray();
}

void BarLineGraph::draw(
		sf::RenderTarget &target, sf::RenderStates states) const {
	auto minMax = getMinMax(data, maxValue);

	sf::Vector2u origin((unsigned int)getPosition().x,
			(unsigned int)getPosition().y + height);

	target.draw(vertices);

	std::ostringstream os;
	os << title << " | " << minMax.first << " | ";

	if (data.empty())
		os << "NaN";
	else
		os << sum / data.size();

	os << " | " << minMax.second;

	sf::Text label = sf::Text(os.str().c_str(), font, 16);
	label.setPosition(sf::Vector2f(origin.x + 5, origin.y - height));
	label.setFillColor(sf::Color::White);
	target.draw(label);
}

void BarLineGraph::regenVertexArray() {
	if (!dirty) {
		return;
	}
	dirty = false;

	auto minMax = getMinMax(data, maxValue);
	int variant = (int)(0.1f * (minMax.second - minMax.first));
	float graphMin = clamp<int>(minMax.first - variant, 0, falseBottomMin);
	float graphMax = minMax.second > 0 ? minMax.second : 1;

	sf::Vector2u origin((unsigned int)getPosition().x,
			(unsigned int)getPosition().y + height);

	int currentX = origin.x + 20;
	const int stepX = 2;

	vertices.setPrimitiveType(sf::Lines);
	vertices.resize(data.size() * 4);

	int i = 0;
	sum = 0;
	for (const auto &v : data) {
		currentX += stepX;
		sum += v;

		assert(currentX - getPosition().x <= getSize().x);

		sf::Color color = sf::Color::White;
		if (enable_colors) {
			if (v > maxValue) {
				color = sf::Color::Cyan;
			} else if (v > 55) {
				color = sf::Color::Green;
			} else if (v > 20) {
				color = sf::Color::Yellow;
			} else {
				color = sf::Color::Red;
			}
		}

		float drawnValue = v;

		if (drawnValue > maxValue) {
			drawnValue = maxValue;
		}

		sf::Vector2f from(currentX,
				origin.y - 15 -
						(height - 40) * (drawnValue - graphMin) /
								(graphMax - graphMin));

		sf::Vector2f to(currentX, origin.y - 10);

		vertices[i * 4].position = from;
		vertices[i * 4].color = color;
		vertices[i * 4 + 1].position = to;
		vertices[i * 4 + 1].color = color;
		vertices[i * 4 + 2].position = from + sf::Vector2f(1, 0);
		vertices[i * 4 + 2].color = color;
		vertices[i * 4 + 3].position = to + sf::Vector2f(1, 0);
		vertices[i * 4 + 3].color = color;

		i++;
	}
}

sf::Vector2u BarLineGraph::getSize() const {
	return {(unsigned int)(maxEntries * 2 + 20), height};
}
