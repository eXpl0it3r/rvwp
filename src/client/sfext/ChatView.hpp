#pragma once

#include <SFML/Graphics.hpp>
#include <RichText.hpp>
#include "types/types.hpp"

#include <deque>

namespace sfext {

class ChatView : public sf::Drawable, public sf::Transformable {
	const sf::Font &font;
	std::deque<sfe::RichText> chat_text;
	const int numberLines;

public:
	ChatView(const sf::Font &font, unsigned int characterSize,
			int numberLines = 15);

	void pushChatMessage(const std::string &msg);

	sf::Vector2f getSize();

private:
	virtual void draw(
			sf::RenderTarget &target, sf::RenderStates states) const override;
};

} // namespace sfext
