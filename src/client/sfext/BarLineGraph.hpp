#pragma once
#include <SFML/Graphics.hpp>
#include "../../types/types.hpp"

#include <deque>

class BarLineGraph : public sf::Drawable, public sf::Transformable {
	const std::string title;
	const sf::Font font;
	const int falseBottomMin;
	const int maxValue;
	unsigned int height = 100;

	sf::VertexArray vertices;
	std::deque<int> data;
	int maxEntries;
	bool dirty = true;
	int sum = 0;

public:
	/// Constructor
	///
	/// @param title Graph title
	/// @param font Font to use
	/// @param falseBottomMin If greater than 0, the graph will have a false
	/// bottom
	/// @param max_data Maximum number of data entries
	/// @param maxValue Maximum value to show, values greater than this will be
	/// clipped
	explicit BarLineGraph(const std::string &title, const sf::Font &font,
			int falseBottomMin = 0, int maxEntries = 100,
			int maxValue = INT_MAX);

	inline void push(int v) {
		if (data.size() >= maxEntries) {
			data.pop_front();
		}
		data.push_back(v);
		dirty = true;
	}

	bool enable_colors = false;

	void draw(sf::RenderTarget &target, sf::RenderStates states) const override;
	void regenVertexArray();

	sf::Vector2u getSize() const;
	void setHeight(unsigned int h) { height = h; }
};
