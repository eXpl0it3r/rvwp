#include "Game.hpp"
#include "Client.hpp"
#include "controllers/CameraController.hpp"
#include "controllers/ModeController.hpp"
#include "input/KeyboardMouseResolver.hpp"
#include "input/GamepadResolver.hpp"
#include "views/world/WorldView.hpp"

#include <sstream>
#include <log.hpp>
#include <cassert>

using namespace client;

Game::Game(sf::RenderWindow *window, tgui::Gui &gui, sf::Font &font,
		InputController *input_ctr)
		: client(std::make_unique<Client>(this, &clientHeroEventBus)),
		  window(window), input_ctr(input_ctr),
		  clientEventListener(client.get(), &architectEventBus, &heroEventBus,
				  &inventoryCommandBus),
		  gui(gui) {
	Log("Game", INFO) << "Initialising Game components";

	def = std::make_unique<content::DefinitionManager>();
	resources = std::make_unique<ResourceManager>(font);

	camera_ctr = std::make_unique<CameraController>(input_ctr);
	registerController(camera_ctr.get());

	world_ctr = std::make_unique<WorldController>(&worldBus, def.get());
	registerController(world_ctr.get());

	entityFootsteps = std::make_unique<EntityFootstepPlayer>(
			def.get(), resources.get(), getWorld(), camera_ctr.get());
	registerController(entityFootsteps.get());

	modeController = std::make_unique<ModeController>(&architectEventBus,
			&heroEventBus, &clientHeroEventBus, def.get(), getWorld(),
			input_ctr, camera_ctr.get());
	registerController(modeController.get());

	//
	// Game View
	//

	gameView = std::make_unique<GameView>(this, &worldBus, &heroEventBus,
			&inventoryCommandBus, def.get(), resources.get(), gui);
	registerController(gameView.get());

	//
	// Input
	//

	input_ctr->setActionStateProvider(
			[&]() { return gameView->isMainViewActive() ? INGAME : GUI; });

	input_ctr->addResolver(std::make_unique<KeyboardMouseResolver>(
			*window, gameView->getScreenToWorldMapper(window)));
	input_ctr->addResolver(std::make_unique<GamepadResolver>(0));

	auto bind = [&](Action action, std::function<void()> func) {
		inputConnections.emplace_back(input_ctr->connect(action, func));
	};

	bind(Action::CAMERA_SLICE_DOWN,
			std::bind(&Game::doCameraZChange, this, false));
	bind(Action::CAMERA_SLICE_UP,
			std::bind(&Game::doCameraZChange, this, true));
	bind(Action::SWITCH_DEBUG_MODE, std::bind(&Game::doSwitchDebugMode, this));
	bind(Action::SWITCH_RENDER_MODE,
			std::bind(&Game::doSwitchRenderMode, this));
	bind(Action::SWITCH_MODE, std::bind(&Game::doSwitchPlayerMode, this));
	bind(Action::GUI_FOCUS_UP, std::bind(&Game::guiChangeFocus, this, true));
	bind(Action::GUI_FOCUS_DOWN, std::bind(&Game::guiChangeFocus, this, false));
	bind(Action::EXIT, std::bind(&Game::guiExit, this));
	bind(Action::CHAT,
			std::bind(&GameView::switchToChatSendView, gameView.get(), ""));
	bind(Action::CHAT_COMMAND,
			std::bind(&GameView::switchToChatSendView, gameView.get(), "/"));
	registerController(input_ctr);
}

Game::~Game() = default;

bool Game::load(network::Address server, const std::string &username,
		const std::string &password) {
	this->username = username;

	Log("Game", INFO) << "Loading game.";

	def->unlock();
	assert(def->size() == 0);

	return gameView->load() && client->connect(server, username, password);
}

void Game::run() {
	Log("Game", INFO) << "Running game";

	def->lock();

	gameView->activate();

	resize(window->getSize().x, window->getSize().y);

	sf::Clock dtimeClock;

	//
	// Start loop
	//
	while (window->isOpen() && client->isConnected()) {
		// Handle Events
		sf::Event event;
		while (window->pollEvent(event)) {
			onEvent(event);
		}

		// Draw all
		sf::Clock renderClock;
		window->clear();
		gameView->draw(*window, sf::RenderStates());
		window->setView(hud_view);
		gameView->drawHUD(*window, sf::RenderStates());
		gui.draw();
		gameView->drawPostGUI(*window, sf::RenderStates());
		window->display();
		last_rtime = renderClock.restart().asSeconds();

		// Get dtime
		float dtime = dtimeClock.restart().asSeconds();

		// Update
		update(dtime);
	}
}

void Game::update(float delta) {
	for (auto ctr : controllers) {
		ctr->update_if_enabled(delta);
	}

	client->update(delta);
	time.update(delta);

	architectEventBus.flush();
	heroEventBus.flush();
	clientHeroEventBus.flush();
	worldBus.flush();
	inventoryCommandBus.flush();

	// Graphs
	HudView *hudView = gameView->getHUDView();
	hudView->pushStatFPS(1.0f / delta);

	debug_dtime_accumulator += delta;
	debug_graph_update_delta += delta;
	ticks_since_last_graph_update++;
	if (debug_graph_update_delta > 0.1f) {
		network::NetworkStats stats;
		client->getStats(stats);

		hudView->pushStatPacketRecv(stats.packetsReceived);
		hudView->pushStatPacketSend(stats.packetsSent);
		hudView->pushStatPacketLost(stats.packetsLost);
		hudView->pushStatPacketReliable(stats.reliableDataInTransit);
		hudView->pushStatDTime(
				debug_dtime_accumulator / (float)ticks_since_last_graph_update);
		hudView->pushStatChunkGen(
				gameView->getWorldView()->getChunkMeshGenAndReset());

		packets = 0;
		debug_dtime_accumulator = 0;
		debug_graph_update_delta = 0;
		ticks_since_last_graph_update = 0;
	}
}

bool Game::onEvent(const sf::Event &event) {
	if (event.type == sf::Event::Closed) {
		window->close();
		return true;
	} else if (event.type == sf::Event::Resized) {
		resize(event.size.width, event.size.height);
		return true;
	}

	if (event.type == sf::Event::KeyPressed ||
			event.type == sf::Event::KeyReleased) {
		return input_ctr->onEvent(event) || gui.handleEvent(event);
	} else {
		return gui.handleEvent(event) || input_ctr->onEvent(event);
	}
}

void Game::resize(unsigned int width, unsigned int height) {
	if (width % 2 == 1 || height % 2 == 1) {
		unsigned int mask = ~((unsigned int)1);
		width &= mask;
		height &= mask;
		window->setSize({width, height});
	}

	gameView->resize(width, height);
	hud_view.setSize(width, height);
	hud_view.setCenter(sf::Vector2f(width / 2, height / 2));
	gui.setAbsoluteView({0, 0, (float)width, (float)height});
}

const Camera &Game::getCamera() const {
	return camera_ctr->getCamera();
}

void Game::displayMessageToChat(const std::string &message) {
	auto hudView = gameView->getHUDView();

	std::string line;
	std::istringstream iss(message);
	while (std::getline(iss, line)) {
		hudView->pushChatMessage(line);
	}
}

void Game::closeAllOpen() {
	while (gameView->exit()) {
	}
}

void Game::guiExit() {
	if (gameView->exit()) {
		return;
	}

	if (gameView->isMainViewActive()) {
		client->disconnect();
		window->close();
	} else {
		gameView->switchToMainView();
	}
}

void Game::say(const std::string &message) {
	client->sendChatMessage(message);
}

void Game::doCameraZChange(bool is_up) {
	camera_ctr->changeCameraZ(is_up);
}

void Game::doSwitchDebugMode() {
	gameView->getHUDView()->switchDebugMode();
}

void Game::doSwitchRenderMode() {
	gameView->getWorldView()->switchRenderMode();
}

void Game::doSwitchPlayerMode() {
	closeAllOpen();

	modeController->nextMode();
	switch (modeController->getMode()) {
	case GameMode::HERO:
		camera_ctr->setMaxZoom(1.f);
		camera_ctr->setCameraZ(
				modeController->getHero()->getPlayerPosition().z);
		gameView->switchToHeroMode();
		break;
	case GameMode::ARCHITECT:
		camera_ctr->setMaxZoom(20.f);
		gameView->switchToArchitectMode();
		break;
	}
}

content::Inventory *Game::getInventory(const std::string &name) {
	if (name == "player:" + username || name == "hero") {
		return modeController->getHero()->getInventory();
	} else {
		return nullptr;
	}
}

content::Inventory *Game::getPlayerInventory() {
	return modeController->getHero()->getInventory();
}

void Game::guiChangeFocus(bool isUp) {
	if (isUp) {
		gui.focusPreviousWidget();
	} else {
		gui.focusNextWidget();
	}
}

void Game::spawnSingleParticle(const V3f &pos, const V3f &velocity,
		const content::Material &material) {
	gameView->getWorldView()->spawnSingleParticle(pos, velocity, material);
}
