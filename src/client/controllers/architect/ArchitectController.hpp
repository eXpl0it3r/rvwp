#pragma once
#include "types/Controller.hpp"
#include "client/models/Camera.hpp"
#include "client/input/IInput.hpp"
#include "client/input/ICursor.hpp"
#include "client/models/ArchitectEvent.hpp"
#include "events/EventBus.hpp"

#include "../ICamera.hpp"
#include "world/Chunk.hpp"
#include "world/Plot.hpp"

#include "content/content.hpp"
#include "content/ToolSpec.hpp"
#include <string>
#include <memory>

namespace client {

class Game;
class CameraController;
class ArchitectController : public Controller, public ICursor {
	EventBus<ArchitectEvent> *bus;
	IInput *input;
	ICamera *cameraController;
	content::DefinitionManager *defMan;
	world::World *world;
	int plotId = 0;

public:
	ArchitectController(EventBus<ArchitectEvent> *architectBus, IInput *input,
			ICamera *cameraController, content::DefinitionManager *defMan,
			world::World *world)
			: bus(architectBus), input(input),
			  cameraController(cameraController), defMan(defMan), world(world) {
		disable();
	}

	world::Plot *getPlot();
	void setPlot(int id) { plotId = id; }

	void update(float dtime) override;

	std::optional<V3f> getCursorPosition() const override;

	world::SelectionSpec getInteractionSelection() const override;

	bool shouldShowCrosshair() const override {
		return input->isVirtualCursor();
	}

	const std::vector<content::ToolSpec> &getAvailableToolSpecs() {
		return defMan->getToolSpecs();
	}

	IInput *getInput() { return input; }

	void cancelWork(V3s from, V3s to);
	void addBuildWork(V3s from, V3s to, const content::TileDef *def,
			world::ChunkLayer layer);
	void addDeconstructWork(V3s from, V3s to, const content::ToolSpec &spec);
};

} // namespace client
