#pragma once

#include "architect/ArchitectController.hpp"
#include "hero/HeroController.hpp"
#include "../../types/Controller.hpp"
#include "client/models/ArchitectEvent.hpp"

namespace client {

enum GameMode { HERO, ARCHITECT };

class ModeController : public Controller {
	std::unique_ptr<ArchitectController> architectController;
	std::unique_ptr<HeroController> heroController;

	GameMode mode = HERO;

public:
	ModeController(EventBus<ArchitectEvent> *architectBus,
			EventBus<HeroEvent> *heroBus, EventBus<ClientHeroEvent> *clientBus,
			content::DefinitionManager *def, world::World *world,
			IInput *inputController, ICamera *cameraController);

	~ModeController();

	void update(float dtime) override;

	void setMode(GameMode newMode);

	GameMode getMode() { return mode; }

	void nextMode() { setMode((GameMode)((mode + 1) % 2)); }

	ArchitectController *getArchitect() { return architectController.get(); }

	HeroController *getHero() { return heroController.get(); }
};

} // namespace client
