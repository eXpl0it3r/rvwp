#pragma once
#include <SFML/Graphics.hpp>
#include "world/Entity.hpp"
#include "world/World.hpp"
#include "content/content.hpp"
#include "types/Controller.hpp"
#include <string>

namespace client {

class Game;
class WorldController : public Controller {
	world::World world;

public:
	WorldController(EventBus<world::WorldEvent> *worldBus,
			content::DefinitionManager *defman)
			: world(defman, worldBus) {}

	world::World *getWorld() { return &world; }

	/// Update the World
	void update(float dtime) override;
};

} // namespace client
