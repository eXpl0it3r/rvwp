#include "content/Inventory.hpp"
#include "../../world/Raycast.hpp"
#include "HeroController.hpp"
#include "HeroEntityController.hpp"

#include <log.hpp>
#include <sanity.hpp>
#include <functional>

using namespace client;
using namespace world;
using namespace content;

HeroController::HeroController(EventBus<HeroEvent> *bus,
		EventBus<ClientHeroEvent> *clientBus, DefinitionManager *def,
		World *world, IInput *input, ICamera *camera)
		: bus(bus), def(def), world(world), input(input),
		  clientListener(clientBus, [this](auto e) { handleClientEvent(e); }) {

	heroEntityController =
			std::make_unique<HeroEntityController>(this, world, input, camera);
	addChild(heroEntityController.get());

	inventory =
			std::make_unique<Inventory>(InventoryLocation::FromPlayer("local"));

	auto bind = [&](Action action, std::function<void()> func) {
		inputConnections.emplace_back(input->connect(action, func));
	};

	bind(Action::USE_L, [this]() { handleAction(Action::USE_L); });
	bind(Action::USE_R, [this]() { handleAction(Action::USE_R); });
	bind(Action::INTERACT, [this]() { handleAction(Action::INTERACT); });
	bind(Action::INVENTORY, [this]() { handleAction(Action::INVENTORY); });
}

HeroController::~HeroController() = default;

void HeroController::update(float dtime) {}

void HeroController::handleClientEvent(const ClientHeroEvent &event) {
	switch (event.type) {
	case ClientHeroEvent::SET_INVENTORY:
		setInventory(event.inv);
		break;
	case ClientHeroEvent::SET_ENTITY:
		heroEntityController->setEntity(event.entity);
		break;
	case ClientHeroEvent::TELEPORT:
		heroEntityController->teleportTo(event.pos);
		break;
	}
}

void HeroController::handleAction(Action action) {
	if (!isEnabled()) {
		return;
	}

	switch (action) {
	case Action::USE_L:
		useItem(0);
		break;
	case Action::USE_R:
		useItem(1);
		break;
	case Action::INTERACT: {
		auto selectedPosition = getCursorPosition();
		if (!selectedPosition) {
			LogF(WARNING,
					"[HeroController] Unable to interact with no "
					"selected position");
			return;
		}

		bus->push(HeroEvent::interact(selectedPosition.value().floor()));
		break;
	}
	case Action::INVENTORY:
		bus->push(HeroEvent::showInventory());
		break;
	default:
		FatalError("Unsupported action")
	}
}

void HeroController::useItem(int slot) {
	InventoryList *hotbar = inventory->get("hotbar").value();

	ItemStack &item = hotbar->get(slot);
	if (item.empty()) {
		SelectionSpec spec = getInteractionSelection();
		if (spec.type == SelectionSpec::SST_Entity) {
			assert(spec.entity);
			int damage = spec.entity->damage(10);
			bus->push(HeroEvent::punch(slot, damage, spec.entity));
		} else {
			dig(slot, spec);
		}
		return;
	}

	// Place tiles
	if (def->getTileDef(item.name)) {
		placeItem(slot);
		return;
	}

	const ItemDef *idef = def->getItemDef(item.name);
	if (!idef) {
		Log("HeroController", ERROR) << "Invalid cid " << item.name;
		return;
	}

	if (idef->weapon_spec) {
		fireWeapon(slot, idef);
		return;
	}

	LogF(ERROR, "[HeroController] Don't know how to use non-weapon item %s",
			item.name.c_str());
}

std::optional<V3f> HeroController::getCursorPosition() const {
	auto playerPosition = getPlayerPosition();
	auto pos = input->getCursorPosition(playerPosition);
	if (pos) {
		return {{pos.value().x, pos.value().y, playerPosition.z}};
	}

	return {};
}

SelectionSpec HeroController::getInteractionSelection() const {
	auto selectedPos = getCursorPosition();
	if (!selectedPos) {
		return {};
	}

	Raycast tracer(world, heroEntityController->getEntity());
	tracer.tile_collision = true;
	tracer.entity_collision = true;
	return tracer.trace_to(
			getPlayerPosition(), selectedPos.value(), 0.2f, 10.f);
}

Entity *HeroController::getHeroEntity() const {
	return heroEntityController->getEntity();
}

V3f HeroController::getPlayerPosition() const {
	return heroEntityController->getPosition();
}

bool HeroController::dig(int hotbarIdx, const SelectionSpec &spec) {
	if (spec.type != SelectionSpec::SST_Tile) {
		return false;
	}

	V3s pos = spec.pos.floor();
	if (!world->set(pos, ChunkLayer::Tile, 0)) {
		return false;
	}

	bus->push(HeroEvent::dig(hotbarIdx, pos));

	return true;
}

bool HeroController::placeItem(int hotbarIdx) {
	InventoryList *list = inventory->get("hotbar").value();

	ItemStack &stack = list->get(hotbarIdx);
	if (stack.empty()) {
		Log("HeroController", ERROR) << "Cannot place empty stack!";
		return false;
	}

	SelectionSpec spec = getInteractionSelection();
	if (spec.type != SelectionSpec::SST_None) {
		Log("HeroController", ERROR)
				<< "Can't place! Something is in the way - cid=" << spec;

		return false;
	}

	// Place tile
	V3s pos = getCursorPosition().value().floor();
	const TileDef *tdef = def->getTileDef(stack.name);
	assert(tdef);
	auto set_tile =
			world->set(pos, typeToChunkLayer(tdef->getType()), tdef->id);
	if (set_tile == nullptr) {
		return false;
	}

	stack.count--;
	SanityCheck(list->get(hotbarIdx).count == stack.count);

	bus->push(HeroEvent::place(hotbarIdx, pos, set_tile));

	return true;
}

void HeroController::setInventory(Inventory *inv) {
	inventory = std::unique_ptr<Inventory>(inv);
}

bool HeroController::fireWeapon(int hotbarIdx, const ItemDef *idef) {
	const WeaponSpec *ws = &idef->weapon_spec.value();

	switch (ws->type) {
	case WeaponSpec::Type::None:
		break;
	case WeaponSpec::Type::Melee: {
		SelectionSpec spec = getInteractionSelection();
		if (spec.type == SelectionSpec::SST_Entity) {
			assert(spec.entity);
			int damage = spec.entity->damage(ws->damage);
			bus->push(HeroEvent::punch(hotbarIdx, damage, spec.entity));
		}

		break;
	}
	case WeaponSpec::Type::Gun: {
		Log("HeroController", INFO) << "Firing gun ";

		V3f ppos = heroEntityController->getPosition();
		V3f direction = getCursorPosition().value() - ppos;
		direction.normalise2();

		Raycast tracer(world);
		SelectionSpec spec = tracer.trace(ppos, direction, 100, 1.1f);
		if (spec.type == SelectionSpec::SST_Entity) {
			assert(spec.entity);

			int damage = 10;
			spec.entity->damage(damage);

			bus->push(HeroEvent::ranged(hotbarIdx, getPlayerPosition(),
					getHeroEntity()->getYaw(), damage, spec.entity));
		} else {
			bus->push(HeroEvent::ranged(hotbarIdx, getPlayerPosition(),
					getHeroEntity()->getYaw(), 0, nullptr));
		}

		return true;
	}
	}

	return false;
}
