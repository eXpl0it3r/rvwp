#pragma once

#include "types/Controller.hpp"
#include "client/controllers/WorldController.hpp"
#include "client/input/IInput.hpp"
#include "client/input/ICursor.hpp"
#include "client/controllers/ICamera.hpp"
#include "client/controllers/CameraController.hpp"
#include "client/models/HeroEvent.hpp"
#include "world/SelectionSpec.hpp"
#include "content/Inventory.hpp"
#include "content/Inventory.hpp"
#include "client/models/ClientHeroEvent.hpp"
#include "events/EventBus.hpp"
#include "events/EventListener.hpp"

#include <string>

namespace client {

class HeroEntityController;
class HeroController : public Controller, public ICursor {
	EventBus<HeroEvent> *bus;
	EventListener<ClientHeroEvent> clientListener;
	content::DefinitionManager *def;
	std::vector<InputConnection> inputConnections;

	std::unique_ptr<HeroEntityController> heroEntityController;
	std::unique_ptr<content::Inventory> inventory;

	world::World *world;
	IInput *input;

public:
	HeroController(EventBus<HeroEvent> *bus,
			EventBus<ClientHeroEvent> *clientBus,
			content::DefinitionManager *def, world::World *world, IInput *input,
			ICamera *camera);

	~HeroController() override;

	void update(float dtime) override;

	/// Get the current selected position
	///
	/// @return world position
	std::optional<V3f> getCursorPosition() const override;

	/// A raycast is fired from the player to the cursor
	/// position to determine the interacted object.
	///
	/// @return Selection
	world::SelectionSpec getInteractionSelection() const override;

	/// Whether the cursor should be shown in the world.
	/// This is used when the source cursor isn't an actual cursor
	///
	/// @return show crosshair
	bool shouldShowCrosshair() const override {
		return input->isVirtualCursor();
	}

	/// Current player position
	///
	/// @return position
	V3f getPlayerPosition() const;

	/// Gets the entity, or nullptr
	/// @return
	world::Entity *getHeroEntity() const;

	/// Replaces the inventory, invalidating all references
	///
	/// Warning: this transfers ownership
	///
	/// @param inventory
	void setInventory(content::Inventory *inv);

	/// @return The inventory
	content::Inventory *getInventory() { return inventory.get(); }

	/// Event handler for the ClientHeroEvent bus
	///
	/// @param event the event
	void handleClientEvent(const ClientHeroEvent &event);

private:
	void handleAction(Action action);
	void useItem(int slot);
	bool dig(int hotbarIdx, const world::SelectionSpec &spec);
	bool placeItem(int hotbarIdx);
	bool fireWeapon(int hotbarIdx, const content::ItemDef *idef);
};

} // namespace client
