#include "HeroEntityController.hpp"
#include "HeroController.hpp"
#include <SFML/Graphics.hpp>
#include <sanity.hpp>

using namespace client;
using namespace world;

HeroEntityController::HeroEntityController(HeroController *heroController,
		world::World *world, IInput *input, ICamera *cameraController)
		: heroController(heroController), world(world), input(input),
		  cameraController(cameraController) {

	auto bind = [&](Action action, std::function<void()> func) {
		inputConnections.emplace_back(input->connect(action, func));
	};

	bind(Action::JUMP, std::bind(&HeroEntityController::jump, this));

	disable();
}

void HeroEntityController::update(float dtime) {
	SanityCheck(entity);

	if (!physics.has_value()) {
		physics = {EntityPhysicsController(world, entity)};
	}

	cameraController->follow(entity);

	V3f moveVector = {};
	if (input->getCurrentActionState() == ActionState::INGAME) {
		moveVector = input->getMoveVector();

		if (input->isActive(Action::SNEAK)) {
			moveVector *= 3.f;
		} else {
			moveVector *= 8.f;
		}
	}

	physics->setMoveVector(moveVector);
	physics->update(dtime);

	// Point to the cursor
	auto selectedPosition = heroController->getCursorPosition();
	if (selectedPosition) {
		V3f delta = selectedPosition.value() - entity->getPosition();

		float yaw;
		if (delta.x > 0) {
			yaw = std::atan(delta.y / delta.x) * 180 / 3.14f + 90;
		} else if (delta.x < 0) {
			yaw = std::atan(delta.y / delta.x) * 180 / 3.14f + 180 + 90;
		} else {
			yaw = (delta.y > 0) ? 180 : 0;
		}

		entity->setYaw(yaw);
	}
}

void HeroEntityController::teleportTo(V3f pos) {
	entity->setPosition(pos);
}

void HeroEntityController::jump() {
	entity->jump(V3f(input->getMoveVector() * 0.2f, 0));
}
