#include "Client.hpp"
#include "Game.hpp"
#include "controllers/hero/HeroEntityController.hpp"
#include "../content/content.hpp"
#include <log.hpp>
#include <thread>
#include <chrono>

using namespace network;
using namespace client;
using namespace std::literals;

Client::Client(Game *game, EventBus<ClientHeroEvent> *heroBus)
		: game(game), heroBus(heroBus) {
	initPacketHandlers();
	Log("Client", INFO) << "Creating client";
}

Client::~Client() {
	disconnect();
}

bool Client::connect(network::Address address, const std::string &username,
		const std::string &password) {
	Log("Client", INFO) << "Connecting...";

	this->username = username;

	if (!socket.startClient(address)) {
		state = DISCONNECTED;
		return false;
	}

	server = address;

	// send connection packet
	state = CONNECTING;
	{
		const int size =
				2 + 2 + (int)username.size() + 2 + (int)password.size();
		Packet pkt(server, TOSERVER_HELLO, size);
		pkt << (u16)PROTOCOL_VERSION;
		pkt << username;
		pkt << password;
		socket.send(pkt);
	}

	// Wait for response
	while (state != LOADED) {
		socket.update();

		if (state == DISCONNECTED) {
			return false;
		}

		std::this_thread::sleep_for(100ms);
	}

	return true;
}

void Client::disconnect() {
	if (state == DISCONNECTED) {
		return;
	}

	Packet pkt(server, TOANY_DISC, 0);
	socket.send(pkt);
	socket.disconnect(server);

	Log("Client", INFO) << "Disconnecting...";
	state = DISCONNECTED;
}

void Client::update(float dtime) {
	socket.update();

	player_send_counter += dtime;
	if (player_send_counter > 0.1f) {
		player_send_counter = 0;
		sendPlayerPosition();
	}
}
