#include "ClientEventListener.hpp"

using namespace client;

void ClientEventListener::handleArchitectEvent(const ArchitectEvent &event) {
	switch (event.type) {
	case ArchitectEvent::NEW_WORK:
		client->sendWorkCreated(event.plotId, event.work);
		break;
	case ArchitectEvent::REMOVE_WORK:
		client->sendWorkRemoved(event.workId);
		break;
	}
}

void ClientEventListener::handleHeroEvent(const HeroEvent &event) {
	switch (event.type) {
	case HeroEvent::INTERACT:
		client->sendTileInteract(event.pos.floor());
		break;
	case HeroEvent::DIG:
	case HeroEvent::PLACE:
		client->sendSetTile(event.pos.floor(), event.tile, event.slot);
		break;
	case HeroEvent::SHOW_INVENTORY:
		// Ignore
		break;
	case HeroEvent::PUNCH:
		client->sendPunchEntity(event.entity, event.slot, event.damage);
		break;
	case HeroEvent::RANGED:
		client->sendFireWeapon(event.pos, event.yaw, event.slot);
		break;
	}
}

void ClientEventListener::handleInventoryEvent(
		const content::InventoryCommand &command) {
	client->sendPlayerInventoryCommand(command);
}
