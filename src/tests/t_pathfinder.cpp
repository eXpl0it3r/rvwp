#include "tests.hpp"

#include "world/AStarPathfinder.hpp"

#include "DefinitionTestData.hpp"

using namespace world;

namespace {

void initChunk(WorldChunk *chunk, content::DefinitionManager &def) {
	for (auto &cid : chunk->terrain) {
		cid = def.getCidFromName("grass");
	}
}

void initWorld(World &world, content::DefinitionManager &def) {
	{
		WorldChunk *chunk = world.getOrCreateChunk({0, 0, 0});
		initChunk(chunk, def);
		chunk->tiles[2 + 4 * CHUNK_SIZE].cid = def.getCidFromName("wall");
		chunk->tiles[3 + 4 * CHUNK_SIZE].cid = def.getCidFromName("wall");
		chunk->tiles[4 + 4 * CHUNK_SIZE].cid = def.getCidFromName("wall");
	}
	{
		WorldChunk *chunk = world.getOrCreateChunk({0, 0, 1});
		chunk->terrain[2 + 4 * CHUNK_SIZE] = def.getCidFromName("grass");
		chunk->terrain[3 + 4 * CHUNK_SIZE] = def.getCidFromName("grass");
		chunk->terrain[4 + 4 * CHUNK_SIZE] = def.getCidFromName("grass");
	}
	{
		WorldChunk *chunk = world.getOrCreateChunk({1, 0, 0});
		initChunk(chunk, def);
		for (int y = 0; y < 16; y++) {
			for (int x = 8; x < 16; x++) {
				chunk->tiles[x + y * CHUNK_SIZE].cid =
						def.getCidFromName("dirt");
			}
		}
	}
	{
		WorldChunk *chunk = world.getOrCreateChunk({1, 0, 1});
		for (int y = 0; y < 16; y++) {
			for (int x = 8; x < 16; x++) {
				chunk->terrain[x + y * CHUNK_SIZE] =
						def.getCidFromName("grass");
			}
		}
	}

	for (int x = -1; x > -50; x--) {
		for (int y = -1; y < 50; y++) {
			WorldChunk *chunk = world.getOrCreateChunk({x, y, 0});
			initChunk(chunk, def);
		}
	}
}
} // namespace

Test(AStarPathfinderStraightLineTest) {
	content::DefinitionManager def{};
	defineTestData(&def);
	World world(&def);
	initWorld(world, def);

	AStarPathfinder pathfinder(&world);

	std::vector<V3s> path;
	Assert(pathfinder.findPath(path, {3, 3, 0}, {6, 3, 0}, {}));

	std::vector<V3s> expected = {{3, 3, 0}, {6, 3, 0}};

	Assert(path.size() == expected.size());

	for (int i = 0; i < path.size(); i++) {
		Assert(path[i] == expected[i]);
	}
}

Test(AStarPathfinderObstacleTest) {
	content::DefinitionManager def{};
	defineTestData(&def);
	World world(&def);
	initWorld(world, def);

	AStarPathfinder pathfinder(&world);

	std::vector<V3s> path;
	Assert(pathfinder.findPath(path, {3, 3, 0}, {3, 5, 0}, {}));

	std::vector<V3s> expected = {{3, 3, 0}, {5, 4, 0}, {5, 5, 0}, {3, 5, 0}};

	Assert(path.size() == expected.size());

	for (int i = 0; i < path.size(); i++) {
		Assert(path[i] == expected[i]);
	}
}

Test(AStarPathfinderAscendTest) {
	content::DefinitionManager def{};
	defineTestData(&def);
	World world(&def);
	initWorld(world, def);

	AStarPathfinder pathfinder(&world);

	std::vector<V3s> path;
	Assert(pathfinder.findPath(path, {17, 3, 0}, {30, 3, 1}, {}));

	std::vector<V3s> expected = {
			{17, 3, 0}, {23, 3, 0}, {24, 3, 1}, {30, 3, 1}};

	Assert(path.size() == expected.size());

	for (int i = 0; i < path.size(); i++) {
		Assert(path[i] == expected[i]);
	}
}

Test(AStarPathfinderFallTest) {
	content::DefinitionManager def{};
	defineTestData(&def);
	World world(&def);
	initWorld(world, def);

	AStarPathfinder pathfinder(&world);

	std::vector<V3s> path;
	Assert(pathfinder.findPath(path, {3, 4, 1}, {0, 4, 0}, {}));

	std::vector<V3s> expected = {{3, 4, 1}, {2, 4, 1}, {1, 4, 0}, {0, 4, 0}};

	Assert(path.size() == expected.size());

	for (int i = 0; i < path.size(); i++) {
		Assert(path[i] == expected[i]);
	}
}

Test(AStarPathfinderImpossibleTest) {
	content::DefinitionManager def{};
	defineTestData(&def);
	World world(&def);
	initWorld(world, def);

	AStarPathfinder pathfinder(&world);

	std::vector<V3s> path;
	Assert(!pathfinder.findPath(path, {3, 0, 0}, {3, 4, 0}, {}));
}

Test(AStarPathfinderGivesUpTest) {
	content::DefinitionManager def{};
	defineTestData(&def);
	World world(&def);
	initWorld(world, def);

	AStarPathfinder pathfinder(&world);

	Pathfinder::Settings settings{};
	settings.giveUpCost = 2.f;

	std::vector<V3s> path;
	Assert(!pathfinder.findPath(path, {3, 3, 0}, {6, 3, 0}, settings));
}

Test(AStarPathfinderNeighbourObstacleTest) {
	content::DefinitionManager def{};
	defineTestData(&def);
	World world(&def);
	initWorld(world, def);

	AStarPathfinder pathfinder(&world);

	Pathfinder::Settings settings{};
	settings.acceptNeighbours = true;

	std::vector<V3s> path;
	Assert(pathfinder.findPath(path, {3, 0, 0}, {3, 4, 0}, settings));

	std::vector<V3s> expected = {{3, 0, 0}, {3, 3, 0}};

	Assert(path.size() == expected.size());

	for (int i = 0; i < path.size(); i++) {
		Assert(path[i] == expected[i]);
	}
}

Test(AStarPathfinderNeighbourStraightLineTest) {
	content::DefinitionManager def{};
	defineTestData(&def);
	World world(&def);
	initWorld(world, def);

	AStarPathfinder pathfinder(&world);

	Pathfinder::Settings settings{};
	settings.acceptNeighbours = true;

	std::vector<V3s> path;
	Assert(pathfinder.findPath(path, {3, 3, 0}, {6, 3, 0}, settings));

	std::vector<V3s> expected = {{3, 3, 0}, {5, 3, 0}};

	Assert(path.size() == expected.size());

	for (int i = 0; i < path.size(); i++) {
		Assert(path[i] == expected[i]);
	}
}
