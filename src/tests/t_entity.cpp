#include "tests.hpp"
#include <sstream>
#include "../world/World.hpp"

using namespace world;

Test(EntityCreateGetTests) {
	content::DefinitionManager defman{};
	auto world = std::make_unique<World>(&defman);
	Assert(world);

	world->createChunkAndActivate(V3s(0, 0, 0));

	world->createChunkAndActivate(V3s(1, 0, 0));

	world->createChunkAndActivate(V3s(0, 1, 0));

	world->createChunkAndActivate(V3s(0, 0, 1));

	Entity *e = world->createEntity(
			V3f(CHUNK_SIZE + 3, 2, 0), Entity::PLAYER_TYPE_NAME);
	Entity *head = e;
	Assert(e);
	Assert(e->id == 1);
	Assert(world->getEntityById(e->id));
	Assert(world->getEntityById(e->id) == e);
	Assert(world->getEntityById(e->id)->id == e->id);
	Assert(world->getEntityList().size() == 1);
	Assert(world->getEntityList().front() == head);
	Assert(e->parent);
	Assert(e->parent->entities.size() == 1);
	Assert(e->parent->pos.x == 1);
	Assert(e->parent->pos.y == 0);
	Assert(e->parent->pos.z == 0);

	e = world->createEntity(
			V3f(CHUNK_SIZE + 3, 3, 0), Entity::PLAYER_TYPE_NAME);
	Assert(e);
	Assert(e->id == 2);
	Assert(world->getEntityById(e->id) == e);
	Assert(world->getEntityById(e->id)->id == e->id);
	Assert(world->getEntityList().size() == 2);
	Assert(world->getEntityList().front() == head);
	Assert(world->getEntityList().back() == e);
	Assert(e->parent);
	Assert(e->parent->entities.size() == 2);
	Assert(e->parent->pos.x == 1);
	Assert(e->parent->pos.y == 0);
	Assert(e->parent->pos.z == 0);

	e = world->createEntity(
			V3f(3, CHUNK_SIZE + 3, 0), Entity::PLAYER_TYPE_NAME);
	Assert(e);
	Assert(e->id == 3);
	Assert(world->getEntityById(e->id) == e);
	Assert(world->getEntityById(e->id)->id == e->id);
	Assert(world->getEntityList().size() == 3);
	Assert(world->getEntityList().front() == head);
	Assert(world->getEntityList().back() == e);
	Assert(e->parent);
	Assert(e->parent->entities.size() == 1);
	Assert(e->parent->pos.x == 0);
	Assert(e->parent->pos.y == 1);
	Assert(e->parent->pos.z == 0);

	e = world->createEntity(V3f(2, 3, 1), Entity::PLAYER_TYPE_NAME);
	Assert(e);
	Assert(e->id == 4);
	Assert(world->getEntityById(e->id) == e);
	Assert(world->getEntityById(e->id)->id == e->id);
	Assert(world->getEntityList().size() == 4);
	Assert(world->getEntityList().front() == head);
	Assert(world->getEntityList().back() == e);
	Assert(e->parent);
	Assert(e->parent->entities.size() == 1);
	Assert(e->parent->pos.x == 0);
	Assert(e->parent->pos.y == 0);
	Assert(e->parent->pos.z == 1);

	Assert(world->getEntityInsertionQueueSize() == 0);
	e = world->createEntity(V3f(3, 1000, 0), Entity::PLAYER_TYPE_NAME);
	Assert(world->getEntityInsertionQueueSize() == 1);

	Assert(world->getEntityList().size() == 5);
	Assert(world->getEntityList().front() == head);
	Assert(world->getEntityList().back() == e);

	world->createChunkAndActivate(V3s(0, 10, 0));
	Assert(world->getEntityInsertionQueueSize() == 1);

	world->createChunkAndActivate(PosToCPos(e->getPosition()));
	Assert(world->getEntityInsertionQueueSize() == 0);
}

Test(EntityGetTests) {
	content::DefinitionManager defman{};
	auto world = std::make_unique<World>(&defman);

	Assert(world);

	world->createChunkAndActivate(V3s(0, 0, 0));

	world->createChunkAndActivate(V3s(1, 0, 0));

	world->createChunkAndActivate(V3s(0, 1, 0));

	auto e = world->createEntity(
			V3f(CHUNK_SIZE + 3, 2, 0), Entity::PLAYER_TYPE_NAME);
	Assert(e->id == 1);
	Assert(world->getEntityById(e->id) == e);
	Assert(e->parent);
	Assert(e->parent->entities.size() == 1);

	e = world->createEntity(
			V3f(CHUNK_SIZE + 3, 3, 0), Entity::PLAYER_TYPE_NAME);
	Assert(e->id == 2);
	Assert(world->getEntityById(e->id) == e);
	Assert(e->parent);
	Assert(e->parent->entities.size() == 2);

	e = world->createEntity(
			V3f(CHUNK_SIZE + 3, 3.1, 0), Entity::PLAYER_TYPE_NAME);
	Assert(e->id == 3);
	Assert(world->getEntityById(e->id) == e);
	Assert(e->parent);
	Assert(e->parent->entities.size() == 3);

	{
		std::vector<Entity *> entities;
		world->getEntities(V3f(CHUNK_SIZE + 3, 2, 0), entities);
		Assert(entities.size() == 1);
		Assert(entities[0] == world->getEntityById(1));
	}
	{
		std::vector<Entity *> entities;
		world->getEntities(V3f(CHUNK_SIZE + 3, 3, 0), entities);
		Assert(entities.size() == 2);
		Assert(entities[0] == world->getEntityById(2));
		Assert(entities[1] == world->getEntityById(3));
	}
	{
		std::vector<Entity *> entities;
		world->getEntities(V3f(CHUNK_SIZE + 3, 3, 0), entities, 1);
		Assert(entities.size() == 1);
		Assert(entities[0] == world->getEntityById(2));
	}
}

Test(EntityRemoveTests) {
	content::DefinitionManager defman{};
	auto world = std::make_unique<World>(&defman);

	Entity *e = nullptr;

	world->createChunkAndActivate(V3s(0, 0, 0));
	world->createChunkAndActivate(V3s(1, 0, 0));

	world->createChunkAndActivate(V3s(0, 1, 0));

	e = world->createEntity(
			V3f(CHUNK_SIZE + 3, 2, 0), Entity::PLAYER_TYPE_NAME);
	Assert(e->id == 1);
	Assert(world->getEntityById(e->id) == e);
	Assert(e->parent);
	Assert(e->parent->entities.size() == 1);

	e = world->createEntity(
			V3f(CHUNK_SIZE + 3, 3, 0), Entity::PLAYER_TYPE_NAME);
	Assert(e->id == 2);
	Assert(world->getEntityById(e->id) == e);
	Assert(e->parent);
	Assert(e->parent->entities.size() == 2);

	auto e2 = world->createEntity(
			V3f(CHUNK_SIZE + 3, 4, 0), Entity::PLAYER_TYPE_NAME);
	Assert(e2->id == 3);
	Assert(world->getEntityById(e2->id) == e2);
	Assert(e2->parent);
	Assert(e2->parent->entities.size() == 3);

	WorldChunk *c = e->parent;
	Assert(world->getEntityById(1) != nullptr);
	Assert(world->getEntityById(1) != e);
	Assert(world->getEntityById(2) == e);
	Assert(c->entities.size() == 3);
	world->removeEntity(e);
	Assert(c->entities.size() == 2);
	Assert(world->getEntityById(3) != nullptr);
	Assert(world->getEntityById(2) == nullptr);
	Assert(world->getEntityById(1) != nullptr);

	e2 = world->createEntity(
			V3f(CHUNK_SIZE + 3, 3, 0), Entity::PLAYER_TYPE_NAME);
	Assert(e2->id == 4);
	Assert(world->getEntityById(e2->id) == e2);
	Assert(e2->parent);
	Assert(e2->parent->entities.size() == 3);
}

Test(EntityPacketMoveTests) {
	content::DefinitionManager defman{};
	auto world = std::make_unique<World>(&defman);

	Entity *e = nullptr;

	world->createChunkAndActivate(V3s(0, 0, 0));

	world->createChunkAndActivate(V3s(1, 0, 0));

	world->createChunkAndActivate(V3s(0, 1, 0));

	e = world->createEntity(
			V3f(CHUNK_SIZE + 3, 2, 0), Entity::PLAYER_TYPE_NAME);
	Assert(e->id == 1);
	Assert(world->getEntityById(e->id) == e);
	Assert(e->parent);
	Assert(e->parent->entities.size() == 1);

	e = world->createEntity(
			V3f(CHUNK_SIZE + 3, 3, 0), Entity::PLAYER_TYPE_NAME);
	e->hasAuthority = false;
	Assert(e->id == 2);
	Assert(world->getEntityById(e->id) == e);
	Assert(e->parent);
	Assert(e->parent->entities.size() == 2);

	auto e2 = world->createEntity(
			V3f(CHUNK_SIZE + 3, 4, 0), Entity::PLAYER_TYPE_NAME);
	Assert(e2->id == 3);
	Assert(world->getEntityById(e2->id) == e2);
	Assert(e2->parent);
	Assert(e2->parent->entities.size() == 3);

	WorldChunk *origin = world->getChunk(V3s(0, 0, 0));
	WorldChunk *c = e->parent;

	Assert(c->pos == V3s(1, 0, 0));
	Assert(origin->pos == V3s(0, 0, 0));
	Assert(world->getEntityById(1) != nullptr);
	Assert(world->getEntityById(1) != e);
	Assert(world->getEntityById(2) == e);
	Assert(c->entities.size() == 3);
	Assert(origin->entities.empty());

	e->move(-6, 0);
	Assert(e->getPosition() == V3f(CHUNK_SIZE - 3, 3, 0));
	Assert(c != e->parent);
}

Test(EntityInterpolationTest) {
	content::DefinitionManager defman{};
	auto world = std::make_unique<World>(&defman);

	auto a = world->createChunkAndActivate(V3s(0, 0, 0));
	auto entity =
			std::make_unique<Entity>(world.get(), Entity::PLAYER_TYPE_NAME, a);

	entity->last_sent_position = V3f(1, 0, 0);
	Assert(entity->getPosition() == V3f(0, 0, 0));
	for (float t = 0; t <= 0.2f; t += 1.f / 60.f) {
		entity->interpolate(1.f / 60.f);
	}
	Assert(entity->getPosition().sqDistance(V3f(1, 0, 0)) < 0.01f);
}

Test(EntityMovementTests) {
	content::DefinitionManager defman{};
	auto world = std::make_unique<World>(&defman);

	auto a = world->createChunkAndActivate(V3s(0, 0, 0));
	world->activateChunk(a);

	auto entity =
			std::make_unique<Entity>(world.get(), Entity::PLAYER_TYPE_NAME, a);

	Assert(world->set(V3s(1, 0, 0), ChunkLayer::Tile, (content_id)2));

	Assert(!entity->move(1, 0));
	Assert(entity->getPosition() == V3f(0, 0, 0));
	Assert(entity->move(0, 1));
	Assert(entity->getPosition() == V3f(0, 1, 0));

	a->terrain[0 + 1 * 16] = 8;

	Assert(!entity->checkForFalling());
	Assert(entity->getPosition() == V3f(0, 1, 0));

	a->terrain[0 + 1 * 16] = 1;

	Assert(!entity->checkForFalling());
	Assert(entity->getPosition() == V3f(0, 1, 0));

	a->terrain[0 + 1 * 16] = 8;

	world->createChunkAndActivate({0, 0, -1});

	Assert(!entity->checkForFalling());
	Assert(entity->getPosition() == V3f(0, 1, 0));

	a->terrain[0 + 1 * 16] = 1;

	Assert(entity->checkForFalling());
	Assert(entity->getPosition() == V3f(0, 1, -1));
}

Test(EntityDamageTests) {
	auto entity = std::make_unique<Entity>(
			nullptr, Entity::PLAYER_TYPE_NAME, nullptr);

	Assert(entity->hp == 100);
	Assert(entity->damage(50) == 50);
	Assert(entity->hp == 50);
	Assert(entity->damage(100) == 50);
	Assert(entity->hp == 0);
	Assert(entity->damage(50) == 0);
	Assert(entity->hp == 0);
}

Test(EntityRangeGetTests) {
	content::DefinitionManager defman{};
	auto world = std::make_unique<World>(&defman);

	//
	// Test no chunks
	//
	{
		std::vector<Entity *> entities;
		world->getEntitiesInRange(V3f(3, 2, 0), entities, 3);
		Assert(entities.empty());
	}

	//
	// Test chump with no entities
	//

	world->createChunkAndActivate(V3s(0, 0, 0));

	{
		std::vector<Entity *> entities;
		world->getEntitiesInRange(V3f(3, 2, 0), entities, 3);
		Assert(entities.empty());
	}

	//
	// Test with chunk above
	//
	{
		world->createChunkAndActivate(V3s(0, 0, 1));

		world->createEntity(V3f(3, 3, 1), Entity::PLAYER_TYPE_NAME);
	}
	{
		std::vector<Entity *> entities;
		world->getEntitiesInRange(V3f(3, 2, 0), entities, 3);
		Assert(entities.empty());
	}

	//
	// Test single chunk with 2 entities, one out of range
	//
	auto entity1 = world->createEntity(V3f(3, 4, 0), Entity::PLAYER_TYPE_NAME);
	Assert(entity1);

	auto entity2 =
			world->createEntity(V3f(3, 5.2, 0), Entity::PLAYER_TYPE_NAME);
	Assert(entity2);

	{
		std::vector<Entity *> entities;
		world->getEntitiesInRange(V3f(3, 2, 0), entities, 3);
		Assert(entities.size() == 1);
		Assert(entities[0] == entity1);
	}

	//
	// Test 2 entities in 1 chunk, but larger range
	//
	{
		std::vector<Entity *> entities;
		world->getEntitiesInRange(V3f(3, 2, 0), entities, CHUNK_SIZE * 3);
		Assert(entities.size() == 2);
		Assert((entities[0] == entity1 && entities[1] == entity2) ||
				(entities[0] == entity2 && entities[1] == entity1));
	}

	//
	// Test 2 entities in different chunk
	//
	world->createChunkAndActivate(V3s(0, -1, 0));

	{
		std::vector<Entity *> entities;
		world->getEntitiesInRange(V3f(3, -1, 0), entities, 7);
		Assert(entities.size() == 2);
		Assert((entities[0] == entity1 && entities[1] == entity2) ||
				(entities[0] == entity2 && entities[1] == entity1));
	}

	//
	// Multi-chunk test
	//
	world->createChunkAndActivate(V3s(1, 1, 0));
	world->createChunkAndActivate(V3s(1, 0, 0));

	{
		std::vector<Entity *> entities;
		world->getEntitiesInRange(V3f(3, 2, 0), entities, CHUNK_SIZE * 3);
		Assert(entities.size() == 2);
		Assert((entities[0] == entity1 && entities[1] == entity2) ||
				(entities[0] == entity2 && entities[1] == entity1));
	}

	auto entity3 = world->createEntity(
			V3f(CHUNK_SIZE + 1, 3, 0), Entity::PLAYER_TYPE_NAME);
	Assert(entity3);

	{
		std::vector<Entity *> entities;
		world->getEntitiesInRange(V3f(3, 2, 0), entities, CHUNK_SIZE * 3);
		Assert(entities.size() == 3);
		Assert((entities[0] == entity1 && entities[1] == entity2) ||
				(entities[0] == entity2 && entities[1] == entity1));
		Assert(entities[2] == entity3);
	}

	//
	// Negative chunk test
	//

	world->createChunkAndActivate(V3s(-1, 0, 0));
	world->createChunkAndActivate(V3s(-1, -1, 0));

	{
		std::vector<Entity *> entities;
		world->getEntitiesInRange(V3f(3, 2, 0), entities, CHUNK_SIZE * 3);
		Assert(entities.size() == 3);
		Assert((entities[0] == entity1 && entities[1] == entity2) ||
				(entities[0] == entity2 && entities[1] == entity1));
		Assert(entities[2] == entity3);
	}

	auto entity4 = world->createEntity(V3f(-1, 0, 0), Entity::PLAYER_TYPE_NAME);
	Assert(entity4);

	{
		std::vector<Entity *> entities;
		world->getEntitiesInRange(V3f(3, 2, 0), entities, CHUNK_SIZE * 3);
		Assert(entities.size() == 4);
		Assert((entities[0] == entity1 && entities[1] == entity2) ||
				(entities[0] == entity2 && entities[1] == entity1));
		Assert(entities[2] == entity3);
		Assert(entities[3] == entity4);
	}

	{
		std::vector<Entity *> entities;
		world->getEntitiesInRange(V3f(0, 0, 0), entities, 2);
		Log("EntityTests", INFO) << entities.size();
		Assert(entities.size() == 1);
		Assert(entities[0] == entity4);
	}
}
