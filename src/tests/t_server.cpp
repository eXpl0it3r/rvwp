#include "tests.hpp"
#include "../network/net.hpp"
#include "../network/packets.hpp"
#include "../network/protocol.hpp"
#include "server/Server.hpp"

using namespace world;

Test(EmergeTests) {
	server::Server server;

	Assert(server.start(30101));
	Assert(server.getWorld()->getChunk(V3s(0, 0, 0)));

	server::Peer client("tester", network::Address(127, 0, 0, 1, 43333));
	World *world = server.getWorld();
	Assert(world);
	WorldChunk *chunk = world->getChunk(V3s(0, 0, 0));
	Assert(chunk);
	size_t entitySize = chunk->entities.size();
	//	TEST(chunk->entities.size() == 0);
	Assert(server.emergePlayer(&client, V3f(0, 0, 0)));
	Assert(client.entity);
	Assert(client.entity->id >= 0);
	Assert(client.entity->parent == chunk);
	Assert(chunk->entities.size() == entitySize + 1);
	Assert(server.leavePlayer(&client));
	Assert(!client.entity);
}

Test(PeerTests) {
	server::Peer client("tester", network::Address(127, 0, 0, 1, 43333));

	Assert(!client.hasChunk(V3s(1, 1, 1)));
	Assert(!client.hasChunk(V3s(-1, -1, -1)));
	Assert(!client.hasChunk(V3s(1, 2, 1)));
	Assert(!client.hasChunk(V3s(1, 1, 2)));
	Assert(!client.hasChunk(V3s(2, 1, 1)));

	client.setChunkSent(V3s(1, 1, 1));

	Assert(client.hasChunk(V3s(1, 1, 1)));
	Assert(!client.hasChunk(V3s(-1, -1, -1)));
	Assert(!client.hasChunk(V3s(1, 2, 1)));
	Assert(!client.hasChunk(V3s(1, 1, 2)));
	Assert(!client.hasChunk(V3s(2, 1, 1)));
}
