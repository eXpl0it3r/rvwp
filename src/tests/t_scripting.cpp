#include "tests.hpp"
#include "../scripting/ModConfig.hpp"
#include "../scripting/ModOrderResolver.hpp"
#include "../scripting/ModException.hpp"
#include <sstream>
#include <iostream>

using namespace scripting;

ModConfig parse(std::string json, std::string path) {
	Json::Value root;
	std::istringstream is(json);
	is >> root;
	return ModConfig::readFromJSON(root, path);
}

Test(ModConfigReadTests) {
	ModConfig config = parse("{"
							 "\"name\": \"mymod\","
							 "\"author\": \"rubenwardy\","
							 "\"custom\": \"blah\""
							 "}",
			"hello");

	Assert(config.name == "mymod");
	Assert(config.author == "rubenwardy");
	Assert(config.path == "hello");
	Assert(config.dependencies.empty());
}

Test(ModConfigReadWithDepsTests) {
	ModConfig config = parse("{"
							 "\"name\": \"mymod\","
							 "\"author\": \"rubenwardy\","
							 "\"custom\": \"blah\","
							 "\"dependencies\": [ \"mod1\", \"mod2\" ]"
							 "}",
			"hello");

	Assert(config.name == "mymod");
	Assert(config.author == "rubenwardy");
	Assert(config.path == "hello");
	Assert(config.dependencies.size() == 2);
	Assert(config.dependencies[0] == "mod1");
	Assert(config.dependencies[1] == "mod2");
}

Test(ModOrderResolverTests) {
	std::vector<ModConfig> mods;
	mods.push_back({
			"mod1",
			"rubenwardy",
			"",
			std::vector<std::string>{"mod3", "mod4"},
	});
	mods.push_back({"mod2", "rubenwardy", "", {}});
	mods.push_back({
			"mod3",
			"rubenwardy",
			"",
			std::vector<std::string>{"mod4"},
	});
	mods.push_back({"mod4", "rubenwardy", "", {}});

	ModOrderResolver resolver(mods);
	auto orderedMods = resolver.resolve();

	Assert(orderedMods[0].name == "mod4");
	Assert(orderedMods[1].name == "mod3");
	Assert(orderedMods[2].name == "mod1" || orderedMods[3].name == "mod1");
	Assert(orderedMods[2].name == "mod2" || orderedMods[3].name == "mod2");
}

Test(ModOrderResolverCycleTests) {
	std::vector<ModConfig> mods;
	mods.push_back({
			"mod1",
			"rubenwardy",
			"",
			std::vector<std::string>{"mod2"},
	});

	mods.push_back({
			"mod2",
			"rubenwardy",
			"",
			std::vector<std::string>{"mod1"},
	});

	ModOrderResolver resolver(mods);
	try {
		resolver.resolve();
	} catch (const ModException &ex) {
		return;
	}

	Assert("Expected exception to be thrown");
}

Test(ModOrderResolverMissingTests) {
	std::vector<ModConfig> mods;
	mods.push_back({
			"mod1",
			"rubenwardy",
			"",
			std::vector<std::string>{"mod2"},
	});

	mods.push_back({"mod3", "rubenwardy", "", {}});

	ModOrderResolver resolver(mods);
	try {
		resolver.resolve();
	} catch (const ModException &ex) {
		return;
	}

	Assert("Expected exception to be thrown");
}

Test(ModOrderResolverRedundantTests) {
	std::vector<ModConfig> mods;

	mods.push_back(
			{"modB", "rubenwardy", "", std::vector<std::string>{"modC"}});
	mods.push_back({
			"modA",
			"rubenwardy",
			"",
			std::vector<std::string>{"modB", "modC"},
	});
	mods.push_back({
			"modC",
			"rubenwardy",
			"",
			{},
	});

	ModOrderResolver resolver(mods);
	auto orderedMods = resolver.resolve();

	Assert(orderedMods[0].name == "modC");
	Assert(orderedMods[1].name == "modB");
	Assert(orderedMods[2].name == "modA");
}
