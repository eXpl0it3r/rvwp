#include "tests.hpp"
#include "network/packets.hpp"
#include "network/readwrite.hpp"
#include "network/protocol.hpp"
#include "content/content.hpp"
#include "content/Inventory.hpp"

Test(CreatePacketTest) {
	const int size = 2 + 2 + 1 + 2 + 1;
	network::Packet pkt(network::Address(), TOSERVER_HELLO, size);
	Assert(!pkt.end() && !pkt.error());
	pkt << (u16)5;
	Assert(!pkt.end() && !pkt.error());
	pkt << std::string("u");
	Assert(!pkt.end() && !pkt.error());
	pkt << std::string("p");
	Assert(pkt.end() && !pkt.error());

	u8 *data = (u8 *)pkt.getActualData();
	u8 expected[] = {0x72, 0x76, 0x77, 0x70, TOSERVER_HELLO, 0x00, 0x05, 0x00,
			0x01, 0x75, 0x00, 0x01, 0x70};

	for (int i = 0; i < size; i++) {
		Assert(expected[i] == data[i]);
	}
}

Test(ReadPacketTest) {
	const int size = 4 + 1 + 2 + 2 + 1 + 2 + 1;
	u8 packet[] = {0x72, 0x76, 0x77, 0x70, TOSERVER_HELLO, 0x00, 0x05, 0x00,
			0x01, 0x75, 0x00, 0x01, 0x70};
	network::Packet pkt(network::Address(), packet, size);
	Assert(!pkt.end() && !pkt.error());
	Assert(pkt.getType() == TOSERVER_HELLO);

	u16 num = 0;
	std::string a, b;

	pkt >> num;
	Assert(!pkt.end() && !pkt.error());
	pkt >> a;
	Assert(!pkt.end() && !pkt.error());
	pkt >> b;

	Assert(num == 5);
	Assert(a == "u");
	Assert(b == "p");
	Assert(pkt.end() && !pkt.error());
}

Test(InvalidProtocolPacketTest) {
	const int size = 4 + 1 + 2 + 2 + 1 + 2 + 1;
	u8 packet[] = {0x72, 0x76, 0x77, 0x71, 0x01, 0x00, 0x05, 0x00, 0x01, 0x75,
			0x00, 0x01, 0x70};
	network::Packet pkt(network::Address(), packet, size);
	Assert(pkt.error());
}

Test(VectorSerializationTest) {
	const int size = 3 * 4;
	u8 data[size];

	V3f pos(10, 10, 1);
	network::writeV3f(data, pos);
	Assert(pos == network::readV3f(data));

	pos = V3f(10.1, 10.1, 1);
	network::writeV3f(data, pos);
	Assert(pos == network::readV3f(data));

	pos = V3f(-10.1, 10.1, 1);
	network::writeV3f(data, pos);
	Assert(pos == network::readV3f(data));

	pos = V3f(10.1, -10.1, 1);
	network::writeV3f(data, pos);
	Assert(pos == network::readV3f(data));
}

Test(InventorySerializationTest) {
	content::Inventory inv{{"player:one"}};
	inv.newList("main", 3, 3);
	inv.newList("two", 3, 3);

	network::Packet pkt(network::Address(), TOCLIENT_PLAYER_INVENTORY,
			inv.serialised_size());
	inv.serialise(pkt);
	Assert(pkt.end());

	pkt.reset();

	content::Inventory inv2{{"player:one"}};
	inv2.deserialise(pkt);
	Assert(inv.equals(inv2));
}
