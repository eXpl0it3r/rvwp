#include "tests.hpp"
#include "types/Controller.hpp"

namespace {

class TestController : public types::Controller {
public:
	bool called = false;

	void update(float dtime) override { called = true; }
};

} // namespace

Test(ControllerUpdateTest) {
	TestController controller{};

	Assert(controller.isEnabled());
	Assert(!controller.called);
	controller.update_if_enabled(1.f);
	Assert(controller.called);
	controller.called = false;

	controller.disable();

	Assert(!controller.called);
	controller.update_if_enabled(1.f);
	Assert(!controller.called);
	controller.update(1.f);
	Assert(controller.called);

	Assert(!controller.isEnabled());
	controller.enable();
	Assert(controller.isEnabled());
}

Test(ControllerEnableDisableTest) {
	TestController controller{};

	Assert(controller.isEnabled());
	controller.disable();
	Assert(!controller.isEnabled());
	controller.enable();
	Assert(controller.isEnabled());
}

Test(ControllerChildrenAddRemove) {
	TestController root{};
	TestController a{};

	Assert(root.isEnabled());

	Assert(!root.called);
	root.update_if_enabled(1.f);
	Assert(root.called);
	Assert(!a.called);
	root.called = false;

	root.addChild(&a);

	Assert(!root.called);
	root.update_if_enabled(1.f);
	Assert(root.called);
	Assert(a.called);
	root.called = false;
	a.called = false;

	root.removeChild(&a);

	Assert(!root.called);
	root.update_if_enabled(1.f);
	Assert(root.called);
	Assert(!a.called);
}

Test(ControllerChildrenEnableDisableTest) {
	TestController root{};
	TestController a{};
	TestController b{};

	root.addChild(&a);
	a.addChild(&b);

	Assert(root.isEnabled());
	Assert(a.isEnabled());
	Assert(b.isEnabled());

	Assert(!root.called);
	Assert(!a.called);
	Assert(!b.called);
	root.update_if_enabled(1.f);
	Assert(root.called);
	Assert(a.called);
	Assert(b.called);
	root.called = false;
	a.called = false;
	b.called = false;

	b.disable();

	Assert(root.isEnabled());
	Assert(a.isEnabled());
	Assert(!b.isEnabled());

	Assert(!root.called);
	Assert(!a.called);
	Assert(!b.called);
	root.update_if_enabled(1.f);
	Assert(root.called);
	Assert(a.called);
	Assert(!b.called);
	root.called = false;
	a.called = false;

	root.disable();

	Assert(!root.isEnabled());
	Assert(!a.isEnabled());
	Assert(!b.isEnabled());

	b.enable();

	Assert(!root.isEnabled());
	Assert(!a.isEnabled());
	Assert(!b.isEnabled());

	TestController c{};
	b.addChild(&c);

	Assert(!root.isEnabled());
	Assert(!a.isEnabled());
	Assert(!b.isEnabled());
	Assert(!c.isEnabled());

	root.enable();

	Assert(root.isEnabled());
	Assert(a.isEnabled());
	Assert(b.isEnabled());
	Assert(c.isEnabled());
}
