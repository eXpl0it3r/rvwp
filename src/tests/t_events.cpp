#include "tests.hpp"

#include "events/EventListener.hpp"
#include "events/EventBus.hpp"

namespace {

struct TestEvent {
	int value;
};

} // namespace

Test(EventsScopedListenerTest) {
	EventBus<TestEvent> bus;

	bool called = false;
	auto func = [&called](auto event) { called = true; };

	{
		Assert(!called);
		EventListener<TestEvent> listener{&bus, func};
		Assert(listener.isAttached());

		Assert(!called);
		bus.push({13});
		Assert(!called);
		bus.flush();
		Assert(called);

		called = false;
	}

	Assert(!called);
	bus.push({13});
	Assert(!called);
	bus.flush();
	Assert(!called);
}

Test(EventsScopedBusTest) {
	bool called = false;
	EventListener<TestEvent> listener{
			nullptr, [&called](auto event) { called = true; }};
	Assert(!called);
	Assert(!listener.isAttached());

	{
		EventBus<TestEvent> bus;
		listener.attach(&bus);
		Assert(listener.isAttached());

		Assert(!called);
		bus.push({13});
		Assert(!called);
		bus.flush();
		Assert(called);

		Assert(listener.isAttached());
	}

	Assert(!listener.isAttached());
}

Test(EventsDetachListenerTest) {
	EventBus<TestEvent> bus;

	bool called = false;
	auto func = [&called](auto event) { called = true; };

	Assert(!called);
	EventListener<TestEvent> listener{&bus, func};
	Assert(listener.isAttached());

	Assert(!called);
	bus.push({13});
	Assert(!called);
	bus.flush();
	Assert(called);

	called = false;

	listener.detach();
	Assert(!listener.isAttached());

	Assert(!called);
	bus.push({13});
	Assert(!called);
	bus.flush();
	Assert(!called);
}
