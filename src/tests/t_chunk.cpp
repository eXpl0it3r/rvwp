#include "tests.hpp"
#include "../world/Chunk.hpp"

using namespace world;

Test(ChunkSolidTest) {
	WorldChunk chunk(V3s(0, 0, 0));
	Assert(!chunk.isFloorSolid());
	Assert(!chunk.isSolid());

	for (int ry = 0; ry < CHUNK_SIZE; ry++) {
		for (int rx = 0; rx < CHUNK_SIZE; rx++) {
			chunk.terrain[rx + ry * CHUNK_SIZE] = 4;
		}
	}

	Assert(chunk.isFloorSolid());
	Assert(!chunk.isSolid());

	chunk.terrain[0 + 0 * CHUNK_SIZE] = 1;

	Assert(!chunk.isFloorSolid());
	Assert(!chunk.isSolid());

	for (int ry = 0; ry < CHUNK_SIZE; ry++) {
		for (int rx = 0; rx < CHUNK_SIZE; rx++) {
			chunk.terrain[rx + ry * CHUNK_SIZE] = 1;
			chunk.floort[rx + ry * CHUNK_SIZE].cid = 4;
		}
	}

	Assert(chunk.isFloorSolid());
	Assert(!chunk.isSolid());

	chunk.floort[0 + 0 * CHUNK_SIZE].clear();

	Assert(!chunk.isFloorSolid());
	Assert(!chunk.isSolid());

	for (int ry = 0; ry < CHUNK_SIZE; ry++) {
		for (int rx = 0; rx < CHUNK_SIZE; rx++) {
			chunk.tiles[rx + ry * CHUNK_SIZE].cid = 4;
		}
	}

	Assert(chunk.isFloorSolid());
	Assert(chunk.isSolid());
}

Test(ChunkClearTest) {
	WorldChunk chunk(V3s(0, 0, 0));

	for (int ry = 0; ry < CHUNK_SIZE; ry++) {
		for (int rx = 0; rx < CHUNK_SIZE; rx++) {
			Assert(chunk.terrain[rx + ry * CHUNK_SIZE] == 1);
		}
	}

	for (int ry = 0; ry < CHUNK_SIZE; ry++) {
		for (int rx = 0; rx < CHUNK_SIZE; rx++) {
			chunk.terrain[rx + ry * CHUNK_SIZE] = 4;
		}
	}

	chunk.clear();

	for (int ry = 0; ry < CHUNK_SIZE; ry++) {
		for (int rx = 0; rx < CHUNK_SIZE; rx++) {
			Assert(chunk.terrain[rx + ry * CHUNK_SIZE] == 1);
		}
	}
}

Test(ChunkTileTimerTest) {
	const TileTimer expectedTimer1 = {
			V3s(17, 4, 0), ChunkLayer::Tile, "foo", 90.f};

	const TileTimer expectedTimer2 = {
			V3s(18, 4, 0), ChunkLayer::Tile, "bar", 50.f};

	const TileTimer expectedTimer3 = {
			V3s(19, 7, 0), ChunkLayer::Tile, "baz", 100.f};

	WorldChunk chunk(V3s(1, 0, 0));

	std::vector<TileTimer> timers;
	chunk.popExpiredTimers(timers, 100.f);
	Assert(timers.empty());
	Assert(chunk.timers.empty());

	Assert(chunk.pushTimer(expectedTimer1));
	Assert(chunk.timers.size() == 1);
	chunk.popExpiredTimers(timers, 50.f);
	Assert(timers.empty());
	Assert(chunk.timers.size() == 1);
	chunk.popExpiredTimers(timers, 89.f);
	Assert(timers.empty());
	Assert(chunk.timers.size() == 1);
	chunk.popExpiredTimers(timers, 100.f);
	Assert(timers.size() == 1);
	Assert(timers[0].position == expectedTimer1.position);
	Assert(timers[0].layer == expectedTimer1.layer);
	Assert(timers[0].event == expectedTimer1.event);
	Assert(chunk.timers.empty());
	timers.clear();

	Assert(chunk.pushTimer(expectedTimer1));
	Assert(chunk.pushTimer(expectedTimer2));
	Assert(chunk.pushTimer(expectedTimer3));

	Assert(chunk.timers.size() == 3);
	chunk.popExpiredTimers(timers, 49.f);
	Assert(timers.empty());
	Assert(chunk.timers.size() == 3);
	chunk.popExpiredTimers(timers, 55.f);
	Assert(timers.size() == 1);
	Assert(timers[0].position == expectedTimer2.position);
	Assert(timers[0].layer == expectedTimer2.layer);
	Assert(timers[0].event == expectedTimer2.event);
	Assert(chunk.timers.size() == 2);
	Assert(chunk.timers[0].layer == expectedTimer1.layer);
	Assert(chunk.timers[0].event == expectedTimer1.event);
	Assert(chunk.timers[1].layer == expectedTimer3.layer);
	Assert(chunk.timers[1].event == expectedTimer3.event);
	timers.clear();

	Assert(chunk.timers.size() == 2);
	chunk.popExpiredTimers(timers, 200.f);
	Assert(timers.size() == 2);
	Assert(timers[0].position == expectedTimer1.position);
	Assert(timers[0].layer == expectedTimer1.layer);
	Assert(timers[0].event == expectedTimer1.event);
	Assert(timers[1].position == expectedTimer3.position);
	Assert(timers[1].layer == expectedTimer3.layer);
	Assert(timers[1].event == expectedTimer3.event);
	Assert(chunk.timers.empty());
}
