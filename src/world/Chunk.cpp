#include "Chunk.hpp"
#include <cstring>

using namespace world;

WorldChunk::WorldChunk(const V3s &pos) : pos(pos) {
	for (int ry = 0; ry < CHUNK_SIZE; ry++) {
		for (int rx = 0; rx < CHUNK_SIZE; rx++) {
			terrain[rx + ry * CHUNK_SIZE] = CID_VOID;
		}
	}
}

bool WorldChunk::isFloorSolid() const {
	for (int i = 0; i < CHUNK_SIZE * CHUNK_SIZE; i++) {
		if (terrain[i] == 1 && floort[i].empty() && tiles[i].empty()) {
			return false;
		}
	}

	return true;
}

bool WorldChunk::isSolid() const {
	for (auto tile : tiles) {
		if (tile.empty()) {
			return false;
		}
	}

	return true;
}

void WorldChunk::clear() {
	for (auto &tile : tiles) {
		tile.cid = 0;
	}

	for (auto &terr : terrain) {
		terr = CID_VOID;
	}

	for (auto &tile : floort) {
		tile.cid = 0;
	}

	memset(lightLevels, 0, sizeof(lightLevels));
}

void WorldChunk::printLightmap() const {
	printf("\nLightmap for %d, %d, %d:\n", pos.x, pos.y, pos.z);
	for (int y = 0; y < CHUNK_SIZE; y++) {
		printf("\t");
		for (int x = 0; x < CHUNK_SIZE; x++) {
			printf("%04X, ", lightLevels[x + y * CHUNK_SIZE]);
		}
		printf("\n");
	}
	printf("\n");
}

bool WorldChunk::pushTimer(const TileTimer &timer) {
	u8 index = getIndex(timer.position);

	for (auto &existingTimer : timers) {
		if (existingTimer.index == index &&
				existingTimer.layer == timer.layer &&
				existingTimer.event == timer.event) {
			if (existingTimer.expiresAt < timer.expiresAt) {
				return false;
			}

			existingTimer.expiresAt = timer.expiresAt;
			return true;
		}
	}

	timers.push_back({index, timer.layer, timer.event, timer.expiresAt});

	return true;
}

void WorldChunk::popExpiredTimers(
		std::vector<TileTimer> &expiredTimers, float gameTime) {
	size_t insert_idx = 0;

	for (size_t i = 0; i < timers.size(); ++i) {
		const auto &timer = timers[i];
		if (timer.expiresAt < gameTime) {
			const V3s position = pos * CHUNK_SIZE +
					V3s(timer.index % CHUNK_SIZE, timer.index / CHUNK_SIZE, 0);
			expiredTimers.push_back(
					{position, timer.layer, timer.event, timer.expiresAt});
			continue;
		}

		if (insert_idx != i) {
			timers[insert_idx] = timers[i];
		}

		insert_idx++;
	}

	// Some timers expired, reduce vector size
	if (insert_idx < timers.size()) {
		timers.resize(insert_idx);
	} else {
		assert(insert_idx == timers.size());
	}
}
