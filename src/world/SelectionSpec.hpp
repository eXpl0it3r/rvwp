#pragma once

#include "../types/types.hpp"
#include "Tile.hpp"
#include "Entity.hpp"

namespace world {

struct SelectionSpec {
	enum SST { SST_None = 0, SST_Tile, SST_Entity };

	SST type = SST_None;
	WorldTile *tile = nullptr;
	Entity *entity = nullptr;
	V3f pos;

	operator bool() const { return type != SST_None; }

	static SelectionSpec makeEmpty() { return {}; }

	static SelectionSpec makeTile(WorldTile *tile, V3f pos) {
		return {SST_Tile, tile, nullptr, pos};
	}

	static SelectionSpec makeEntity(Entity *entity, V3f pos) {
		return {SST_Entity, nullptr, entity, pos};
	}
};

std::ostream &operator<<(std::ostream &out, const SelectionSpec &spec);

} // namespace world
