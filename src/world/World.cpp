#include "World.hpp"
#include <cmath>
#include <log.hpp>
#include <cassert>
#include <unordered_map>
#include <unordered_set>
#include <sanity.hpp>

using namespace world;

WorldChunk *World::getChunk(const V3s &cpos, ChunkState *state) {
	if (chunkCache && chunkCache->pos == cpos) {
		if (state) {
			*state = chunkCache->getState();
		}
		return chunkCache;
	}

	auto it = sectors.find(V2s(cpos));
	if (it != sectors.end()) {
		for (const auto &chunk : it->second.chunks) {
			if (chunk->pos.z == cpos.z) {
				if (state) {
					*state = chunk->getState();
				}
				chunkCache = chunk.get();
				return chunkCache;
			}
		}
	}

	if (state) {
		*state = CHUNK_NULL;
	}

	return nullptr;
}

WorldChunk *World::getOrCreateChunk(const V3s &pos) {
	if (chunkCache && chunkCache->pos == pos) {
		return chunkCache;
	}

	WorldSector *sector = nullptr;

	// Find existing sector
	auto it = sectors.find(V2s(pos));
	if (it == sectors.end()) {
		// No such sector
		sector = &sectors.emplace(V2s(pos), WorldSector()).first->second;
	} else {
		sector = &it->second;
		for (const auto &chunk : sector->chunks) {
			if (chunk->pos.z == pos.z) {
				chunkCache = chunk.get();
				return chunkCache;
			}
		}
	}

	auto chunkPtr = std::make_unique<WorldChunk>(pos);
	auto chunk = chunkPtr.get();
	sector->chunks.emplace_back(std::move(chunkPtr));
	onChunkEmerge(chunk);
	chunkCache = chunk;
	return chunk;
}

void World::onChunkEmerge(WorldChunk *c) {
	c->bumpState(CHUNK_LOADED);

	if (bus) {
		bus->push(WorldEvent::chunkEmerged(c));
	}

	// TODO: optimise
	std::list<Entity *> buffer = entity_insertion_queue;
	entity_insertion_queue.clear();

	for (auto e : buffer) {
		if (PosToCPos(e->getPosition()) == c->pos) {
			reinsertEntityToChunk(e);
		} else {
			entity_insertion_queue.push_back(e);
		}
	}
}

void World::getChunksInArea(std::vector<WorldChunk *> &res, const V3f &position,
		float radius, ChunkState minState) {
	assert(res.empty());

	V3s bpos(static_cast<s32>(std::floor(position.x / (float)CHUNK_SIZE)),
			static_cast<s32>(std::floor(position.y / (float)CHUNK_SIZE)),
			position.z);

	for (const auto &it : sectors) {
		const auto &pos = it.first;
		if (pos.x > bpos.x - radius && pos.x < bpos.x + radius &&
				pos.y > bpos.y - radius && pos.y < bpos.y + radius) {
			for (const auto &chunk : it.second.chunks) {
				if (chunk->pos.z == position.z && chunk->getState() >= minState)
					res.push_back(chunk.get());
			}
		}
	}
}

int World::getChunkCount() const {
	int counter = 0;
	for (const auto &it : sectors) {
		counter += it.second.chunks.size();
	}
	return counter;
}

void World::getDirtyChunks(std::vector<WorldChunk *> &res) {
	assert(res.empty());

	for (const auto &it : sectors) {
		for (const auto &chunk : it.second.chunks) {
			if (chunk->dirty) {
				res.push_back(chunk.get());
			}
		}
	}
}

void World::getNeighbouringChunks2d(
		std::vector<WorldChunk *> &res, const V3s &cpos, ChunkState minState) {
	for (const auto &delta : NEIGHBOURS_8) {
		ChunkState state;
		auto chunk = getChunk(cpos + V3s(delta.x, delta.y, 0), &state);
		if (state >= minState) {
			res.push_back(chunk);
		}
	}
}

void World::getNeighbouringChunks3d(
		std::vector<WorldChunk *> &res, const V3s &cpos, ChunkState minState) {
	// quarter_diagonal = sqrt(1^2 + 1^2) / 2 = sqrt(2) / 3 = 0.7071
	getNeighbouringChunks2d(res, cpos + V3s(0, 0, -1), minState);
	getNeighbouringChunks2d(res, cpos + V3s(0, 0, 0), minState);
	getNeighbouringChunks2d(res, cpos + V3s(0, 0, 1), minState);

	ChunkState state;
	auto chunk = getChunk(cpos + V3s(0, 0, -1), &state);
	if (state >= minState) {
		res.push_back(chunk);
	}

	chunk = getChunk(cpos + V3s(0, 0, 1), &state);
	if (state >= minState) {
		res.push_back(chunk);
	}
}

WorldTile *World::set(const V3s &pos, ChunkLayer layer, const WorldTile &tile) {
	WorldChunk *chunk = getChunk(PosToCPos(pos));
	if (!chunk || !chunk->isActive()) {
		return nullptr;
	}

	int idx = chunk->getIndex(pos);

	switch (layer) {
	case ChunkLayer::Tile:
		chunk->tiles[idx] = tile;
		if (bus) {
			bus->push(WorldEvent::setTile(chunk, pos, layer));
		}
		return &chunk->tiles[idx];
	case ChunkLayer::Floor:
		chunk->floort[idx] = tile;
		if (bus) {
			bus->push(WorldEvent::setTile(chunk, pos, layer));
		}
		return &chunk->floort[idx];
	}

	FatalError("Unknown layer in World::set")
}

WorldTile *World::get(const V3s &pos, ChunkLayer layer, ChunkState *state) {
	WorldChunk *chunk = getChunk(PosToCPos(pos), state);
	if (!chunk) {
		return nullptr;
	}

	int idx = chunk->getIndex(pos);

	WorldTile *tile = nullptr;
	switch (layer) {
	case ChunkLayer::Floor:
		tile = &chunk->floort[idx];
		break;
	case ChunkLayer::Tile:
		tile = &chunk->tiles[idx];
		break;
	}

	SanityCheck(tile);

	if (tile->empty()) {
		return nullptr;
	}

	return tile;
}

content_id World::getFloorTerrain(const V3s &pos, ChunkState *state) {
	WorldChunk *chunk = getChunk(PosToCPos(pos), state);
	if (!chunk) {
		return 0;
	}

	int idx = chunk->getIndex(pos);

	if (!chunk->floort[idx].empty())
		return chunk->floort[idx].cid;

	return chunk->terrain[idx];
}

std::optional<std::shared_ptr<content::KeyValueStore>> World::getMetaData(
		const V3s &pos, ChunkLayer layer) {
	WorldChunk *chunk = getChunk(PosToCPos(pos));
	if (!chunk) {
		return {};
	}

	return chunk->getMetaData(pos, layer);
}

bool World::hasGround(const V3s &pos) {
	const WorldChunk *chunk = getChunk(PosToCPos(pos));
	if (!chunk) {
		return true;
	}

	int idx = chunk->getIndex(pos);
	return !chunk->floort[idx].empty() || chunk->terrain[idx] > CID_VOID;
}

bool World::collidesAt(const V3f &from, const V3f &to) {
	// TODO: Make collision detection ray based.
	ChunkState state;
	WorldTile *tile = get(to.floor(), ChunkLayer::Tile, &state);
	if (state < CHUNK_LOADED) {
		return true;
	} else if (!tile || tile->cid == 0) {
		return false;
	}

	auto def = defman->getTileDef(tile->cid);
	if (!def) {
		return true;
	}

	if (def->collides) {
		for (const auto &box : def->collisionBoxes) {
			if (box.contains(to - to.floor().floating())) {
				return true;
			}
		}
	}

	return false;
}

bool World::hasAnything(const V3s &pos) {
	const WorldChunk *chunk = getChunk(PosToCPos(pos));
	if (!chunk) {
		return true;
	}

	int idx = chunk->getIndex(pos);
	return !chunk->floort[idx].empty() || chunk->terrain[idx] > CID_VOID ||
			!chunk->tiles[idx].empty();
}

bool World::canStandAt(const V3s &pos) {
	const WorldChunk *chunk = getChunk(PosToCPos(pos));
	if (!chunk) {
		return false;
	}

	int idx = chunk->getIndex(pos);
	if (chunk->floort[idx].empty() && chunk->terrain[idx] <= CID_VOID) {
		return false;
	}
	if (chunk->tiles[idx].empty()) {
		return true;
	}

	auto def = defman->getTileDef(chunk->tiles[idx].cid);
	return def && !def->collides;
}

std::optional<int> World::getSurfaceHeight(V3s pos) {
	V3s cpos = PosToCPos(pos);

	auto it = sectors.find(V2s(cpos));
	if (it == sectors.end()) {
		return {};
	}

	std::optional<int> height = {};
	for (const auto &chunk : it->second.chunks) {
		pos.z = chunk->pos.z;
		if ((!height.has_value() || height.value() > chunk->pos.z) &&
				canStandAt(pos)) {
			height = chunk->pos.z;
		}
	}

	return height;
}

void World::findNodesWithMetaData(
		std::vector<std::pair<V3s, WorldTile *>> &result, const V3f &pos,
		float range, ChunkLayer layer, const std::string &metadata) {
	assert(result.empty());

	float sqRange = range * range;
	std::vector<WorldChunk *> queue;
	std::unordered_set<V3s> visited;
	V3s originChunkPos = PosToCPos(pos);

	auto originChunk = getChunk(originChunkPos);
	if (originChunk) {
		queue.push_back(originChunk);
	}
	visited.insert(originChunkPos);

	while (!queue.empty()) {
		WorldChunk *chunk = queue[queue.size() - 1];
		queue.pop_back();

		u8 idx = 0;
		for (int y = 0; y < 16; y++) {
			for (int x = 0; x < 16; x++) {
				assert(idx == x + y * CHUNK_SIZE);

				auto meta = chunk->getMetaDataInternalNoCreate(idx, layer);
				if (meta.has_value() && meta.value()->has(metadata)) {
					auto tile = chunk->getInternal(idx, layer);
					assert(!tile->empty());
					result.emplace_back(chunk->pos + V3s(x, y, 0), tile);
				}

				idx++;
			}
		}

		for (const auto &offset : AROUND_2D) {
			V3s newChunkPos = chunk->pos + offset;
			if (visited.find(newChunkPos) != visited.end()) {
				V3f nearPos = CPosToPos(newChunkPos).floating();

				if (pos.x > nearPos.x + CHUNK_SIZE) {
					nearPos.x += CHUNK_SIZE - 1;
				} else if (pos.x > nearPos.x) {
					nearPos.x = pos.x;
				}

				if (pos.y > nearPos.y + CHUNK_SIZE) {
					nearPos.y += CHUNK_SIZE - 1;
				} else if (pos.y > nearPos.y) {
					nearPos.y = pos.y;
				}

				if (nearPos.sqDistance(pos) < sqRange) {
					auto newChunk = getChunk(newChunkPos);
					if (newChunk) {
						visited.insert(newChunkPos);
						queue.push_back(newChunk);
					}
				}
			}
		}
	}
}

bool World::pushTimer(const TileTimer &timer) {
	WorldChunk *chunk = getChunk(PosToCPos(timer.position));
	if (!chunk) {
		return false;
	}

	if (!chunk->pushTimer(timer)) {
		return false;
	}

	if (timer.expiresAt < nextTimerCheck) {
		nextTimerCheck = timer.expiresAt;
	}

	return true;
}

void World::popExpiredTimers(std::vector<TileTimer> &timers, float gameTime) {
	assert(timers.empty());

	if (gameTime < nextTimerCheck) {
		return;
	}

	for (auto &sector : sectors) {
		for (auto &chunk : sector.second.chunks) {
			if (chunk && chunk->isActive()) {
				chunk->popExpiredTimers(timers, gameTime);
			}
		}
	}
}

Entity *World::createEntity(const V3f &pos, const std::string &typeName,
		const content::Material &material, int id) {
	auto &unique = entityList.emplace_back(
			std::make_unique<Entity>(this, typeName, nullptr, material));
	auto entity = unique.get();

	entity->setPositionRaw(pos);
	entity->last_sent_position = pos;

	if (id == -2) {
		entity->id = entity_id_counter;
		entity_id_counter++;
	} else {
		entity->id = id;
		if (id >= entity_id_counter) {
			entity_id_counter = id + 1;
		}
	}

	assert(entity_lookup.find(entity->id) == entity_lookup.end());
	entity_lookup[entity->id] = entity;

	reinsertEntityToChunk(entity);

	return entity;
}

void World::removeEntity(Entity *entity) {
	LogF(INFO, "[World] Removing entity %d", entity->id);

	entity_lookup.erase(entity_lookup.find(entity->id));
	entity->id = -2;

	// Remove from parent
	if (entity->parent) {
		entity->parent->entities.remove(entity);
		entity->parent = nullptr;
	}

	auto it = std::find_if(entityList.begin(), entityList.end(),
			[&](const auto &x) { return x.get() == entity; });
	entityList.erase(it);
}

void World::reinsertEntityToChunk(Entity *entity) {
	if (entity->parent)
		entity->parent->entities.remove(entity);

	WorldChunk *chunk = getChunk(PosToCPos(entity->getPosition()));
	if (chunk) {
		entity->parent = chunk;
		chunk->entities.push_back(entity);
	} else {
		entity->parent = nullptr;
		Log("World", INFO)
				<< "reinsertEntityToChunk: Chunk does not exist, adding to "
				   "insertion queue";
		entity_insertion_queue.push_back(entity);
	}
}

Entity *World::getEntityById(int id) {
	auto it = entity_lookup.find(id);
	if (it == entity_lookup.end()) {
		return nullptr;
	} else {
		return it->second;
	}
}

void World::getEntities(
		V3f pos, std::vector<Entity *> &entities, size_t giveup_count) {
	assert(entities.empty());

	V3s cpos = PosToCPos(pos.floor());
	WorldChunk *chunk = getChunk(cpos);
	if (chunk) {
		for (auto ent : chunk->entities) {
			float r = ent->properties.radius;
			if (std::fabs(pos.x - ent->getPosition().x) < r &&
					std::fabs(pos.y - ent->getPosition().y) < r) {
				entities.push_back(ent);
				if (giveup_count > 0 && entities.size() >= giveup_count) {
					return;
				}
			}
		}
	}
}

void World::getEntitiesInRange(
		const V3f &pos, std::vector<Entity *> &entities, float range) {
	assert(entities.empty());

	float sqRange = range * range;
	std::vector<WorldChunk *> queue;
	std::unordered_set<V3s> visited;
	V3s originChunkPos = PosToCPos(pos);

	auto originChunk = getChunk(originChunkPos);
	if (originChunk) {
		queue.push_back(originChunk);
	}
	visited.insert(originChunkPos);

	while (!queue.empty()) {
		WorldChunk *chunk = queue[queue.size() - 1];
		queue.pop_back();

		for (auto entity : chunk->entities) {
			if (entity->getPosition().sqDistance(pos) < sqRange) {
				entities.push_back(entity);
			}
		}

		for (const auto &offset : AROUND_2D) {
			V3s newChunkPos = chunk->pos + offset;
			if (visited.find(newChunkPos) == visited.end()) {
				V3f nearPos = CPosToPos(newChunkPos).floating();

				if (pos.x > nearPos.x + CHUNK_SIZE) {
					nearPos.x += CHUNK_SIZE - 1;
				} else if (pos.x > nearPos.x) {
					nearPos.x = pos.x;
				}

				if (pos.y > nearPos.y + CHUNK_SIZE) {
					nearPos.y += CHUNK_SIZE - 1;
				} else if (pos.y > nearPos.y) {
					nearPos.y = pos.y;
				}

				if (nearPos.sqDistance(pos) < sqRange) {
					auto newChunk = getChunk(newChunkPos);
					if (newChunk) {
						visited.insert(newChunkPos);
						queue.push_back(newChunk);
					}
				}
			}
		}
	}
}

Plot *World::createPlot(const std::string &name, const Rect3s &bounds, int id) {
	if (id < 0) {
		id = plot_id_counter;
		plot_id_counter++;
	}

	plots.emplace_back(std::make_unique<Plot>(id, name, bounds));
	return plots.back().get();
}

Plot *World::getPlot(const V3s &pos) {
	for (auto &plot : plots) {
		if (plot->contains(pos)) {
			return plot.get();
		}
	}

	return nullptr;
}

Plot *World::getPlot(int id) {
	for (auto &plot : plots) {
		if (plot->id == id) {
			return plot.get();
		}
	}

	return nullptr;
}

Plot *World::getPlotByWork(u32 workId) {
	for (auto &plot : plots) {
		if (plot->hasWork(workId)) {
			return plot.get();
		}
	}

	return nullptr;
}

void World::getPlotsInRect(std::vector<Plot *> &res, const Rect3s &rect) {
	assert(res.empty());

	for (auto &plot : plots) {
		if (plot->intersects(rect)) {
			res.push_back(plot.get());
		}
	}
}

Work *World::getWork(u32 workId) {
	for (auto &plot : plots) {
		auto work = plot->getWork(workId);
		if (work) {
			return work;
		}
	}

	return nullptr;
}

Work *World::addWork(Plot *plot, std::unique_ptr<Work> &&work) {
	auto ret = plot->addWorkRaw(std::move(work));

	if (ret && bus) {
		assert(ret->id == 0);
		ret->id = work_id_counter;
		work_id_counter++;

		bus->push(WorldEvent::workCreated(plot, ret));
	}

	return ret;
}

bool World::removeWork(u32 workId) {
	for (auto &plot : plots) {
		if (plot->removeWorkRaw(workId)) {
			if (bus) {
				bus->push(WorldEvent::workRemoved(plot.get(), workId));
			}
			return true;
		}
	}

	return false;
}
