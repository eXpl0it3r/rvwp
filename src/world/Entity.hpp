#pragma once

#include "types/types.hpp"
#include "content/ItemDef.hpp"
#include "content/Material.hpp"
#include "content/KeyValueStore.hpp"

#include <log.hpp>
#include <utility>
#include <memory>

namespace world {

struct EntityProperties {
	content::Material material;
	f32 radius = 0.5;
	bool footsteps = true;
};

class World;
class WorldChunk;
class Entity {
protected:
	World *world;
	V3f position;

	/// Clockwise rotation from right, in degrees
	f32 yaw = 0;

public:
	enum class Property { Position, Yaw, HP, Material, Radius, Footsteps };

	static const int PropertyCount = 6;
	static const std::string PLAYER_TYPE_NAME;

	WorldChunk *parent;

	const std::string typeName;
	EntityProperties properties;

	int id = -1;
	bool hasAuthority = true;

	u16 hp = 100;

	std::shared_ptr<content::KeyValueStore> metadata;

	/// Interpolation targets or last sent position
	V3f last_sent_position;

	/// Interpolation target or last sent yaw
	f32 last_sent_yaw = 0;

	bool dirty_properties = false;

	/// Used to check packet order in entity sends
	u32 packet_order_counter = 0;

	Entity(World *world, const std::string &typeName, WorldChunk *parent,
			const content::Material &material = content::Material())
			: world(world), parent(parent), typeName(typeName),
			  metadata(std::make_shared<content::KeyValueStore>()) {
		properties.material = material;
	}

	Entity(const Entity &entity) = delete;

	void interpolate(float dtime);

	inline const V3f &getPosition() const { return position; }

	/// Sets the position. Will re-insert entity if changed chunks
	void setPosition(const V3f &newpos);

	/// Sets the position, won't do anything else
	inline void setPositionRaw(const V3f &newpos) { position = newpos; }

	/// @returns Clockwise rotation from right, in degrees
	inline const float &getYaw() const { return yaw; }

	/// Set yaw
	///
	/// @param Clockwise rotation from right, in degrees
	void setYaw(float v) {
		yaw = v;

		while (yaw < 0) {
			yaw += 360.f;
		}

		while (yaw > 360.f) {
			yaw -= 360.f;
		}
	}

	/// Immediately move an entity to the given position
	/// Checks for collision and reinserts if the chunk changes
	/// @return result
	bool moveTo(const V3f &newpos);

	/// Immediately move an entity by the given amount
	/// Checks for collision and reinserts if the chunk changes
	/// @return result
	inline bool move(float x, float y) {
		return moveTo(position + V3f(x, y, 0));
	}

	/// Try to jump
	///
	/// @param delta move distance, z must be 0
	/// @return
	bool jump(const V3f &delta);

	/// Makes the entity fall if standing on void
	/// @return true if fell
	bool checkForFalling();

	/// Damage the entity
	/// @return points actually removed
	int damage(int points);

	/// Get entity info
	std::string getInfoAsString() const;
};

} // namespace world
