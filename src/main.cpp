#include <log.hpp>
#include <fstream>
#include <string>

#include "network/net.hpp"
#include "server/Server.hpp"
#include "world/DebugUI.hpp"

#ifdef _WIN32
#	include <direct.h>
#	pragma comment(linker, "/SUBSYSTEM:windows /ENTRY:mainCRTStartup")
#else
#	include <unistd.h>
#endif

#ifndef SERVER
#	include "tests/tests.hpp"
#	include "client/ClientLauncher.hpp"
#endif

using namespace network;

namespace {

inline bool FileExists() {
	return std::ifstream("assets/textures/tileset.diffuse.png").good();
}

bool setCHDIR(bool &is_installed) {
	if (FileExists()) {
		is_installed = false;
		return true;
	}

	chdir("../");
	if (FileExists()) {
		is_installed = false;
		return true;
	}

#ifndef _WIN32
	chdir("share/rvwp");
	if (FileExists()) {
		is_installed = true;
		return true;
	}

	chdir("/usr/local/share/rvwp");
	if (FileExists()) {
		is_installed = true;
		return true;
	}

	chdir("/usr/share/rvwp");
	if (FileExists()) {
		is_installed = true;
		return true;
	}
#endif

	return false;
}

int handleMain(int argc, char *argv[]) {
#ifndef SERVER
	if (!TestRunner::RunTests()) {
		LogF(ERROR, "[Main] An auto test failed!");
		return 1;
	}

	std::string cmd;
	if (argc >= 2) {
		cmd = argv[1];
	}

	if (cmd == "tests") {
		return 0;
	} else if (cmd == "server") {
#endif
		LogF(INFO, "[Main] Starting server!");

		loguru::set_thread_name("Server");
		server::ServerSpec spec{};
		server::Server s(spec);
		if (s.start(30100)) {
			s.run();
		}
		return 0;
#ifndef SERVER
	} else {
		loguru::set_thread_name("Client");

		client::ClientLauncher launcher;
		return launcher.launch() ? 0 : 1;
	}
#endif
}

} // namespace

int main(int argc, char *argv[]) {
	std::ios::sync_with_stdio(false);

	loguru::g_colorlogtostderr = true;
	loguru::g_stderr_verbosity = loguru::Verbosity_MAX;
	loguru::g_preamble_date = false;
	loguru::g_preamble_time = false;
	loguru::g_preamble_uptime = false;
	loguru::g_preamble_thread = true;
	loguru::g_preamble_file = false;
	loguru::g_preamble_verbose = false;
	loguru::g_preamble_pipe = true;
	loguru::init(argc, argv);
	loguru::set_thread_name("Main");
#ifdef NDEBUG
	loguru::add_file("rvwplog.txt", loguru::Append, loguru::Verbosity_INFO);
#endif

	bool installed;
	if (!setCHDIR(installed)) {
		LogF(ERROR, "[Main] Unable to find game's resources!");
		return 1;
	}

	if (!InitializeSockets()) {
		LogF(ERROR, "[Main] Failed to initialize sockets");
		return 1;
	}

	debug::DebugUI::Init();

	int retcode = handleMain(argc, argv);

	ShutdownSockets();
	debug::DebugUI::CleanUp();

	return retcode;
}

/*! @mainpage Welcome to RVWP
 *
 * @section intro_sec Introduction
 *
 * Procedurally generated city for a player to explore.
 * See docs/ for more info (ie: design docs)
 *
 * @section run Running
 *
 * To compile: `cmake . && make -j3` <br />
 * Start server using `./bin/rvwp server` or `./bin/server`.
 * Open the client using `./bin/rvwp`.
 * Run tests using `./bin/rvwp tests` or `./bin/server tests`.
 *
 */
