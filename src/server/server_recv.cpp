#include "Server.hpp"

#include <log.hpp>
#include <sstream>

#include "world/Raycast.hpp"
#include "content/InventoryCommand.hpp"
#include "build_info.hpp"

using namespace server;
using namespace world;
using namespace network;

#define B(func) std::bind(&Server::func, this, _1)

void Server::initPacketHandlers() {
	using namespace std::placeholders;

	// clang-format off

	network::PacketRoutingTable table = {
			nullptr_PKT,                     // TONONE_nullptr

			B(handlePlayerConnect),          // TOSERVER_HELLO
			nullptr_PKT,                     // TOCLIENT_HELLO
			nullptr_PKT,                     // TOCLIENT_READY
			B(handlePlayerDisconnect),       // TOANY_DISC
			nullptr_PKT,
			nullptr_PKT,

			nullptr_PKT,                     // TOCLIENT_PROFILE
			nullptr_PKT,                     // TOCLIENT_PLAYER_INVENTORY
			B(handlePlayerInventoryCommand),
			B(handleChatMessage),

			nullptr_PKT,                     // TOCLIENT_SET_TIME
			nullptr_PKT,                     // TOCLIENT_WORLD_BLOCK
			nullptr_PKT,                     // TOCLIENT_WORLD_NEW_ENTITY
			nullptr_PKT,                     // TOCLIENT_WORLD_UPDATE_ENTITY
			nullptr_PKT,                     // TOCLIENT_WORLD_DELETE_ENTITY
			nullptr_PKT,                     // TOCLIENT_WORLD_GOTO
			B(handlePlayerMove),
			B(handleTileInteract),
			B(handlePunchEntity),
			B(handleSetTile),
			nullptr_PKT,                     // TOCLIENT_WORLD_PLOT
			B(handleWorldNewWork),           // TOANY_WORLD_NEW_WORK
			B(handleWorldRemoveWork),        // TOANY_WORLD_REMOVE_WORK
			B(handleFireWeapon),             // TOSERVER_FIRE_WEAPON
			nullptr_PKT,                     // TOCLIENT_SPAWN_PARTICLE
	};

	// clang-format on

	socket.setHandlers(table);
}

void Server::handlePlayerConnect(Packet &pkt) {
	Address sender = pkt.getOther();
	size_t size = pkt.size();

	// Read packet
	if (size < 4) {
		Log("Server", ERROR)
				<< "Incorrect size for TOSERVER_HELLO packet, was " << size
				<< " bytes should be 9 or more. Packet ignored.";
		return;
	}

	u16 protocol_v;
	std::string username;
	std::string password;

	pkt >> protocol_v;
	pkt >> username;
	pkt >> password;
	assert(pkt.end());

	Log("Server", INFO) << "Client connected (client protocol_v=" << protocol_v
						<< ")";

	// Reject peer if
	if (findPeerByName(username)) {
		Log("Server", ERROR) << "failed to connect player, already connected.";
		sendDisconnectAndRemovePeer(
				sender, "You're already logged in, try again later.");
		return;
	}

	// Create peer
	peers[sender] = std::make_unique<Peer>(username, sender);
	auto client = peers[sender].get();

	// send MOTD
	{
		std::ostringstream os;
		os << "Welcome to the test server. ";
		os << "Version: " << BuildInfo::VERSION_GIT_SHA1;

		std::string motd = os.str();
		const size_t size = 2 + 2 + motd.size();
		Packet pkt(sender, TOCLIENT_HELLO, size);
		pkt << (u16)PROTOCOL_VERSION;
		pkt << motd;
		socket.send(pkt);
	}

	// Get Player profile
	ServerPlayer *player = getPlayerInfo(username, true);
	assert(client->player_info == player);

	// Send stuff
	sendDefinitions(client);
	sendPlayerProfile(client);
	sendPlayerInventory(client);

	// Emerge
	if (!emergePlayer(client, player->last_position)) {
		Log("Server", ERROR) << "Failed to emerge player!";
		return;
	}

	// Ready
	client->loaded = true;
	{
		Packet pkt(sender, TOCLIENT_READY, 0);
		socket.send(pkt);
	}

	// send chunks
	sendChunkToPlayer(client, world_ctr->getChunk(V3s(0, 0, 0)));
	sendChunkToPlayer(client, world_ctr->getChunk(V3s(1, 0, 0)));
	sendChunkToPlayer(client, world_ctr->getChunk(V3s(0, 0, 1)));

	sendGameTimeToPlayer(client, time);
}

void Server::handlePlayerDisconnect(Packet &pkt) {
	auto it = peers.find(pkt.getOther());
	if (it != peers.end()) {
		std::unique_ptr<Peer> client = std::move(it->second);
		peers.erase(it);

		leavePlayer(client.get());

		Log("Server", INFO) << "Player " << client->name << " disconnected. ";
		printPlayers();
	} else {
		Log("Server", ERROR)
				<< "Invalid disconnect request. (Peer doesn't exist.)";
	}
}

void Server::handlePlayerMove(Packet &pkt) {
	Peer *peer = getPeer(pkt.getOther());
	if (pkt.size() != 3 * 4 + 4 + 4 + 1 + 3 * 4) {
		Log("Server", ERROR)
				<< "Incorrect size for TOSERVER_PLAYER_MOVE packet, was "
				<< pkt.size() << " bytes, expected 21. Packet ignored.";
		return;
	} else if (!peer) {
		Log("Server", ERROR)
				<< "Invalid PLAYER_MOVE request. (Peer doesn't exist.)";
		return;
	} else if (!peer->entity) {
		Log("Server", ERROR) << "Failed to perform PLAYER_MOVE request. "
							 << "(Peer doesn't have an entity)";
		return;
	} else if (peer->entity->hasAuthority) {
		LogF(ERROR,
				"[Server] Failed to perform PLAYER_MOVE request. (Peer doesn't "
				"have authority over their entity)");
		return;
	}

	V3f pos, camera_pos;
	f32 yaw;
	u32 counter;
	u8 flags;
	pkt >> pos;
	pkt >> yaw;
	pkt >> counter;
	pkt >> flags;
	pkt >> camera_pos;
	assert(pkt.end());

	Entity *entity = peer->entity;
	if (counter <= entity->packet_order_counter) {
		Log("Server", ERROR)
				<< "Current EntLSCount (" << entity->packet_order_counter
				<< ") is later than proposed (" << counter
				<< "). Packet ignored.";
		return;
	}

	const float moveSqDistance = entity->getPosition().sqDistance(pos);
	if (moveSqDistance > 8.f * 0.6f) {
		LogF(ERROR,
				"[Server] Failed to perform PLAYER_MOVE request. (Moved too "
				"far, %f)",
				moveSqDistance);
		sendPlayerPosition(peer);
		return;
	}

	entity->setYaw(yaw);
	entity->packet_order_counter = counter;

	if (peer->player_info) {
		peer->player_info->last_position = pos;
	}

	entity->setPosition(pos);
}

void Server::handlePlayerInventoryCommand(Packet &pkt) {
	Peer *peer = getPeer(pkt.getOther());
	if (!peer) {
		Log("Server", ERROR)
				<< "Invalid TOSERVER_PLAYER_INVENTORY_COMMAND request. "
				   "(Peer doesn't exist.)";
		return;
	} else if (!peer->player_info) {
		Log("Server", ERROR)
				<< "Invalid TOSERVER_PLAYER_INVENTORY_COMMAND request. "
				   "(Player info doesn't exist.)";
		return;
	}

	u8 utype;
	pkt >> utype;

	auto type = (content::InventoryCommand::Type)utype;

	std::string from_inv, from_list;
	u16 from_idx;
	pkt >> from_inv;
	pkt >> from_list;
	pkt >> from_idx;

	std::string to_inv, to_list;
	u16 to_idx;
	pkt >> to_inv;
	pkt >> to_list;
	pkt >> to_idx;

	u16 count;
	pkt >> count;

	assert(pkt.end());

	content::StackLocation from = {{from_inv}, from_list, from_idx};
	content::StackLocation to = {{to_inv}, to_list, to_idx};

	content::InventoryCommand command(type, from, to, count);

	content::Inventory *inv = &peer->player_info->inventory;
	content::InventoryProvider provider = {
			[&](auto inventoryName) -> content::Inventory * {
				if (inventoryName == inv->getLocation().name) {
					return inv;
				} else {
					return nullptr;
				}
			}};

	if (!command.perform(provider)) {
		LogF(ERROR,
				"[Server] Invalid TOSERVER_PLAYER_INVENTORY_COMMAND request.");
		sendPlayerInventory(peer);
		return;
	}

	LogF(INFO, "[Server] Performed inventory command successfully.");
}

void Server::handleChatMessage(Packet &pkt) {
	Peer *client = getPeer(pkt.getOther());
	if (!client) {
		Log("Server", ERROR)
				<< "Invalid CHAT_MESSAGE request. (Peer doesn't exist.)";
		return;
	}

	std::string message;
	pkt >> message;

	if (message.find_first_of("\n\r") != std::wstring::npos) {
		sendChatMessageToPlayer(client, "Multiple lines are not supported");
		return;
	}

	Log("Server", INFO) << message;

	scripting_ctr.onChatMessage(client->name, message);
}

void Server::handleSetTile(Packet &pkt) {
	Peer *client = getPeer(pkt.getOther());
	if (!client) {
		Log("Server", ERROR)
				<< "Invalid WORLD_SET_TILE request. (Peer doesn't exist.)";
		return;
	} else if (!client->player_info) {
		Log("Server", ERROR) << "Invalid WORLD_SET_TILE request. (Player info "
								"doesn't exist.)";
		return;
	}

	// Read packet
	V3s pos;
	u32 cid;
	u8 hotbar_idx;
	u16 hp;
	u32 metadata_len;
	pkt >> pos;
	pkt >> cid;
	pkt >> hotbar_idx;
	pkt >> hp;
	pkt >> metadata_len;
	assert(pkt.end());

	if (!doSetTileNoRollback(client, pos, cid, hotbar_idx, hp)) {
		sendPlayerInventory(client);

		WorldChunk *chunk = world_ctr->getWorld()->getChunk(PosToCPos(pos));
		if (chunk) {
			sendChunkToPlayer(client, chunk);
		}
	}
}

bool Server::doSetTileNoRollback(
		Peer *client, const V3s &pos, u32 cid, u8 hotbar_id, u16 hp) {

	const content::TileDef *tdef =
			(cid == 0) ? nullptr : definitionManager.getTileDef(cid);
	if (cid != 0 && !tdef) {
		LogF(ERROR, "[Server] Invalid WORLD_SET_TILE request. (unknown tile)");
		return false;
	}

	if (cid != 0) {
		// Check stack
		content::Inventory &inv = client->player_info->inventory;

		content::InventoryList *list = inv.get("hotbar").value_or(nullptr);
		assert(list);

		content::ItemStack &stack = list->get(hotbar_id);
		if (stack.name != tdef->name || stack.empty()) {
			LogF(ERROR,
					"[Server] Invalid WORLD_SET_TILE request. (unable to "
					"place empty stack)");
			return false;
		}

		// Consume item
		stack.count--;
	}

	// TODO: Validate position

	WorldTile tile{};
	content::ItemType type = content::ItemType::Tile;
	if (cid > 0) {
		tile.cid = cid;
		type = tdef->getType();
	} else {
		assert(tile.cid == 0);
	}

	ChunkLayer layer = typeToChunkLayer(type);

	WorldChunk *chunk = nullptr;
	if (type == content::ItemType::Tile) {
		chunk = world_ctr->getWorld()->getChunk(PosToCPos(pos));
		if (!chunk) {
			Log("Server", ERROR) << "Failed to set tile at " << pos.x << ", "
								 << pos.y << ", " << pos.z;
			return false;
		}
	}

	if (!world_ctr->set(pos, layer, tile)) {
		Log("Server", ERROR) << "Failed to set tile at " << pos.x << ", "
							 << pos.y << ", " << pos.z;
		return false;
	}

	Log("Server", INFO) << "Set tile at " << pos.x << ", " << pos.y << ", "
						<< pos.z;

	if (type == content::ItemType::Tile) {
		chunk->dirty = true;
	}

	sendSetTileToAllPlayers(pos, layer, client);
	return true;
}

void Server::handleTileInteract(Packet &pkt) {
	Peer *client = getPeer(pkt.getOther());
	if (!client) {
		Log("Server", ERROR)
				<< "Invalid TILE_INTERACT request. (Peer doesn't exist.)";
		return;
	}

	if (!client->entity) {
		Log("Server", ERROR) << "Invalid TILE_INTERACT request. (Peer's entity "
								"doesn't exist.)";
	}

	// Read packet
	V3s pos;
	pkt >> pos;
	assert(pkt.end());

	world_ctr->tileInteract(client, pos);
}

void Server::handlePunchEntity(Packet &pkt) {
	Peer *peer = getPeer(pkt.getOther());
	if (!peer) {
		Log("Server", ERROR)
				<< "Invalid PUNCH_ENTITY request. (Peer doesn't exist.)";
		return;
	}

	// Read packet
	u32 id;
	u32 update_idx;
	u8 hotbar_idx;
	u16 damage;
	u16 expected_after_hp;
	pkt >> id >> update_idx >> hotbar_idx >> damage >> expected_after_hp;
	assert(pkt.end());

	Entity *ent = world_ctr->getWorld()->getEntityById(id);
	if (!ent) {
		sendRemoveEntityToPlayer(peer, id);
		return;
	} else if (ent->hp == 0) {
		return;
	}

	world_ctr->punchEntity(ent, peer->entity, 10);
}

void Server::handleWorldNewWork(Packet &pkt) {
	Peer *client = getPeer(pkt.getOther());
	if (!client) {
		LogF(ERROR,
				"[Server] Invalid PUNCH_ENTITY request. (Peer doesn't exist.)");
		return;
	}

	u32 clientId;
	u32 plotId;
	std::string type;
	Rect3s bounds;
	content::Material material;
	std::string itemName;
	u8 ulayer;

	pkt >> clientId >> plotId >> type >> bounds;
	material.deserialise(pkt);
	pkt >> itemName >> ulayer;

	world::ChunkLayer layer = static_cast<world::ChunkLayer>(ulayer);

	auto work = getWorld()->addWork(plotId,
			std::make_unique<Work>(type, bounds, material, itemName, layer));
	if (!work) {
		LogF(ERROR, "Failed to add work from client");
		sendWorkRemovedToPlayer(client, 0);
		return;
	}

	client->workClientToServer[clientId] = work->id;
	client->workServerToClient[work->id] = clientId;
}

void Server::handleWorldRemoveWork(Packet &pkt) {
	Peer *peer = getPeer(pkt.getOther());
	if (!peer) {
		LogF(ERROR,
				"[Server] Invalid PUNCH_ENTITY request. (Peer doesn't exist.)");
		return;
	}

	u32 serverId;
	u32 clientId;

	pkt >> serverId >> clientId;

	if (serverId == 0 && clientId != 0) {
		serverId = peer->workClientToServer[clientId];
	}

	if (serverId == 0) {
		LogF(ERROR,
				"[Server] Failed to remove work: Unable to find serverId from "
				"clientId=%d",
				clientId);
		return;
	}

	if (!getWorld()->removeWork(serverId)) {
		LogF(ERROR, "[Server] Unable to find work to remove");
		return;
	}

	peer->workClientToServer.erase(clientId);
	peer->workServerToClient.erase(serverId);
}

void Server::handleFireWeapon(Packet &pkt) {
	Peer *peer = getPeer(pkt.getOther());
	if (!peer) {
		LogF(ERROR,
				"[Server] Invalid FIRE_WEAPON request. (Peer doesn't exist.)");
		return;
	}

	Entity *entity = peer->entity;
	if (!entity) {
		LogF(ERROR,
				"[Server] Invalid FIRE_WEAPON request. (Peer doesn't have an "
				"entity.)");
		return;
	}

	V3f position;
	f32 yaw;
	u8 hotbarIdx;
	pkt >> position >> yaw >> hotbarIdx;

	LogF(ERROR, "[Server] Received fire weapon %f,%f,%d yaw %f hotbar %d",
			position.x, position.y, position.z, yaw, hotbarIdx);

	if (position.sqDistance(entity->getPosition()) > 1.f) {
		LogF(ERROR,
				"[Server] Invalid FIRE_WEAPON request. (Gun fire too far.)");
		return;
	}

	auto stack = peer->player_info->inventory.get("hotbar", hotbarIdx);
	auto def = definitionManager.getItemDef(stack.name);
	if (stack.empty() || !def) {
		LogF(ERROR,
				"[Server] Invalid FIRE_WEAPON request. (Specified hotbar slot "
				"is empty)");
		return;
	}

	if (!def->weapon_spec.has_value()) {
		LogF(ERROR,
				"[Server] Invalid FIRE_WEAPON request. (Hotbar item isn't a "
				"weapon)");
		return;
	}

	auto weaponSpec = def->weapon_spec.value();
	if (weaponSpec.type != content::WeaponSpec::Type::Gun) {
		LogF(ERROR,
				"[Server] Invalid FIRE_WEAPON request. (Hotbar item isn't a "
				"gun)");
		return;
	}

	yaw = yaw * PI / 180.f - PI / 2.f;
	V3f direction(std::cos(yaw), std::sin(yaw), 0);
	sendSpawnParticleToAllPlayers(
			position, direction * 50.f, content::Material("fire.png"), peer);

	Raycast raycast{getWorld(), entity};
	SelectionSpec selection =
			raycast.trace(position, direction, weaponSpec.range);
	if (selection.type == SelectionSpec::SST_Entity) {
		world_ctr->punchEntity(selection.entity, entity, weaponSpec.damage);
	}
}
