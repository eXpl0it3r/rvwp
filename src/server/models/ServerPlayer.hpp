#pragma once
#include "content/Player.hpp"
#include "../../types/types.hpp"

namespace server {

class ServerPlayer : public content::Player {
public:
	explicit ServerPlayer(const std::string &username) : Player(username) {}

	std::string hash;
	std::string salt;

	// TODO: don't store last_position here, store it in the database
	V3f last_position;

	// TODO: last login time, time online, etc
};

} // namespace server
