#include "world/LightCalculator.hpp"
#include "WorldController.hpp"
#include "server/Server.hpp"

using namespace server;
using namespace world;

void WorldController::update(float dtime) {
	worldEventBus.flush();

	for (auto aa : world.getEntityList()) {
		if (aa->hasAuthority) {
			scriptingController->updateEntity(aa, dtime);
		}
	}

	// Run tile timers
	std::vector<TileTimer> timers;
	world.popExpiredTimers(
			timers, server->getGameTime().realTimeSinceBeginning);
	if (!timers.empty()) {
		scriptingController->runExpiredTimers(timers);
	}

	// Send dirty chunks
	std::vector<WorldChunk *> chunks;
	world.getDirtyChunks(chunks);
	for (auto chunk : chunks) {
		server->sendDirtyChunkToWatchers(chunk);
	}
}

void WorldController::onWorldEvent(const WorldEvent &event) {
	switch (event.type) {
	case WorldEvent::WORK_CREATED: {
		auto work = world.getWork(event.workId);
		SanityCheck(work);
		server->sendWorkCreatedToWatchers(event.plotId, work);
		break;
	}
	case WorldEvent::WORK_REMOVED:
		server->sendWorkRemovedToWatchers(event.plotId, event.workId);
		break;
	case WorldEvent::SET_TILE:
		onSetTile(event);
		break;
	default:
		break;
	}
}

void WorldController::onSetTile(const world::WorldEvent &event) {
	LightCalculator light(definitionManager, &world);

	auto pos = event.pos;
	auto chunk = event.chunk;
	auto tile = world.get(pos, event.layer);
	auto tileDef = tile ? definitionManager->getTileDef(tile->cid) : nullptr;

	switch (event.layer) {
	case ChunkLayer::Floor: {
		auto chunkBelow = world.getChunk(chunk->pos + V3s(0, 0, -1));
		light.removeSunlight(chunkBelow, pos + V3s(0, 0, -1));
		break;
	}

	case ChunkLayer::Tile:
		if (tileDef && tileDef->lightSource > 0) {
			SetArtificialRed(chunk->lightLevels[chunk->getIndex(pos)],
					tileDef->lightSource);
			light.spreadArtificial(chunk, pos);
		} else {
			light.removeArtificial(chunk, pos);
		}

		light.removeSunlight(chunk, pos);
		break;
	}
}

void WorldController::emerge(const V3s &cpos) {
	auto c = world.getOrCreateChunk(cpos);
	mapgen->generate(c);

	std::vector<WorldChunk *> chunks;
	chunks.reserve(26);
	world.getNeighbouringChunks3d(chunks, cpos);
	for (const auto &chunk : chunks) {
		if (!chunk->isActive()) {
			checkChunkForActivation(chunk);
		}
	}

	{
		auto chunk = world.getChunk(cpos + V3s(0, 0, 1));
		if (chunk) {
			checkChunkForActivation(chunk);
		}
	}
}

bool WorldController::checkChunkForActivation(WorldChunk *chunk) {
	std::vector<WorldChunk *> chunks;
	chunks.reserve(8);
	world.getNeighbouringChunks2d(chunks, chunk->pos, CHUNK_LOADED);
	if (chunks.size() != 8) {
		return false;
	}

	if (!chunk->isFloorSolid()) {
		if (world.getChunkState(chunk->pos + V3s(0, 0, -1)) < CHUNK_LOADED) {
			return false;
		}
	}

	world.activateChunk(chunk);

	LightCalculator calc{definitionManager, &world};
	calc.spreadEdges(chunk);

	return true;
}

bool WorldController::emergePlayer(Peer *client, V3f position) {
	Log("WorldCTR", INFO) << "Emerging player " << client->name << "...";

	// Emerge Player
	auto entity = world.createEntity(position, Entity::PLAYER_TYPE_NAME,
			content::Material("player.png"));
	entity->hasAuthority = false;
	client->entity = entity;

	return true;
}

bool WorldController::tileInteract(Peer *client, const V3s &pos) {
	assert(client->entity);
	if (!client->entity) {
		LogF(ERROR,
				"[WorldCTR] Could not handle tile interaction, as the peer has "
				"no entity");
		return false;
	}

	return scriptingController->tileInteract(client->entity, pos);
}

int WorldController::punchEntity(
		world::Entity *target, world::Entity *puncher, int damage) {
	int totalDamage = target->damage(damage);

	if (target->hp == 0) {
		Peer *dead_player = server->findPeerByEntity(target);
		if (dead_player) {
			target->setPosition(server->getRespawnPosition(dead_player));
			target->hp = 100;
			server->sendPlayerPosition(dead_player);
		} else {
			world.removeEntity(target);
			server->sendRemoveEntityToAllPlayers(target->id, nullptr);
		}
	}

	return totalDamage;
}
