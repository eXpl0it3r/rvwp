#pragma once
#include "../../types/types.hpp"
#include "types/Controller.hpp"
#include "world/World.hpp"
#include "world/Entity.hpp"
#include "../../content/content.hpp"
#include "content/Inventory.hpp"
#include "../../mapgen/Mapgen.hpp"
#include "server/models/Peer.hpp"
#include "ScriptingController.hpp"
#include "events/EventListener.hpp"

namespace server {

class Server;
class WorldController : public Controller {
	Server *server;

	EventBus<world::WorldEvent> worldEventBus;
	const EventListener<world::WorldEvent> listener;
	world::World world;

	content::DefinitionManager *definitionManager;
	ScriptingController *scriptingController;
	std::unique_ptr<MG::Mapgen> mapgen;

public:
	WorldController(Server *server, content::DefinitionManager *def,
			server::ScriptingController *scriptingController,
			std::unique_ptr<MG::Mapgen> &&mapgen)
			: server(server),
			  listener(&worldEventBus, [&](const auto &x) { onWorldEvent(x); }),
			  world(def, &worldEventBus), definitionManager(def),
			  scriptingController(scriptingController),
			  mapgen(std::move(mapgen)) {}

	void update(float dtime) override;

	void onWorldEvent(const world::WorldEvent &event);
	void onSetTile(const world::WorldEvent &event);

	std::vector<world::Entity *> getEntityList() {
		return world.getEntityList();
	}
	void emerge(const V3s &cpos);
	bool checkChunkForActivation(world::WorldChunk *chunk);
	bool emergePlayer(Peer *client, V3f position);
	bool tileInteract(Peer *client, const V3s &pos);

	/// Entity punches entity
	///
	/// @param punched The entity that is punched
	/// @param puncher The puncher
	/// @param damage Damage amount
	/// @return
	int punchEntity(world::Entity *punched, world::Entity *puncher, int damage);

	[[nodiscard]] inline world::WorldTile *set(
			const V3s &pos, world::ChunkLayer layer, content_id cid) {
		return world.set(pos, layer, cid);
	}

	[[nodiscard]] inline world::WorldTile *set(const V3s &pos,
			world::ChunkLayer layer, const world::WorldTile &tile) {
		return world.set(pos, layer, tile);
	}

	inline world::WorldTile *get(const V3s &pos, world::ChunkLayer layer) {
		return world.get(pos, layer);
	}

	inline world::WorldChunk *getChunk(const V3s &pos) {
		return world.getChunk(pos);
	}

	inline void removeEntity(world::Entity *e) { world.removeEntity(e); }

	inline world::World *getWorld() { return &world; }
};

} // namespace server
