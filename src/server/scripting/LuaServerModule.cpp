#include "LuaServerModule.hpp"

using namespace scripting;

#define add_func(name)                                                         \
	core.set_function(#name, &LuaServerModule::l_##name, this)

LuaServerModule::LuaServerModule(
		server::ScriptingController *scriptingController, sol::state &lua,
		sol::table &core, std::function<void(const sol::error &)> handler)
		: scriptingController(scriptingController), lua(lua), core(core),
		  handleError(handler) {
	PlayerRef::registerToNamespace(lua);

	add_func(chat_send_player);
	add_func(chat_send_all);
	add_func(send_chunks_to_player);
	add_func(get_time);
	add_func(set_time_of_day);
	add_func(get_player);
}

void LuaServerModule::onChatMessage(
		const std::string &name, const std::string &message) {
	sol::function f = core["on_chat_message"];
	auto ret = f(name, message);
	if (!ret.valid()) {
		handleError(ret);
	}
}

bool LuaServerModule::l_chat_send_player(
		const std::string &player, const std::string &message) {
	return scriptingController->sendChatMessage(player, message);
}

void LuaServerModule::l_chat_send_all(const std::string &message) {
	scriptingController->sendChatMessage(message);
}

bool LuaServerModule::l_send_chunks_to_player(
		const std::string &name, int width, int max_emerge) {
	return scriptingController->loadChunksAroundPlayer(name, width, max_emerge);
}

sol::table LuaServerModule::l_get_time(sol::this_state state) {
	const world::GameTime &time = scriptingController->getGameTime();

	sol::table table(state.L, sol::create);
	table["time_of_day"] = time.timeOfDay;
	table["hours"] = time.getHours();
	table["minutes"] = time.getMinutes();
	table["clock"] = time.getString();
	table["daylight"] = time.getDaylight();
	table["time_since_beginning"] = time.realTimeSinceBeginning;
	table["speed"] = time.speed;
	return table;
}

void LuaServerModule::l_set_time_of_day(
		float time, sol::optional<float> speed) {
	scriptingController->setTimeOfDay(time, speed.value_or(-1.f));
}

sol::optional<PlayerRef> LuaServerModule::l_get_player(
		const std::string &username) {
	auto player = scriptingController->getPlayer(username);
	if (!player) {
		return {};
	}

	return PlayerRef(player, scriptingController);
}
