#pragma once

#include "server/controllers/ScriptingController.hpp"
#include "scripting/Lua.hpp"
#include "PlayerRef.hpp"

namespace scripting {

class LuaServerModule {
	server::ScriptingController *scriptingController;
	sol::state &lua;
	sol::table &core;
	std::function<void(const sol::error &)> handleError;

public:
	LuaServerModule(server::ScriptingController *scriptingController,
			sol::state &lua, sol::table &core,
			std::function<void(const sol::error &)> handler);

	void onChatMessage(const std::string &name, const std::string &message);

private:
	bool l_chat_send_player(
			const std::string &playerName, const std::string &message);
	void l_chat_send_all(const std::string &message);
	bool l_send_chunks_to_player(
			const std::string &name, int width, int max_emerge);
	sol::table l_get_time(sol::this_state state);
	void l_set_time_of_day(float time, sol::optional<float> speed);
	sol::optional<PlayerRef> l_get_player(const std::string &username);
};

} // namespace scripting
