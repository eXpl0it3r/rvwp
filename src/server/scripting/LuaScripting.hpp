#pragma once

#include "../controllers/ScriptingController.hpp"

#include "ScriptingInterface.hpp"
#include "types/types.hpp"
#include "scripting/Lua.hpp"
#include "scripting/ModConfig.hpp"
#include "scripting/KeyValueStoreRef.hpp"
#include "DebugUIRef.hpp"
#include "LuaServerModule.hpp"
#include "LuaWorldModule.hpp"
#include "scripting/LuaDebugger.hpp"
#include "scripting/LuaSecurity.hpp"

#include <string>

extern "C" {
#include <lua.h>
#include <lauxlib.h>
#include <lualib.h>
}

namespace scripting {

class LuaScripting : public ScriptingInterface {
	sol::state lua;
	sol::table core;

	server::ScriptingController *scriptingController;
	std::unordered_map<std::string, ModConfig> mods_by_name;
	bool isDead = true;

	std::unique_ptr<LuaDebugger> debugger;
	std::unique_ptr<LuaSecurity> security;
	std::unique_ptr<LuaServerModule> serverModule;
	std::unique_ptr<LuaWorldModule> worldModule;

	void handleError(const sol::error &error);

public:
	explicit LuaScripting(server::ScriptingController *scriptingController)
			: scriptingController(scriptingController) {}

	~LuaScripting() override = default;

	void init(Domain domain) override;
	bool getIsDead() override { return isDead; }

	bool initMod(const ModConfig &mod) override;

	void onChatMessage(
			const std::string &name, const std::string &message) override {
		serverModule->onChatMessage(name, message);
	}

	void runExpiredTimers(
			const std::vector<world::TileTimer> &timers) override {
		worldModule->runExpiredTimers(timers);
	}

	bool createEntity(world::Entity *entity) override {
		return worldModule->createEntity(entity);
	}

	bool updateEntity(world::Entity *entity, float dtime) override {
		return worldModule->updateEntity(entity, dtime);
	}

	bool tileInteract(world::Entity *entity, const V3s &pos) override {
		return worldModule->tileInteract(entity, pos);
	}

private:
	sol::table l_get_mods(sol::this_state state);
	void l_register_item(const sol::table &table);
	void l_register_tool(const sol::table &table);
};

} // namespace scripting
