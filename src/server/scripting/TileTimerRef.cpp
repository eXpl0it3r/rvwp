#include <utility>

#include "TileTimerRef.hpp"

using namespace scripting;
using namespace world;

void TileTimerRef::registerToNamespace(sol::state &lua) {
	lua.new_usertype<TileTimerRef>(
			"TileTimerRef", "start", &TileTimerRef::start);
}

bool TileTimerRef::start(float delay) {
	float gametime = scriptingCtr->getGameTime().realTimeSinceBeginning;

	TileTimer timer = {position, layer, event, gametime + delay};

	return scriptingCtr->pushTileTimer(timer);
}
