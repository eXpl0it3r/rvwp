#include "Server.hpp"
#include "../mapgen/NoiseMG.hpp"
#include "world/DebugUI.hpp"
#include <log.hpp>
#include <sstream>
#include <thread>
#include <chrono>

using namespace server;
using namespace world;
using namespace network;
using namespace std::literals;

Server::Server(const ServerSpec &spec) : spec(spec), scripting_ctr(this) {
	definitionManager.unlock();
	definitionManager.defineVoid();

	scripting_ctr.recreate();

	registerController(&scripting_ctr);

	world_ctr = std::make_unique<WorldController>(this, &definitionManager,
			&scripting_ctr, std::make_unique<MG::NoiseMG>(&definitionManager));

	registerController(world_ctr.get());

	requestEmerge(V3s(0, 0, 0));
	requestEmerge(V3s(1, 0, 0));
	requestEmerge(V3s(0, 0, 1));

	getWorld()->createPlot("Base", {V3s(0, -5, 0), V3s(20, 20, 3)});

	definitionManager.lock();

	initPacketHandlers();
}

Server::~Server() {
	Log("Server", INFO) << "Server shutting down...";

	if (socket.isOpen()) {
		for (auto &it : peers) {
			Peer *peer = it.second.get();
			sendDisconnectAndRemovePeer(
					peer->address, "Server is shutting down...");
		}
	}
}

bool Server::start(unsigned short port) {
	if (!socket.startServer(port)) {
		Log("Server", ERROR) << "Failed to open socket.";
		return false;
	}

	return true;
}

bool Server::run() {
	using namespace std::chrono;
	Log("Server", INFO) << "Server listening on port " << socket.getPort();

	int dtimeSum = 0;
	int dtimeCount = 0;

	float dtime = 0.1f;
	while (socket.isOpen()) {
		high_resolution_clock::time_point start = high_resolution_clock::now();

		update(dtime);

		high_resolution_clock::time_point end = high_resolution_clock::now();

		auto duration = std::chrono::duration_cast<std::chrono::microseconds>(
				end - start);
		auto us = duration.count();
		float fps = 1000000.f / (float)us;
		float percBudget = 100 * (float)us / (1000000.f / 10.f);
		auto level = loguru::Verbosity_OFF;
		if (percBudget > 95.f) {
			level = loguru::Verbosity_ERROR;
		} else if (percBudget > 30.f) {
			level = loguru::Verbosity_WARNING;
		}

		if (level != loguru::Verbosity_OFF) {
			VLOG_F(level,
					"[Server] Server update took %ld us, %.0f fps, %.1f%% "
					"budget",
					us, fps, percBudget);
		}

		dtimeSum += us;
		dtimeCount++;

		if (dtimeCount > 50) {
			float avgUs = (float)dtimeSum / (float)dtimeCount;
			float avgFPS = 1000000.f / avgUs;
			float avgPercBudget = 100.f * avgUs / (1000000.f / 10.f);
			LogF(INFO,
					"[Server] Last 5 steps average: update took %.0f us, %.0f "
					"fps, %.1f%% budget",
					avgUs, avgFPS, avgPercBudget);

			dtimeSum = 0;
			dtimeCount = 0;
		}

		std::this_thread::sleep_for(100ms - duration);
	}
	return true;
}

void Server::close() {
	socket.close();
}

bool Server::update(float dtime) {
	socket.update();

	time.update(dtime);

	// Update controllers
	for (auto ctr : controllers) {
		ctr->update_if_enabled(dtime);
	}

	detectAndSendEntityChanges();

	for (const auto &pair : peers) {
		Peer *peer = pair.second.get();
		if (!peer->entity) {
			continue;
		}

		loadChunksAroundPlayer(peer);

		auto rect =
				Rect3s::fromCenter(peer->entity->getPosition().floor(), 16 * 3);
		std::vector<Plot *> plots;
		getWorld()->getPlotsInRect(plots, rect);
		for (const auto &plot : plots) {
			if (!peer->hasPlot(plot)) {
				sendPlotToPlayer(peer, plot);
			}
		}
	}

	debug::DebugUI().display();

	return true;
}

void Server::loadChunksAroundPlayer(
		Peer *peer, const int width, const int max_emerge) {
	if (!peer->entity) {
		return;
	}

	int chunks_emerged = 0;

	const V3f pos = peer->entity->getPosition();
	V3s center_chunk_pos = PosToCPos(pos);
	const int HW = width / 2;

	V2s rel;
	V2s delta = {0, -1};
	for (int i = 0; i < width * width; i++) {
		if (rel.x > -HW && rel.x <= HW && rel.y > -HW && rel.y < HW) {
			bool chunk_is_solid = false;
			for (int rz = 0; rz < 4; rz++) {
				V3s chunk_pos = center_chunk_pos + V3s(rel.x, rel.y, -rz);
				WorldChunk *chunk = nullptr;
				if (loadChunkForPlayer(peer, chunk_pos, &chunk)) {
					chunks_emerged++;
					if (chunks_emerged >= max_emerge) {
						return;
					}
				}

				if (chunk && rz == 0) {
					chunk_is_solid = chunk->isSolid();
				}

				if (chunk && chunk->isFloorSolid()) {
					break;
				}
			}

			// Load chunk above

			if (!chunk_is_solid) {
				V3s chunk_pos = center_chunk_pos + V3s(rel.x, rel.y, 1);
				if (loadChunkForPlayer(peer, chunk_pos)) {
					chunks_emerged++;
					if (chunks_emerged >= max_emerge) {
						return;
					}
				}
			}
		}

		if (rel.x == rel.y || (rel.x < 0 && rel.x == -rel.y) ||
				(rel.x > 0 && rel.x == 1 - rel.y)) {
			int tmp = delta.x;
			delta.x = -delta.y;
			delta.y = tmp;
		}

		rel += delta;
	}
}

bool Server::loadChunkForPlayer(
		Peer *peer, const V3s &cpos, WorldChunk **chunk) {
	if (peer->hasChunk(cpos)) {
		return false;
	}

	WorldChunk *c = world_ctr->getChunk(cpos);
	if (!c) {
		requestEmerge(cpos);
		c = world_ctr->getChunk(cpos);
		assert(c);
	}

	if (!c->isActive()) {
		return false;
	}

	sendChunkToPlayer(peer, c);

	if (chunk) {
		*chunk = c;
	}
	return true;
}

Peer *Server::findPeerByName(const std::string &name) const {
	for (auto &it : peers) {
		if (it.second->name == name) {
			return it.second.get();
		}
	}
	return nullptr;
}

Peer *Server::findPeerByEntity(const Entity *ent) const {
	assert(ent);
	for (auto &it : peers) {
		if (it.second->entity == ent) {
			return it.second.get();
		}
	}
	return nullptr;
}

void Server::requestEmerge(const V3s &cpos) {
	if (world_ctr->getWorld()->getChunkState(cpos) != CHUNK_NULL) {
		return;
	}

	world_ctr->emerge(cpos);
}

bool Server::emergePlayer(Peer *client, V3f position) {
	assert(client);
	return world_ctr->emergePlayer(client, position);
}

bool Server::leavePlayer(Peer *client) {
	assert(client);

	Log("Server", INFO) << "Leaving player " << client->name << "...";

	Entity *e = client->entity;
	if (e) {
		int id = e->id;
		world_ctr->removeEntity(e);
		client->entity = nullptr;

		sendRemoveEntityToAllPlayers(id, client);
	}

	if (spec.shutdown_on_last_leave && peers.empty()) {
		close();
	}

	return true;
}

void Server::printPlayers(bool brackets) {
	std::stringstream ss;
	if (brackets) {
		ss << "(List of online players: ";
	}

	for (auto &it : peers) {
		Peer *peer = it.second.get();
		if (peer) {
			ss << " " << peer->name;
		} else {
			ss << " [nullptr]";
		}
	}

	if (brackets) {
		ss << ")";
	}

	ss << ss.str();
}

ServerPlayer *Server::getPlayerInfo(const std::string &name, bool create) {
	Peer *peer = findPeerByName(name);
	if (peer && peer->player_info) {
		return peer->player_info;
	}

	auto saved = player_infos.find(name);
	if (saved != player_infos.end()) {
		if (peer) {
			peer->player_info = saved->second.get();
		}
		return saved->second.get();
	} else if (!create) {
		return nullptr;
	}

	player_infos[name] = std::make_unique<ServerPlayer>(name);
	ServerPlayer *player = player_infos[name].get();

	player->last_position = V3f(5, 5, 0);

	content::Inventory *inventory = &player->inventory;
	content::InventoryList *list_hotbar = inventory->newList("hotbar", 2, 1);
	assert(list_hotbar);
	list_hotbar->set(1, content::ItemStack("torch", 99));

	content::InventoryList *list_main = inventory->newList("main", 6, 5);
	assert(list_main);

	int i = 0;
	for (const auto &itemDef : definitionManager) {
		list_main->set(i, content::ItemStack(itemDef->name, 99));
		i++;
	}

	if (peer) {
		peer->player_info = player;
	}
	return player;
}

void Server::setTimeOfDay(float timeOfDay, float speed) {
	time.timeOfDay = timeOfDay;
	if (speed >= 0) {
		time.speed = speed;
	}

	for (auto &it : peers) {
		sendGameTimeToPlayer(it.second.get(), time);
	}
}
