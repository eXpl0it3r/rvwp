#pragma once

#include "../types/types.hpp"
#include "../network/packets.hpp"

#include <array>
#include <optional>

namespace content {

#define TILE_SIZE 64

/// The draw type of the material
enum EMatType {
	/// A static texture, all tiles look the same
	EMT_STATIC,

	/// 8-way connection
	EMT_CON_8BIT,

	/// Rotates to connect to walls
	EMT_DOORLIKE
};

static std::array<std::string, 3> materialTypes = {
		"static", "8bit", "doorlike"};

static inline std::optional<EMatType> strToMaterialType(
		const std::string &str) {
	if (str == "static") {
		return EMT_STATIC;
	} else if (str == "8bit") {
		return EMT_CON_8BIT;
	} else if (str == "doorlike") {
		return EMT_DOORLIKE;
	} else {
		return {};
	}
}

inline const std::string &materialTypeToString(EMatType materialType) {
	return materialTypes[static_cast<u8>(materialType)];
}

class Material {
public:
	/// The tileset this material is in,
	/// or just the image name if not a tileset.
	std::string name;

	/// Material Draw Type
	EMatType type;

	/// Start positions
	u16 x, y, w, h;

	/// Number of frames in animation
	u16 l;

	Material()
			: name(""), type(EMT_STATIC), x(0), y(0), w(TILE_SIZE),
			  h(TILE_SIZE), l(1) {}

	explicit Material(const std::string &name)
			: name(name), type(EMT_STATIC), x(0), y(0), w(TILE_SIZE),
			  h(TILE_SIZE), l(1) {}

	Material(const std::string &tileset, u16 x, u16 y,
			EMatType type = EMT_STATIC, u16 l = 1)
			: name(tileset), type(type), x(x), y(y), w(TILE_SIZE), h(TILE_SIZE),
			  l(l) {}

	Material(u16 x, u16 y, EMatType type = EMT_STATIC, u16 l = 1)
			: Material("tileset.diffuse.png", x, y, type, l) {}

	size_t serialised_size() const { return 2 + name.size() + 1 + 2 * 5; }

	void serialise(network::Packet &pkt) const {
		pkt << name;
		pkt << (u8)type;
		pkt << (u16)x;
		pkt << (u16)y;
		pkt << (u16)w;
		pkt << (u16)h;
		pkt << (u16)l;
	}

	void deserialise(network::Packet &pkt) {
		pkt >> name;
		u8 t2;
		pkt >> t2;
		type = (EMatType)t2;
		pkt >> x;
		pkt >> y;
		pkt >> w;
		pkt >> h;
		pkt >> l;
	}
};

} // namespace content
