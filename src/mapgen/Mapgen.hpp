#pragma once

#include "world/World.hpp"

namespace MG {

class Mapgen {
protected:
	content::DefinitionManager *def = nullptr;

public:
	explicit Mapgen(content::DefinitionManager *def) : def(def) {}
	virtual ~Mapgen() = default;

	virtual void generate(world::WorldChunk *c) = 0;
};

} // namespace MG
