#include "FlatMG.hpp"
#include "../content/content.hpp"

using namespace MG;
using namespace world;

void FlatMG::generate(WorldChunk *c) {
	auto cpos = c->pos;

	if (cpos.x == 0 && cpos.y == 0) {
		// Top wall
		for (int x = 8; x < 8 + 5; x++) {
			c->tiles[x + 7 * CHUNK_SIZE].cid = def->getCidFromName("wall");
			c->tiles[x + 11 * CHUNK_SIZE].cid = def->getCidFromName("wall");
		}

		// Left wall
		c->tiles[8 + 8 * CHUNK_SIZE].cid = (cpos.z == 0)
				? def->getCidFromName("door")
				: def->getCidFromName("wall");
		c->tiles[8 + 9 * CHUNK_SIZE].cid = def->getCidFromName("wall");
		c->tiles[8 + 10 * CHUNK_SIZE].cid = def->getCidFromName("wall");

		// Right wall
		for (int y = 8; y < 11; y++) {
			assert(!c->tiles[8 + 4 + y * CHUNK_SIZE]);
			c->tiles[8 + 4 + y * CHUNK_SIZE].cid = def->getCidFromName("wall");
		}

		// Floor
		for (int x = 9; x < 8 + 4; x++)
			for (int y = 8; y < 11; y++) {
				c->floort[x + y * CHUNK_SIZE].cid =
						def->getCidFromName("floor_wood");

				if (cpos.z == 0) {
					c->lightLevels[x + y * CHUNK_SIZE] = 0x05;
				}
			}

		c->tiles[9 + 10 * CHUNK_SIZE].cid = (cpos.z == 0)
				? def->getCidFromName("stairs_up")
				: def->getCidFromName("stairs_down");
	}

	// Set terrain
	for (int i = 0; i < CHUNK_SIZE * CHUNK_SIZE; i++) {
		c->terrain[i] = (cpos.z == 0) ? def->getCidFromName("dirt")
									  : def->getCidFromName("void");
	}

	if (cpos.z != 0) {
		c->terrain[5] = def->getCidFromName("stone");
	}
}
