#pragma once

#include <functional>
#include <cassert>

#include "EventBus.hpp"

template <class T>
class EventListener : public IEventListener<T> {
	EventBus<T> *bus;
	std::function<void(T)> function;

public:
	EventListener() : bus(nullptr) {}

	EventListener(EventBus<T> *bus, std::function<void(T)> function)
			: bus(bus), function(function) {
		if (bus) {
			bus->addListener(this);
		}
	}

	EventListener(const EventListener &other) = delete;

	~EventListener() { detach(); }

	/// Invoke listener with event object
	///
	/// @param event
	void operator()(const T &event) const override { function(event); }

	/// Replace event bus
	void attach(EventBus<T> *to) {
		assert(to);

		detach();

		bus = to;
		bus->addListener(this);
	}

	/// Remove from parent EventBus
	void detach() override {
		if (bus) {
			bus->removeListener(this);
			bus = nullptr;
		}
	}

	/// @return whether the listener is attached
	bool isAttached() const { return bus != nullptr; }
};
