#pragma once

#include <queue>
#include <algorithm>
#include <functional>

template <class T>
class IEventListener {
public:
	virtual ~IEventListener() = default;

	virtual void operator()(const T &event) const = 0;
	virtual void detach() = 0;
};

/// Generic event bus
///
/// @tparam T The Event type
template <class T>
class EventBus {
	std::queue<T> events;
	std::vector<IEventListener<T> *> listeners;

public:
	EventBus() = default;
	EventBus(const EventBus<T> &other) = delete;

	~EventBus() {
		for (auto listener : listeners) {
			listener->detach();
		}
	}

	/// Emplace an event
	///
	/// @tparam Args
	/// @param args
	template <class... Args>
	void emplace(Args &&...args) {
		events.emplace(std::forward<Args>(args)...);
	}

	/// Push an event
	///
	/// @param event
	void push(T event) { events.push(event); }

	/// Add a listener.
	///
	/// @param listener
	void addListener(IEventListener<T> *listener) {
		listeners.push_back(listener);
	}

	/// Remove a listener
	///
	/// @param listener
	void removeListener(IEventListener<T> *listener) {
		listeners.erase(
				std::remove(listeners.begin(), listeners.end(), listener));
	}

	/// Process all events
	void flush() {
		while (!events.empty()) {
			const auto &event = events.front();
			raise(event);
			events.pop();
		}
	}

private:
	void raise(const T &event) {
		for (auto listener : listeners) {
			(*listener)(event);
		}
	}
};
