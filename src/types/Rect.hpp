#pragma once

#include "Vector.hpp"

namespace types {
template <typename T>
class Rect3 {
public:
	Vector3<T> minPos;
	Vector3<T> size;

	Rect3<T>() = default;

	Rect3<T>(Vector3<T> minPos, Vector3<T> size) : minPos(minPos), size(size) {}

#ifdef SFML_RECT_HPP
	Rect3<T>(const sf::Rect<T> &other, const s32 &z, const s32 &zheight)
			: minPos(other.left, other.top, z),
			  size(other.width, other.height, zheight) {}

	explicit operator sf::Rect<T>() const {
		return {minPos.x, minPos.y, size.x, size.y};
	}
#endif

	Vector3<T> getMaxPos() const { return minPos + size; }

	inline bool empty() const {
		return size.x == 0 || size.y == 0 || size.z == 0;
	}

	bool contains(const Vector3<T> &pos) const {
		Vector3<T> maxPos = getMaxPos();
		return pos.x >= minPos.x && pos.x < maxPos.x && pos.y >= minPos.y &&
				pos.y < maxPos.y && pos.z >= minPos.z && pos.z < maxPos.z;
	}

	bool contains(const Rect3<T> &rect) const {
		Vector3<T> thisMaxPos = getMaxPos();
		Vector3<T> rectMaxPos = rect.getMaxPos();

		return minPos.x <= rect.minPos.x && minPos.y <= rect.minPos.y &&
				minPos.z <= rect.minPos.z && rectMaxPos.x <= thisMaxPos.x &&
				rectMaxPos.y <= thisMaxPos.y && rectMaxPos.z <= thisMaxPos.z;
	}

	bool intersects(const Rect3<T> &other) const {
		Vector3<T> maxPos = getMaxPos();
		Vector3<T> omaxPos = other.getMaxPos();
		T interLeft = std::max(minPos.x, other.minPos.x);
		T interTop = std::max(minPos.y, other.minPos.y);
		s32 interAbove = std::max(minPos.z, other.minPos.z);
		T interRight = std::min(maxPos.x, omaxPos.x);
		T interBottom = std::min(maxPos.y, omaxPos.y);
		s32 interBelow = std::min(maxPos.z, omaxPos.z);
		return (interLeft < interRight) && (interTop < interBottom) &&
				(interAbove < interBelow);
	}

	void extend(const Rect3<T> &other) {
		if (empty()) {
			minPos = other.minPos;
			size = other.size;
		} else {
			Vector3<T> maxPos =
					std::max(minPos + size, other.minPos + other.size);
			minPos = std::min(minPos, other.minPos);
			size = maxPos - minPos;
		}
	}

	bool operator==(const Rect3<T> &other) const {
		return minPos == other.minPos && size == other.size;
	}

	static Rect3<T> fromCenter(const Vector3<T> &pos, T sidelength) {
		Vector3<T> minPos = pos - sidelength / (T)2;
		Vector3<T> size = {sidelength, sidelength, sidelength};
		return {minPos, size};
	}

	static Rect3<T> fromInclusive(Vector3<T> from, Vector3<T> to) {
		Vector3<T> size = to - from + Vector3<T>(1, 1, 1);
		return {from, size};
	}
};

typedef Rect3<s32> Rect3s;
typedef Rect3<f32> Rect3f;

} // namespace types
