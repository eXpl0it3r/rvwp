#pragma once

#include <cstdint>
#include <cmath>
#include <string>

namespace types {

typedef std::uint8_t u8;
typedef std::uint16_t u16;
typedef std::uint32_t u32;
typedef std::uint64_t u64;

typedef std::int8_t s8;
typedef std::int16_t s16;
typedef std::int32_t s32;
typedef std::int64_t s64;

typedef unsigned int uint;

typedef float f32;
typedef double f64;

// Just to check I've defined them right.
static_assert(sizeof(u8) == 1);
static_assert(sizeof(u16) == 2);
static_assert(sizeof(u32) == 4);
static_assert(sizeof(u64) == 8);
static_assert(sizeof(s8) == 1);
static_assert(sizeof(s16) == 2);
static_assert(sizeof(s32) == 4);
static_assert(sizeof(s64) == 8);
static_assert(sizeof(f32) == 4);
static_assert(sizeof(f64) == 8);

const inline static double PI = 3.14159265358979323846;

} // namespace types

#include "Vector.hpp"

using namespace types;

static const std::string ESCAPE_CODE = {(char)0x1B};
inline std::string MakeColorEscape(const std::string &code) {
	return ESCAPE_CODE + "(c@" + code + ")";
}

template <class T>
inline T clamp(T x, T min, T max) {
	if (x < min) {
		return min;
	} else if (x > max) {
		return max;
	} else {
		return x;
	}
}
