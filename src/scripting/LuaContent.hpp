#pragma once

#include "content/Material.hpp"
#include "Lua.hpp"

namespace scripting {

extern content::Material LuaToMaterial(const sol::object &obj);

extern sol::table MaterialToLua(
		const content::Material &material, sol::this_state state);

} // namespace scripting
