#pragma once

#include <lua.hpp>

#ifndef LUAJIT_VERSION
#	error "PUC Lua is not supported, please use LuaJIT"
#endif

#include "../types/types.hpp"

#define SOL_ALL_SAFETIES_ON 1
#include <sol/sol.hpp>

#include "../types/Rect.hpp"

namespace sol {

template <typename T>
struct lua_type_of<Vector3<T>>
		: std::integral_constant<sol::type, sol::type::table> {};

template <typename Handler, typename T>
inline bool sol_lua_check(sol::types<Vector3<T>>, lua_State *L, int index,
		Handler &&handler, sol::stack::record &tracking) {
	int absoluteIndex = lua_absindex(L, index);
	if (!stack::check<sol::table>(L, absoluteIndex, handler)) {
		tracking.use(1);
		return false;
	}

	sol::stack::get_field(L, 1, absoluteIndex);
	bool x = sol::stack::check<float>(L, -1);

	sol::stack::get_field(L, 2, absoluteIndex);
	bool y = sol::stack::check<float>(L, -1);

	sol::stack::get_field(L, 3, absoluteIndex);
	bool z = sol::stack::check<float>(L, -1);

	sol::stack::pop_n(L, 3);

	tracking.use(1);
	return x && y && z;
}

inline V3f sol_lua_get(sol::types<V3f>, lua_State *L, int index,
		sol::stack::record &tracking) {
	int absoluteIndex = lua_absindex(L, index);

	sol::table table = sol::stack::get<sol::table>(L, absoluteIndex);
	float x = table[1];
	float y = table[2];
	float z = table[3];

	tracking.use(1);

	return V3f{x, y, static_cast<s32>(std::floor(z))};
}

inline V3s sol_lua_get(sol::types<V3s>, lua_State *L, int index,
		sol::stack::record &tracking) {
	int absoluteIndex = lua_absindex(L, index);

	sol::table table = sol::stack::get<sol::table>(L, absoluteIndex);
	float x = table[1];
	float y = table[2];
	int z = table[3];

	tracking.use(1);

	return V3f(x, y, z).floor();
}

template <typename T>
inline int sol_lua_push(
		sol::types<Vector3<T>>, lua_State *L, const Vector3<T> &pos) {
	lua_createtable(L, 3, 0);

	lua_getglobal(L, "Vector");
	lua_setmetatable(L, -2);

	sol::stack_table vec(L);
	vec[1] = pos.x;
	vec[2] = pos.y;
	vec[3] = pos.z;

	return 1;
}

} // namespace sol

namespace scripting {

template <typename T>
inline sol::table RectToTable(const Rect3<T> &rect, sol::this_state state) {
	sol::table table(state.L, sol::create);
	table["from"] = rect.minPos;
	table["size"] = rect.size;
	return table;
}

template <typename T>
inline Rect3<T> TableToRect(sol::table obj) {
	return {obj["from"], obj["size"]};
}

template <typename T>
inline sol::optional<T> std2sol(std::optional<T> opt) {
	if (opt.has_value()) {
		return {opt.value()};
	} else {
		return {};
	}
}

template <typename T>
inline std::optional<T> sol2std(sol::optional<T> opt) {
	if (opt.has_value()) {
		return {opt.value()};
	} else {
		return {};
	}
}

} // namespace scripting
