#pragma once

#include <json/json.h>
#include <string>
#include <vector>

namespace scripting {

class ModConfig {
public:
	const std::string name;
	const std::string author;
	const std::string path;
	const std::vector<std::string> dependencies;

	static ModConfig readFromJSON(
			const Json::Value &root, const std::string &path);

	/// Create mod configuration by reading a file.
	///
	/// @returns The config, always
	static ModConfig readFromFile(
			const std::string &config, const std::string &path);
};

} // namespace scripting
