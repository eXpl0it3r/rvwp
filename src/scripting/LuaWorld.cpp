#include "LuaWorld.hpp"

world::Pathfinder::Settings scripting::TableToSettings(
		sol::table table, content::DefinitionManager *definitionManager) {
	world::Pathfinder::Settings defaultValues{};

	std::unordered_map<content_id, float> weights;

	sol::optional<sol::table> optionalWeights = table["weights"];
	if (optionalWeights) {
		for (const auto &value : optionalWeights.value()) {
			auto name = value.first.as<std::string>();
			auto weight = value.second.as<float>();

			content_id cid = definitionManager->getCidFromName(name);
			if (cid == 0) {
				throw sol::error("Unknown tile name: " + name);
			}

			weights[cid] = weight;
		}
	}

	// clang-format off
	return {
			table.get_or("give_up_cost", defaultValues.giveUpCost),
			table.get_or("accept_exact_target", defaultValues.acceptExactTarget),
			table.get_or("accept_neighbours", defaultValues.acceptNeighbours),
			table.get_or("accept_below", defaultValues.acceptBelow),
			weights
	};
	// clang-format on
}
