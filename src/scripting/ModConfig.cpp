#include "ModConfig.hpp"

#include <fstream>
#include <cassert>

using namespace scripting;

#define parseCheck(cond, msg) assert((cond) && (msg));

ModConfig ModConfig::readFromJSON(
		const Json::Value &root, const std::string &path) {
	parseCheck(root.isObject(), "Expected object as root");

	std::string name = root["name"].asString();
	std::string author = root["author"].asString();

	std::vector<std::string> dependencies;
	if (root.isMember("dependencies")) {
		for (const auto &dep : root["dependencies"]) {
			dependencies.push_back(dep.asString());
		}
	}

	return {
			name,
			author,
			path,
			dependencies,
	};
}

ModConfig ModConfig::readFromFile(
		const std::string &config, const std::string &path) {
	Json::Value root;
	{
		std::ifstream is(config);
		is >> root;
	}

	return readFromJSON(root, path);
}
