#pragma once

#include "scripting/Lua.hpp"

#include <tuple>
#include <utility>

namespace scripting {

class LuaSecurity {
	sol::state &lua;
	sol::environment env;
	std::string activeModPath;

public:
	explicit LuaSecurity(sol::state &lua) : lua(lua) { buildEnvironment(); }

	sol::environment &getEnvironment() { return env; }

	void setActiveModPath(std::string v) { activeModPath = std::move(v); }

	/// Checks whether access to a path is allowed.
	///
	/// @param filepath Path to resource
	/// @param write_required
	/// @return true if action is allowed
	bool checkPath(const std::string &filepath, bool write_required);

private:
	void buildEnvironment();

	/// Secure loadstring. Prohibits bytecode, applies environment.
	///
	/// @param str Source code
	/// @param chunkname Chunk name
	/// @return Either (func, nil) or (nil, error-str)
	std::tuple<sol::object, sol::object> loadstring(const std::string &str,
			const std::string &chunkname = sol::detail::default_chunk_name());

	/// Secure loadfile. Checks path, then calls secure loadstring.
	///
	/// @param path Path to file
	/// @return Either (func, nil) or (nil, error-str)
	std::tuple<sol::object, sol::object> loadfile(const std::string &path);

	/// Secure dofile
	/// @param path Path to file
	/// @return Return value of function
	sol::object dofile(const std::string &path);
};

} // namespace scripting
