#include "ModOrderResolver.hpp"
#include "ModException.hpp"

using namespace scripting;

std::vector<ModConfig> ModOrderResolver::resolve() {
	ordered.clear();
	added.clear();

	for (const auto &it : mods_by_name) {
		resolveSpecific(it.second);
	}

	// Check dependencies appear before
	std::unordered_set<std::string> present;
	for (const auto &mod : ordered) {
		present.insert(mod.name);
		for (const auto &depname : mod.dependencies) {
			if (present.find(depname) == present.end()) {
				throw ModException(
						"Mod is missing dependency, or there is a cycle");
			}
		}
	}

	return ordered;
}

void ModOrderResolver::resolveSpecific(const ModConfig &mod) {
	if (added.find(mod.name) != added.end()) {
		return;
	}

	added.insert(mod.name);

	for (const auto &modname : mod.dependencies) {
		const auto &it = mods_by_name.find(modname);
		if (it != mods_by_name.end()) {
			resolveSpecific(it->second);
		}
	}

	ordered.push_back(mod);
}
