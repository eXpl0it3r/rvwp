#pragma once
#include "types/types.hpp"
#include "types/Rect.hpp"
#include "address.hpp"
#include "protocol.hpp"
#include <vector>

namespace network {

class Packet {
	Address to;
	EPacketType type;
	std::vector<u8> data;
	size_t cursor;

public:
	Packet();
	Packet(Address to, EPacketType type);
	Packet(Address to, EPacketType type, size_t size);
	Packet(Address from, u8 *packet, size_t size);
	Packet(const Packet &other) = delete;

	void setData(u8 *packet, size_t size);

	inline void reset() { cursor = 5; }

	inline void *getActualData() const { return (void *)(&data[0]); }

	inline Address getOther() const { return to; }

	inline void setOther(Address other) { to = other; }

	inline bool end() const { return cursor == data.size(); }
	inline size_t getRemaining() const { return data.size() - cursor; }

	inline EPacketType getType() const { return type; }

	inline bool error() const { return type == TONONE_nullptr; }

	inline size_t size() const { return data.size() - 5; }

	inline size_t actualSize() const { return data.size(); }

	// Ints
	Packet &operator<<(bool i);
	Packet &operator<<(u8 i);
	Packet &operator<<(u16 i);
	Packet &operator<<(u32 i);
	Packet &operator<<(u64 i);
	Packet &operator>>(bool &i);
	Packet &operator>>(u8 &dst);
	Packet &operator>>(u16 &dst);
	Packet &operator>>(u32 &dst);
	Packet &operator>>(u64 &dst);

	// Floats
	Packet &operator<<(const f32 &i);
	Packet &operator>>(f32 &dst);

	// Vectors
	Packet &operator<<(const V3f &i);
	Packet &operator<<(const V3s &i);
	Packet &operator<<(const Rect3s &i);
	Packet &operator<<(const Rect3f &i);
	Packet &operator>>(V3f &dst);
	Packet &operator>>(V3s &dst);
	Packet &operator>>(Rect3s &dst);
	Packet &operator>>(Rect3f &dst);

	// strings
	Packet &operator<<(const std::string &i);
	Packet &operator>>(std::string &dst);
};

} // namespace network
