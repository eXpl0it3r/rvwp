#pragma once
#include "types/types.hpp"

#if HAVE_ENDIAN_H
#	ifdef _WIN32
#		define __BYTE_ORDER 0
#		define __LITTLE_ENDIAN 0
#		define __BIG_ENDIAN 1
#	elif defined(__MACH__) && defined(__APPLE__)
#		include <machine/endian.h>
#	elif defined(__FreeBSD__)
#		include <sys/endian.h>
#	else
#		include <endian.h>
#	endif
#endif

#include <string.h>
#include <assert.h>

namespace network {

#if HAVE_ENDIAN_H
// use machine native byte swapping routines
// Note: memcpy below is optimized out by modern compilers

inline u16 readU16(const u8 *data) {
	u16 val;
	memcpy(&val, data, 2);
	return be16toh(val);
}

inline u32 readU32(const u8 *data) {
	u32 val;
	memcpy(&val, data, 4);
	return be32toh(val);
}

inline u64 readU64(const u8 *data) {
	u64 val;
	memcpy(&val, data, 8);
	return be64toh(val);
}

inline void writeU16(u8 *data, u16 i) {
	u16 val = htobe16(i);
	memcpy(data, &val, 2);
}

inline void writeU32(u8 *data, u32 i) {
	u32 val = htobe32(i);
	memcpy(data, &val, 4);
}

inline void writeU64(u8 *data, u64 i) {
	u64 val = htobe64(i);
	memcpy(data, &val, 8);
}
#else
// generic byte-swapping implementation
inline u16 readU16(const u8 *data) {
	return ((u16)data[0] << 8) | ((u16)data[1] << 0);
}

inline u32 readU32(const u8 *data) {
	return ((u32)data[0] << 24) | ((u32)data[1] << 16) | ((u32)data[2] << 8) |
			((u32)data[3] << 0);
}

inline u64 readU64(const u8 *data) {
	return ((u64)data[0] << 56) | ((u64)data[1] << 48) | ((u64)data[2] << 40) |
			((u64)data[3] << 32) | ((u64)data[4] << 24) | ((u64)data[5] << 16) |
			((u64)data[6] << 8) | ((u64)data[7] << 0);
}

inline void writeU16(u8 *data, u16 i) {
	data[0] = (i >> 8) & 0xFF;
	data[1] = (i >> 0) & 0xFF;
}

inline void writeU32(u8 *data, u32 i) {
	data[0] = (i >> 24) & 0xFF;
	data[1] = (i >> 16) & 0xFF;
	data[2] = (i >> 8) & 0xFF;
	data[3] = (i >> 0) & 0xFF;
}

inline void writeU64(u8 *data, u64 i) {
	data[0] = (i >> 56) & 0xFF;
	data[1] = (i >> 48) & 0xFF;
	data[2] = (i >> 40) & 0xFF;
	data[3] = (i >> 32) & 0xFF;
	data[4] = (i >> 24) & 0xFF;
	data[5] = (i >> 16) & 0xFF;
	data[6] = (i >> 8) & 0xFF;
	data[7] = (i >> 0) & 0xFF;
}
#endif // HAVE_ENDIAN_H

inline u8 readU8(const u8 *data) {
	return ((u8)data[0] << 0);
}

inline s8 readS8(const u8 *data) {
	return (s8)readU8(data);
}

inline s16 readS16(const u8 *data) {
	return (s16)readU16(data);
}

inline s32 readS32(const u8 *data) {
	return (s32)readU32(data);
}

inline s64 readS64(const u8 *data) {
	return (s64)readU64(data);
}

inline f32 readF32(const u8 *data) {
	return *reinterpret_cast<const float *>(data);
}

inline V3f readV3f(const u8 *data) {
	V3f pos;
	pos.x = readF32(&data[0]);
	pos.y = readF32(&data[4]);
	pos.z = readU32(&data[8]);
	return pos;
}

inline V3s readV3s(const u8 *data) {
	V3s pos;
	pos.x = readU32(&data[0]);
	pos.y = readU32(&data[4]);
	pos.z = readU32(&data[8]);
	return pos;
}

inline std::string readString(const u8 *data, int &p) {
	u16 len = readU16(data);
	p += len + 2;
	return std::string(&((char *)data)[2], len);
}

inline void writeU8(u8 *data, u8 i) {
	data[0] = (i >> 0) & 0xFF;
}

inline void writeS8(u8 *data, s8 i) {
	writeU8(data, (u8)i);
}

inline void writeS16(u8 *data, s16 i) {
	writeU16(data, (u16)i);
}

inline void writeS32(u8 *data, s32 i) {
	writeU32(data, (u32)i);
}

inline void writeS64(u8 *data, s64 i) {
	writeU64(data, (u64)i);
}

inline void writeF32(u8 *data, f32 i) {
	f32 *ptr = reinterpret_cast<f32 *>(data);
	*ptr = i;
}

inline void writeV3f(u8 *data, const V3f &pos) {
	assert(sizeof(pos.x) == 4);
	writeF32(&data[0], pos.x);
	writeF32(&data[4], pos.y);
	writeU32(&data[8], pos.z);
}

inline void writeV3s(u8 *data, const V3s &pos) {
	assert(sizeof(pos.x) == 4);
	writeU32(&data[0], (u32)pos.x);
	writeU32(&data[4], (u32)pos.y);
	writeU32(&data[8], (u32)pos.z);
}

inline void writeString(u8 *data, const std::string &string) {
	writeU16(data, string.size());
	memcpy(&data[2], &string[0], string.size());
}

inline void writeString(u8 *data, u8 *start, size_t size) {
	memcpy(data, start, size);
}

} // namespace network
