#include "net.hpp"
#include "packets.hpp"
#include <log.hpp>
#include <assert.h>
#include "enet/enet.h"
#include <sanity.hpp>

using namespace network;

void network::null_opcode_handler(Packet &pkt) {
	LogF(ERROR, "[Socket] Unable to find handler for packet of type %d.",
			pkt.getType());
}

bool network::InitializeSockets() {
	return enet_initialize() == 0;
}

void network::ShutdownSockets() {
	enet_deinitialize();
}

void Socket::close() {
	if (host) {
		Log("Socket", INFO) << "Closing socket for " << port;
		enet_host_destroy(host);
		host = nullptr;
	}
}

bool Socket::startServer(unsigned short nport) {
	assert(!isOpen());

	ENetAddress address;
	address.host = ENET_HOST_ANY;
	address.port = nport;

	host = enet_host_create(
			&address /* the address to bind the server host to */,
			32 /* allow up to 32 clients and/or outgoing connections */,
			3 /* allow up to 3 channels to be used: 0, 1, and 2 */,
			0 /* assume any amount of incoming bandwidth */,
			0 /* assume any amount of outgoing bandwidth */);

	if (host == nullptr) {
		Log("", ERROR) << "Unable to create ENet server host";
		return false;
	}

	port = nport;

	return true;
}

bool Socket::startClient(const Address &address) {
	host = enet_host_create(nullptr /* create a client host */,
			1 /* only allow 1 outgoing connection */,
			3 /* allow up 2 channels to be used, 0 and 1 */,
			57600 / 8 /* 56K modem with 56 Kbps downstream bandwidth */,
			14400 / 8 /* 56K modem with 14 Kbps upstream bandwidth */);

	if (host == nullptr) {
		Log("Net", ERROR) << "Unable to create ENet server host";
		return false;
	}

	ENetAddress eaddress;
	eaddress.host = address.GetAddress();
	eaddress.port = address.GetPort();

	server = enet_host_connect(host, &eaddress, 3, 0);
	if (server == nullptr) {
		Log("Net", ERROR) << "No available peers to initiate connection";
		return false;
	}

	ENetEvent event;
	auto ret = enet_host_service(host, &event, 10000);
	if (ret > 0 && event.type == ENET_EVENT_TYPE_CONNECT) {
		LOG_F(INFO, "[Socket] Connected to %x:%d", event.peer->address.host,
				event.peer->address.port);
		return true;
	} else {
		Log("Net", ERROR) << "Unable to connect to server " << ret << " : "
						  << event.type;

		return false;
	}
}

bool Socket::isOpen() const {
	return host != nullptr;
}

void Socket::send(const Packet &pkt) {
	if (!isOpen()) {
		return;
	}

	int channel = 0;
	u32 flags = ENET_PACKET_FLAG_RELIABLE;

	auto type = pkt.getType();
	if (type == TOCLIENT_WORLD_BLOCK) {
		channel = 2;
	} else if (type == TOSERVER_PLAYER_MOVE) {
		channel = 1;
		flags = 0; // ENET_PACKET_FLAG_UNSEQUENCED;
	}

	if (send(pkt, channel, flags) != 0) {
		LogF(ERROR, "[Socket] Failed to send packet");
		assert(false);
	}
}

bool Socket::send(const Packet &pkt, int channel, u32 flags) {
	if (!isOpen()) {
		return false;
	}

	ENetPacket *packet =
			enet_packet_create(pkt.getActualData(), pkt.actualSize(), flags);

	return enet_peer_send(getPeer(pkt.getOther()), channel, packet);
}

void Socket::disconnect(const Address &address) {
	enet_peer_disconnect(getPeer(address), 0);
}

void Socket::setHandlers(PacketRoutingTable &table) {
	handlers = std::move(table);
}

void Socket::update() {
	if (server) {
		reliableDataInTransit += server->reliableDataInTransit;
	}

	ENetEvent event;
	Packet pkt;
	while (host && enet_host_service(host, &event, 0) > 0) {
		switch (event.type) {
		case ENET_EVENT_TYPE_CONNECT:
			LogF(INFO, "[Socket] A new client connected from %x:%d",
					event.peer->address.host, event.peer->address.port);
			assert(event.peer->connectID > 0);
			peers[event.peer->connectID] = event.peer;
			break;

		case ENET_EVENT_TYPE_RECEIVE: {
			pkt.setOther(Address(
					event.peer->address.host, event.peer->address.port));
			pkt.setData(event.packet->data, event.packet->dataLength);

			EPacketType type = pkt.getType();

			if ((int)type < handlers.size()) {
				handlers[(int)type](pkt);
				if (!pkt.end()) {
					LogF(WARNING,
							"Package %d wasn't wholly consumed. Remaining: %ld "
							"bytes",
							(int)type, pkt.getRemaining());
				}
			} else {
				null_opcode_handler(pkt);
			}

			enet_packet_destroy(event.packet);

			break;
		}
		case ENET_EVENT_TYPE_DISCONNECT:
			LogF(INFO, "[Socket] %x:%d disconnected.", event.peer->address.host,
					event.peer->address.port);

			pkt = Packet(
					Address(event.peer->address.host, event.peer->address.port),
					TOANY_DISC, 0);
			handlers[(int)TOANY_DISC](pkt);

			if (event.peer->connectID) {
				peers.erase(event.peer->connectID);
			}
			break;
		case ENET_EVENT_TYPE_NONE:
			break;
		}
	}
}

void Socket::getStats(NetworkStats &stats) {
	stats.packetsReceived = host->totalReceivedPackets - totalReceivedPackets;
	totalReceivedPackets = host->totalReceivedPackets;

	stats.packetsSent = host->totalSentPackets - totalSentPackets;
	totalSentPackets = host->totalSentPackets;

	if (server) {
		stats.packetsLost = server->packetsLost;
		stats.reliableDataInTransit = reliableDataInTransit;
		reliableDataInTransit = 0;
	}
}

ENetPeer *Socket::getPeer(const Address &address) {
	if (server) {
		return server;
	}

	for (auto peer2 : peers) {
		auto eaddr =
				Address(peer2.second->address.host, peer2.second->address.port);
		if (eaddr == address) {
			return peer2.second;
		}
	}

	return nullptr;
}
