#pragma once
#include "../types/types.hpp"
#include "address.hpp"
#include "packets.hpp"

#include <functional>
#include <array>
#include <unordered_map>

extern "C" {
struct _ENetHost;
typedef struct _ENetHost ENetHost;
struct _ENetPeer;
typedef struct _ENetPeer ENetPeer;
}

#define nullptr_PKT (&network::null_opcode_handler)

namespace network {
typedef std::function<void(Packet &)> PacketHandler;
typedef std::array<PacketHandler, NUM_PACKET_TYPES> PacketRoutingTable;

bool InitializeSockets();
void ShutdownSockets();
void null_opcode_handler(Packet &pkt);

struct NetworkStats {
	int packetsReceived = 0;
	int packetsSent = 0;
	u32 packetsLost = 0;
	u32 reliableDataInTransit = 0;
};

class Packet;

/// A simple class to make enet RAII and also
/// provide some abstraction
class Socket {
	ENetHost *host = nullptr;
	ENetPeer *server = nullptr;
	PacketRoutingTable handlers;
	std::unordered_map<size_t, ENetPeer *> peers;

	int port = -1;

	// Stats
	u32 totalReceivedPackets = 0;
	u32 totalSentPackets = 0;
	int reliableDataInTransit = 0;

public:
	~Socket() { close(); }

	void close();
	bool startServer(unsigned short nport = 0);
	bool startClient(const Address &address);

	int getPort() const { return port; }
	bool isOpen() const;

	void send(const Packet &pkt);
	void disconnect(const Address &address);
	void setHandlers(PacketRoutingTable &table);
	void update();

	void getStats(NetworkStats &stats);

private:
	bool send(const Packet &pkt, int channel, u32 type);

	ENetPeer *getPeer(const Address &address);
};

} // end namespace network
